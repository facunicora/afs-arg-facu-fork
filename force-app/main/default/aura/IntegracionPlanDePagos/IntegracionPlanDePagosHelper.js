({
	enviarPagos : function(component, event) {
        var action = component.get("c.enviarPlanDePagos");
        var recordId = component.get("v.recordId");
        var toastEvent = $A.get("e.force:showToast");
        
        action.setParams({
            "planId": recordId
        });
        action.setCallback(this, function(response) {           
            if (response.getState() == "SUCCESS"){
                var res = response.getReturnValue();
                if(res == "OK" || res === "OK"){
                    var messageTemplate = "Los pagos serán enviados momentáneamente";
                    toastEvent.setParams({ 
                        type : "Success",
                        duration : 5000,
                        message: messageTemplate
                    });
                    toastEvent.fire();
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire(); 
                }else{
                    var messageTemplate = res;
                    toastEvent.setParams({ 
                        type : "error",
                        duration : 5000,
                        message: messageTemplate
                    });
                    toastEvent.fire();
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire(); 
                }
            }else{
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire(); 
            }
        });
        
        $A.enqueueAction(action);
    }
})