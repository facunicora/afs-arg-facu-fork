({
	fetchPicklistValues : function(component, objectName,fieldsList) {
		var action = component.get("c.getPicklistValues");
        action.setParams({
            "objName": objectName,
            "fields": fieldsList
        })
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") { 
                var allValues = response.getReturnValue();
                component.set("v.tipoVueloOptions", allValues['AG_TipodeVuelo__c']);
                component.set("v.terminalOptions", allValues['AG_Terminal__c']);
                component.set("v.direccionOptions", allValues['AG_Direccion__c']);
                component.set("v.tipoViajeOptions", allValues['AG_TipoDeViaje__c']);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	}
})