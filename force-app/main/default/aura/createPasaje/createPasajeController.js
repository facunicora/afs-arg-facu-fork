({
	doInit: function(component, event, helper) {
        
        component.find("pasajeData").getNewRecord(
            "AG_Pasajes__c", // objectApiName
            null, // recordTypeId
            false, // skip cache?
            $A.getCallback(function() {
                var rec = component.get("v.pasaje");
                if(rec === null) {
                    console.log("Error initializing record template: " + error);
                }
                else {
                    console.log("Record template initialized: " + rec.apiName);
                }
            })
        );

        helper.fetchPicklistValues(component,'AG_Pasajes__c','AG_TipodeVuelo__c,AG_TipoDeViaje__c,AG_Terminal__c,AG_Direccion__c');
    },
    
    handleSavePasaje: function(component, event, helper) {
		component.set("v.simplePasaje.CG_TravelPlan__c", component.get("v.recordId"));
        component.set("v.simplePasaje.AG_ParticipanteDelPrograma__c", component.get("v.selectedParticipant.Id"));
        component.set("v.simplePasaje.AG_OrigenPasaje__c", component.get("v.selectedOrigen.Id"));
        component.set("v.simplePasaje.AG_DestinoPasaje__c", component.get("v.selectedDestino.Id"));
        component.find("pasajeData").saveRecord(function(saveResult) {
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                
                // Success! Prepare a toast UI message
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Pasaje Guardado",
                    "message": "El nuevo pasaje fue creado."
                });
                
                // Update the UI: close panel, show toast, refresh account page
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                
                // Reload the view so components not using force:recordData
                // are updated
                $A.get("e.force:refreshView").fire();
            }
            else if (saveResult.state === "INCOMPLETE") {
                console.log("User is offline, device doesn't support drafts.");
            }
            else if (saveResult.state === "ERROR") {
                console.log('Problem saving pasaje, error: ' +
                            JSON.stringify(saveResult.error));
                var errorMsg = '';
                for (var i = 0; i < saveResult.error.length; i++) {
                    errorMsg =  errorMsg + saveResult.error[i].message + '\n';
                }
                component.set("v.pasajeError", errorMsg);
                    
            }else {
                console.log('Unknown problem, state: ' + saveResult.state +
                                    ', error: ' + JSON.stringify(saveResult.error));

            }
        });
        
    },
    handleCancel: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
})