public class ParticipanteDelProgromaTriggerHandler {
    public static void isAfterUpdate(List<AG_ParticipanteDelPrograma__c> ppList,Map<Id,AG_ParticipanteDelPrograma__c> oldMap){
        system.debug('isAfterUpdate');
        createQuoteMayores(ppList,oldMap);
    }
    
    public static void isBeforeInsert(List<AG_ParticipanteDelPrograma__c> ppList){
        system.debug('isBeforeInsert');
        programSegMedicoVerified(ppList);
        updateRecordType(ppList);
        ocultarOMostrarPermisosYConsentimientosEnElPorfolio(ppList,null);
    }
    
    public static void isBeforeUpdate(Map<Id,AG_ParticipanteDelPrograma__c> mapNew, Map<Id,AG_ParticipanteDelPrograma__c> mapOld){
        system.debug('isBeforeUpdate');
        programSegMedicoVerified(mapNew.Values());
        updateRecordType(mapNew.Values());
        ocultarOMostrarPermisosYConsentimientosEnElPorfolio(mapNew.values(),mapOld);
        verificarCuota0mayores(mapNew.Values(),mapOld);
    }
    
    public static void verificarCuota0mayores(List<AG_ParticipanteDelPrograma__c> ppList,Map<Id,AG_ParticipanteDelPrograma__c> mapOld){
        List<Cuota0mayores2__c> cuota0mayores = Cuota0mayores2__c.getAll().values();
        Map<String,Cuota0mayores2__c> cuota0mayoresMap = new Map<String,Cuota0mayores2__c>();
        
        for(Cuota0mayores2__c q0 : cuota0mayores){
            cuota0mayoresMap.put(q0.Partner__c,q0);
        }
        
        Set<Id> ppIds = (new Map<Id,AG_ParticipanteDelPrograma__c>(ppList)).keySet();
        List<Presupuestador__c> preList = [SELECT Estado__c, Partner__c, Presupuestos__c FROM Presupuestador__c WHERE Estado__c = 'Aprobado' AND Presupuestos__c IN: ppIds];
        
        Map<Id,Presupuestador__c> mapPre = new Map<Id,Presupuestador__c>();
        for(Presupuestador__c pre : preList){
            mapPre.put(pre.Presupuestos__c,pre); 
        }
        
        for(AG_ParticipanteDelPrograma__c pp : ppList){
            Schema.RecordTypeInfo recordTypeInfoPP = Schema.SObjectType.AG_ParticipanteDelPrograma__c.getRecordTypeInfosById().get(pp.RecordTypeId);
            if(mapOld.get(pp.Id).AG_EstadoDeLaPostulacion__c != pp.AG_EstadoDeLaPostulacion__c && pp.AG_EstadoDeLaPostulacion__c == 'Admisión en Proceso' && !recordTypeInfoPP.getDeveloperName().containsIgnoreCase('Menores')){
                Decimal montoCuotaCero = (mapPre.get(pp.Id) == null  || cuota0mayoresMap.get(mapPre.get(pp.Id).Partner__c) == null) ? cuota0mayoresMap.get('Otro').cuota_0__c : cuota0mayoresMap.get(mapPre.get(pp.Id).Partner__c).cuota_0__c;
                if(pp.Total_Pagado_en_Plan_de_Pago__c < montoCuotaCero){
                    pp.addError('Para pasar a Admisión en Proceso tiene que haberse pagado el derecho de postulación');
                }
            } 
        }
    }
    
    public static void sendEmailMayoresPlanDeCuotas(List<Id> idAccountList,Map<Id,Id> mapPlanDePago){
        List<EmailTemplate> templateUsed = [Select id,name from EmailTemplate where name = 'A5 - Apertura plan de pagos'];
        // Map<Id,Account> newAccountList = (Map<Id,Account>)[SELECT Id,PersonContactId,PersonEmail FROM Account WHERE Id: IN ];
        List<Messaging.SingleEmailMessage> masterListMails = new List<Messaging.SingleEmailMessage>();
        if(templateUsed.size()>0){
            for(Id idAccount : idAccountList){
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                //list<string> emailAddress = new List<String>();
                //emailAddress.add(emailAdd);
                //email.setToAddresses(emailAddress);
                //email.setTargetObjectId(pp.AG_Participante__c);
                email.setTargetObjectId(idAccount);
                email.setWhatid(mapPlanDePago.get(idAccount));
                email.setTemplateId(templateUsed[0].id);
                email.saveAsActivity = false;
                masterListMails.add(email);
            }
        }
        
        
        if(masterListMails.size() > 0) Messaging.sendEmail(masterListMails);
    }
    
    public static void createQuoteMayores(List<AG_ParticipanteDelPrograma__c> ppList,Map<Id,AG_ParticipanteDelPrograma__c> oldMap){
        Set<Id> ppIds = new Set<Id>();
     
        for(AG_ParticipanteDelPrograma__c pDp : ppList){
            Schema.RecordTypeInfo recordTypeInfoPP = Schema.SObjectType.AG_ParticipanteDelPrograma__c.getRecordTypeInfosById().get(pDp.RecordTypeId);
            if(oldMap.get(pDp.Id).AG_EstadoDeLaPostulacion__c != pDp.AG_EstadoDeLaPostulacion__c && pDp.AG_EstadoDeLaPostulacion__c == 'Admisión Asignado' && !recordTypeInfoPP.getDeveloperName().containsIgnoreCase('Menores')){
             
                ppIds.add(pDp.Id);
            }
        }

        if(ppIds.size() > 0){       
            //Set<Id> ppIds = (new Map<Id,SObject>(ppList)).keySet();
            List<AG_PlanDePago__c> pDpList = [SELECT Id, AG_ParticipanteDelPrograma__c,AG_ParticipanteDelPrograma__r.AG_Participante__r.PersonContactId FROM AG_PlanDePago__c WHERE AG_ParticipanteDelPrograma__c IN:ppIds ];
            List<AG_PlanDePago__c> pDpListNew = new List<AG_PlanDePago__c>();
            List<Id> idAccountList = new List<Id>();
            Map<Id,Id> mapPlanDePago = new Map<Id,Id>();
            
            for(AG_ParticipanteDelPrograma__c pp : ppList){
                //  Schema.RecordTypeInfo recordTypeInfoPP = Schema.SObjectType.AG_ParticipanteDelPrograma__c.getRecordTypeInfosById().get(pp.RecordTypeId);
                
                //     if(oldMap.get(pp.Id).AG_EstadoDeLaPostulacion__c != pp.AG_EstadoDeLaPostulacion__c && pp.AG_EstadoDeLaPostulacion__c == 'Admisión Asignado' && !recordTypeInfoPP.getDeveloperName().containsIgnoreCase('Menores')){
                
                for(AG_PlanDePago__c pDp : pDpList){
                    if(pDp.AG_ParticipanteDelPrograma__c == pp.Id){
                        pDp.Fecha_fin__c = date.today().daysBetween(pp.Fecha__c) < 31 ? pp.Fecha__c.addDays(7) : pp.Fecha__c.addDays(-45);
                        pDp.Monto__c = pp.Monto_Aprobado__c;
                        pDp.Medio_de_Pago__c = pp.AG_MedioDePago_del__c;
                        pDp.Divisa__c = pp.Divisa_Aprobada__c;
                        pDp.CG_CreateQuota__c = true;
                        pDpListNew.add(pDp);
                        mapPlanDePago.put(pDp.AG_ParticipanteDelPrograma__r.AG_Participante__r.PersonContactId,pDp.Id);
                        idAccountList.add(pDp.AG_ParticipanteDelPrograma__r.AG_Participante__r.PersonContactId);
                        break;
                    }
                }
                //}
            }
            
            if(pDpListNew.size()>0){
                update pDpListNew;  
                sendEmailMayoresPlanDeCuotas(idAccountList,mapPlanDePago);
            }    
            
        }
    }
    
    public static void programSegMedicoVerified(List<AG_ParticipanteDelPrograma__c> ppList){
        Set<Id> setPIds = new Set<Id>();
        
        string recordtypename;
        for(AG_ParticipanteDelPrograma__c pp : ppList){
           
            if(pp.recordtypeid != null) recordtypename = Schema.SObjectType.AG_ParticipanteDelPrograma__c.getRecordTypeInfosById().get(pp.recordtypeid).getname();
            if(recordtypename != null && !recordtypename.contains('Menores')){
                setPIds.add(pp.AG_Programa__c);
            }
        } 
        
        Map<Id,AG_Programa__c> newPpMap = new Map<Id,AG_Programa__c>([SELECT Id,Content_provider__c FROM AG_Programa__c WHERE Id IN: setPIds]);
        
        if(setPIds.size() > 0){
            for(AG_ParticipanteDelPrograma__c pp :  ppList){
                Map<String, String> currentValueMap = new Map<String, String>();
                if(pp.HC_Paginas_a_ocultar__c != null){
                    for(String s : pp.HC_Paginas_a_ocultar__c.split(';')){
                        currentValueMap.put(s,s);
                    } 
                }
                
                if(newPpMap.get(pp.AG_Programa__c) != null && newPpMap.get(pp.AG_Programa__c).Content_provider__c == 'AFS' && currentValueMap.containsKey('Historial Médico')){
                    pp.addError('El Programa tiene un Content Provider de AFS y no se puede ocultar la Página Historial medico');
                }
            }   
        }
        
        
    }
    
    public static void updateRecordType(List<AG_ParticipanteDelPrograma__c> ppList){
        try{
            map<id, Schema.RecordTypeInfo> mapRecordTypePpId = Schema.SObjectType.AG_ParticipanteDelPrograma__c.getRecordTypeInfosById();
            map<string, Schema.RecordTypeInfo> mapRecordTypePpName = Schema.SObjectType.AG_ParticipanteDelPrograma__c.getRecordTypeInfosByDeveloperName();
            set<id> progSet = new set<id>();
            Map<String,String> mapRTppRTProg = new Map<String,String>();
            Map<String,String> mapRTProgRTpp = new Map<String,String>();
            for(recordtype_mapping__c rt : recordtype_mapping__c.getAll().values()){
              
                mapRTProgRTpp.put(rt.RecordType_Name_Prog__c,rt.RecordType_Name_PP__c);
                mapRTppRTProg.put(rt.RecordType_Name_PP__c,rt.RecordType_Name_Prog__c);
            }
            
            Map<Id,AG_ParticipanteDelPrograma__c> oldMap = new Map<Id,AG_ParticipanteDelPrograma__c>(); 
            if(trigger.isUpdate) oldMap = (Map<Id, AG_ParticipanteDelPrograma__c>)Trigger.oldMap; 
            for(AG_ParticipanteDelPrograma__c pp : ppList){
                if(trigger.isInsert || (trigger.isUpdate && oldMap.get(pp.Id).AG_Programa__c != pp.AG_Programa__c)){ 
                    progSet.add(pp.AG_Programa__c);  
                }  
            }
            
            if(progSet.size() > 0){
                Map<id, AG_Programa__c> progList = new map<id, AG_Programa__c>([SELECT RecordType.DeveloperName From AG_Programa__c WHERE id in :progSet]);
                String recPPName;
                String recProgName;
                for(AG_ParticipanteDelPrograma__c pp :  ppList){
                    recProgName = progList.get(pp.AG_Programa__c).RecordType.DeveloperName;
                    if(pp.RecordTypeId == null){
                        pp.RecordTypeId = mapRecordTypePpName.get(mapRTProgRTpp.get(recProgName)).RecordTypeId;
                    }else{
                        recPPName = mapRecordTypePpId.get(pp.RecordTypeId).DeveloperName;
                        if(mapRTppRTProg.get(recPPName) != recProgName && mapRecordTypePpName.get(mapRTProgRTpp.get(recProgName)) != null){
                            pp.RecordTypeId = mapRecordTypePpName.get(mapRTProgRTpp.get(recProgName)).RecordTypeId;
                        }
                    }
                    
                } 
            }
        }catch (Exception e){
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
    }
    
    public static void ocultarOMostrarPermisosYConsentimientosEnElPorfolio(List<AG_ParticipanteDelPrograma__c> lstNew,Map<Id,AG_ParticipanteDelPrograma__c> mapOld){
        Set<Id> setAccountIds = new Set<Id>();
        Map<Id,Account> mapAccounts;
        
        for(AG_ParticipanteDelPrograma__c pp : lstNew){
            Schema.RecordTypeInfo recordTypeInfoPP = Schema.SObjectType.AG_ParticipanteDelPrograma__c.getRecordTypeInfosById().get(pp.RecordTypeId);
            if(recordTypeInfoPP != null && !recordTypeInfoPP.getDeveloperName().containsIgnoreCase('menores') && (mapOld == null || pp.Fecha__c != mapOld.get(pp.Id).Fecha__c)){
                setAccountIds.add(pp.AG_Participante__c);
            }            
        }
        
        if(setAccountIds.size() > 0){
            mapAccounts = new Map<Id,Account>([SELECT Id, PersonBirthDate FROM Account WHERE Id in :setAccountIds]);
            
            for(AG_ParticipanteDelPrograma__c pp : lstNew){
                OcultarOMostrarSeccionesEnElPorfolio.ocultarOMostrarPermisosYConsentimientos(pp,pp.Fecha__c,mapAccounts.get(pp.AG_Participante__c).PersonBirthDate);             
            }
        } 
    }   
}

/*********/