public class PasajeTriggers {

    public static void createTravelPlanVuelo() {
        
        List<AG_Pasajes__c> listPasajes = (List<AG_Pasajes__c>)trigger.new;
        set<string> setKey = new set<string>();
        List<AG_AeropuertosDelTravelPlan__c> listAeropuertosDelTravelPlan = new List<AG_AeropuertosDelTravelPlan__c>();
        
        for (AG_Pasajes__c pas : listPasajes) {
            string key = pas.CG_TravelPlan__c + pas.AG_VueloNro__c;
            
            if (!setKey.contains(key)) {
                AG_AeropuertosDelTravelPlan__c atp = new AG_AeropuertosDelTravelPlan__c();
                atp.CG_AeropuertoDestino__c = pas.AG_DestinoPasaje__c;
                atp.CG_AeropuertoOrigen__c = pas.AG_OrigenPasaje__c;
                atp.Direccion__c = pas.AG_Direccion__c;
                atp.Fecha_de_Llegada__c = pas.Fecha_de_Llegada__c;
                atp.Hora_de_Llegada__c = pas.Hora_de_Llegada__c;
                atp.Fecha_de_Salida__c = pas.Fecha_de_Salida__c;
                atp.Hora_de_Salida__c = pas.Hora_de_Salida__c;
                atp.AG_TravelPlan__c = pas.CG_TravelPlan__c;
                atp.CG_NroVuelo__c = pas.AG_VueloNro__c;
                atp.CG_CodigoDeAerolinea__c = pas.AG_Aerolinea__c;
                listAeropuertosDelTravelPlan.add(atp);
            } else {
                setKey.add(key);
            }
            
        }
        
        if (!listAeropuertosDelTravelPlan.IsEmpty()) {
            Database.SaveResult[] result = database.insert(listAeropuertosDelTravelPlan, false);
        }
        
    }
    
}