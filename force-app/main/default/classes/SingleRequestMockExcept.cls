@IsTest
public class SingleRequestMockExcept implements HttpCalloutMock {
    public SingleRequestMockExcept() {
    }
    
    public HTTPResponse respond(HTTPRequest req) {
        CalloutException e = (CalloutException)CalloutException.class.newInstance();
        e.setMessage('TEST ERROR');
        throw e;
    }
}