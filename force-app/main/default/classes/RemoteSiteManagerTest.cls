@IsTest
private class RemoteSiteManagerTest {
    public static testmethod void getTokenTest(){
        insert new Finnegans_Integration__c(Name = 'Access Token', Enabled__c = true, Endpoint__c = 'Test', Client_Id__c = 'Test', Client_Secret__c = 'Test');
        
        Test.setMock(HttpCalloutMock.class, new SingleRequestMock(200, 'Complete', 'Response'));
        
        Test.startTest();
        RemoteSiteManager.getToken(true);
        Test.stopTest();
         
        system.assertEquals('Response', [SELECT Response__c FROM Integration_Log__c LIMIT 1].Response__c);
    }
    public static testmethod void noCustomSettingTest(){
        Test.startTest();
        RemoteSiteManager.getToken(false);
        Test.stopTest();
    }
    public static testmethod void noCustomSettingTest2(){
        insert new Finnegans_Integration__c(Name = 'Access Token', Enabled__c = true, Endpoint__c = 'Test', Client_Id__c = 'Test', Client_Secret__c = 'Test');
        
        Test.setMock(HttpCalloutMock.class, new SingleRequestMock(200, 'Complete', 'Response'));
        Test.startTest();
        System.enqueueJob(new RemoteSiteManagerQueueable('sendPersonalAccount', null, null));
        Test.stopTest();
    }
    public static testmethod void noCustomSettingTest3(){
        insert new Finnegans_Integration__c(Name = 'Access Token', Enabled__c = true, Endpoint__c = 'Test', Client_Id__c = 'Test', Client_Secret__c = 'Test');
        
        Account acc = Util_Test.createPersonAccount();
        insert acc;
        acc = [SELECT Id, PersonContactId, PersonContact.LeadSource, AG_DNI__c, LastName
               FROM Account WHERE Id = :acc.Id];
        Contact con = new Contact(Id = acc.PersonContactId, LeadSource = 'Referido por Voluntario');
        update con;
        AG_Programa__c prog = Util_Test.createPrograma(Schema.SObjectType.AG_Programa__c.getRecordTypeInfosByName().get('Mayores de 18').getRecordTypeId());
        insert prog;
        AG_ParticipanteDelPrograma__c part = Util_Test.createParticipanteDelPrograma2(acc, null, prog);
        insert part;
        AG_PlanDePago__c plan = new AG_PlanDePago__c(Participante__c = acc.Id, AG_ParticipanteDelPrograma__c = part.Id);
        insert plan;
        
        AG_Pago__c pago = new AG_Pago__c(AG_PlanDePago__c = plan.Id, Divisa__c = 'Peso argentino.', 
                                         AG_FechaDeAcreditacion__c = Date.today(), AG_Importe__c = 5000);
        insert pago;
        
        Test.setMock(HttpCalloutMock.class, new SingleRequestMock(200, 'Complete', 'Response'));
        Test.startTest();
        System.enqueueJob(new RemoteSiteManagerQueueable('sendCobranza', null, new List<AG_Pago__c>{pago}));
        Test.stopTest();
    }
    public static testmethod void getTokenTestException(){
        insert new Finnegans_Integration__c(Name = 'Access Token', Enabled__c = true, Endpoint__c = 'Test', Client_Id__c = 'Test', Client_Secret__c = 'Test');
        
        Test.setMock(HttpCalloutMock.class, new SingleRequestMockExcept());
        
        Test.startTest();
        RemoteSiteManager.getToken(true);
        Test.stopTest();
        
        system.assertEquals(null, Finnegans_Integration__c.getInstance('Access Token').Access_Token__c);
        system.assertEquals('TEST ERROR', [SELECT Comments__c FROM Integration_Log__c LIMIT 1].Comments__c);
    }
    
    public static testmethod void sendPersonalAccount(){
        insert new List<Finnegans_Integration__c>{new Finnegans_Integration__c(Name = 'Access Token', Enabled__c = true, Endpoint__c = 'Test', Client_Id__c = 'Test', Client_Secret__c = 'Test'),
            									  new Finnegans_Integration__c(Name = 'Api Cliente', Enabled__c = true, Endpoint__c = 'Test2', Client_Id__c = 'Test2', Client_Secret__c = 'Test2')};
        
        Test.setMock(HttpCalloutMock.class, new SingleRequestMock(200, 'Complete', 'Response'));
        
        Account acc = Util_Test.createPersonAccount();
        insert acc;
        acc = [SELECT Id, PersonContactId, PersonContact.LeadSource, AG_DNI__c, LastName
               FROM Account WHERE Id = :acc.Id];
        Contact con = new Contact(Id = acc.PersonContactId, LeadSource = 'Referido por Voluntario');
        update con;
        AG_Programa__c prog = Util_Test.createPrograma(Schema.SObjectType.AG_Programa__c.getRecordTypeInfosByName().get('Mayores de 18').getRecordTypeId());
        insert prog;
        AG_ParticipanteDelPrograma__c part = Util_Test.createParticipanteDelPrograma2(acc, null, prog);
        insert part;
        AG_PlanDePago__c plan = new AG_PlanDePago__c(Participante__c = acc.Id, AG_ParticipanteDelPrograma__c = part.Id);
        insert plan;
        
        AG_Pago__c pago = new AG_Pago__c(AG_PlanDePago__c = plan.Id, Divisa__c = 'Peso argentino.', 
                                         AG_FechaDeAcreditacion__c = Date.today(), AG_Importe__c = 5000);
        insert pago;
        
        Test.startTest();
        acc = [SELECT Id, IsPersonAccount, AG_DNI__c, name, PersonEmail, PersonContact.Situacion_IIBB__c, 
                                   		  PersonContact.Categoria_Fiscal_Codigo__c, PersonContact.Constancia_Inscripcion_Afip__c, 
                                   		  PersonContact.Identificacion_Tributaria_Codigo__c, PersonMailingCountry, 
                                   		  PersonContact.Condicion_Pago_Codigo__c, PersonContact.Enviado_a_Ceres__c, PersonContactId
               FROM Account WHERE ID = :acc.Id];
        System.enqueueJob(new RemoteSiteManagerQueueable('sendPersonalAccount', acc.Id, new List<AG_Pago__c>{pago}));
        Test.stopTest();
        
        system.assertEquals(1, [SELECT COUNT() FROM Integration_Log__c]);
        system.assertEquals('Response', [SELECT Response__c FROM Integration_Log__c LIMIT 1].Response__c);
        system.assertEquals(true, [SELECT PersonContact.Enviado_a_Ceres__c FROM Account LIMIT 1].PersonContact.Enviado_a_Ceres__c);
    }
    public static testmethod void sendPersonalAccountNoDni(){
        insert new List<Finnegans_Integration__c>{new Finnegans_Integration__c(Name = 'Access Token', Enabled__c = true, Endpoint__c = 'Test', Client_Id__c = 'Test', Client_Secret__c = 'Test'),
            									  new Finnegans_Integration__c(Name = 'Api Cliente', Enabled__c = true, Endpoint__c = 'Test2', Client_Id__c = 'Test2', Client_Secret__c = 'Test2')};
        
        Test.setMock(HttpCalloutMock.class, new SingleRequestMock(200, 'Complete', 'Response'));
        
        Account acc = Util_Test.createPersonAccount();
        acc.AG_DNI__c = null;
        insert acc;
        acc = [SELECT Id, PersonContactId, PersonContact.LeadSource, AG_DNI__c, LastName
               FROM Account WHERE Id = :acc.Id];
        Contact con = new Contact(Id = acc.PersonContactId, LeadSource = 'Referido por Voluntario');
        update con;
        
        Test.startTest();
        acc = [SELECT Id, IsPersonAccount, AG_DNI__c, name, PersonEmail, PersonContact.Situacion_IIBB__c, 
                                   		  PersonContact.Categoria_Fiscal_Codigo__c, PersonContact.Constancia_Inscripcion_Afip__c, 
                                   		  PersonContact.Identificacion_Tributaria_Codigo__c, PersonMailingCountry, 
                                   		  PersonContact.Condicion_Pago_Codigo__c, PersonContact.Enviado_a_Ceres__c, PersonContactId
                                   FROM Account WHERE ID = :acc.Id];
        System.enqueueJob(new RemoteSiteManagerQueueable('sendPersonalAccount', acc.Id, null));
        Test.stopTest();
        
        system.assertEquals(0, [SELECT COUNT() FROM Integration_Log__c]);
        system.assertEquals(false, [SELECT PersonContact.Enviado_a_Ceres__c FROM Account LIMIT 1].PersonContact.Enviado_a_Ceres__c);
    }
    public static testmethod void sendPersonalAccountErrorSend(){
        insert new List<Finnegans_Integration__c>{new Finnegans_Integration__c(Name = 'Access Token', Enabled__c = true, Endpoint__c = 'Test', Client_Id__c = 'Test', Client_Secret__c = 'Test'),
            									  new Finnegans_Integration__c(Name = 'Api Cliente', Enabled__c = true, Endpoint__c = 'Test2', Client_Id__c = 'Test2', Client_Secret__c = 'Test2')};
        
        Test.setMock(HttpCalloutMock.class, new SingleRequestMock(400, 'Complete', 'ERROR'));
        
        Account acc = Util_Test.createPersonAccount();
        insert acc;
        acc = [SELECT Id, PersonContactId, PersonContact.LeadSource, AG_DNI__c, LastName
               FROM Account WHERE Id = :acc.Id];
        Contact con = new Contact(Id = acc.PersonContactId, LeadSource = 'Referido por Voluntario');
        update con;
        AG_Programa__c prog = Util_Test.createPrograma(Schema.SObjectType.AG_Programa__c.getRecordTypeInfosByName().get('Mayores de 18').getRecordTypeId());
        insert prog;
        AG_ParticipanteDelPrograma__c part = Util_Test.createParticipanteDelPrograma2(acc, null, prog);
        insert part;
        AG_PlanDePago__c plan = new AG_PlanDePago__c(Participante__c = acc.Id, AG_ParticipanteDelPrograma__c = part.Id);
        insert plan;
        
        AG_Pago__c pago = new AG_Pago__c(AG_PlanDePago__c = plan.Id, Divisa__c = 'Peso argentino.', 
                                         AG_FechaDeAcreditacion__c = Date.today(), AG_Importe__c = 5000);
        insert pago;
        
        Test.startTest();
        acc = [SELECT Id, IsPersonAccount, AG_DNI__c, name, PersonEmail, PersonContact.Situacion_IIBB__c, 
                                   		  PersonContact.Categoria_Fiscal_Codigo__c, PersonContact.Constancia_Inscripcion_Afip__c, 
                                   		  PersonContact.Identificacion_Tributaria_Codigo__c, PersonMailingCountry, 
                                   		  PersonContact.Condicion_Pago_Codigo__c, PersonContact.Enviado_a_Ceres__c, PersonContactId
                                   FROM Account WHERE ID = :acc.Id];
        System.enqueueJob(new RemoteSiteManagerQueueable('sendPersonalAccount', acc.Id, new List<AG_Pago__c>{pago}));
        Test.stopTest();
        
        system.assertEquals(1, [SELECT COUNT() FROM Integration_Log__c]);
        system.assertEquals('ERROR', [SELECT Response__c FROM Integration_Log__c LIMIT 1].Response__c);
        system.assertEquals(false, [SELECT PersonContact.Enviado_a_Ceres__c FROM Account LIMIT 1].PersonContact.Enviado_a_Ceres__c);
    }
    public static testmethod void sendPersonalAccountNoPersonAccount(){
        insert new List<Finnegans_Integration__c>{new Finnegans_Integration__c(Name = 'Access Token', Enabled__c = true, Endpoint__c = 'Test', Client_Id__c = 'Test', Client_Secret__c = 'Test'),
            									  new Finnegans_Integration__c(Name = 'Api Cliente', Enabled__c = true, Endpoint__c = 'Test2', Client_Id__c = 'Test2', Client_Secret__c = 'Test2')};
        
        Test.setMock(HttpCalloutMock.class, new SingleRequestMock(200, 'Complete', 'Response'));
        
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business_Account').getRecordTypeId();
        Account acc = new Account(Name = 'Test', recordTypeId = devRecordTypeId);
        insert acc;
        
        system.debug([SELECT IsPersonAccount, PersonContactId FROM Account WHERE Id = :acc.Id]);
        
        Test.startTest();
        System.enqueueJob(new RemoteSiteManagerQueueable('sendPersonalAccount', acc.Id, null));
        Test.stopTest();
    }
    
    public static testmethod void sendCobranza(){
        insert new List<Finnegans_Integration__c>{new Finnegans_Integration__c(Name = 'Access Token', Enabled__c = true, Endpoint__c = 'Test', Client_Id__c = 'Test', Client_Secret__c = 'Test'),
            									  new Finnegans_Integration__c(Name = 'Api Cobranza', Enabled__c = true, Endpoint__c = 'Test2', Client_Id__c = 'Test2', Client_Secret__c = 'Test2'),
            									  new Finnegans_Integration__c(Name = 'Api Cliente', Enabled__c = true, Endpoint__c = 'Test3', Client_Id__c = 'Test2', Client_Secret__c = 'Test2')};
        
        Test.setMock(HttpCalloutMock.class, new SingleRequestMock(200, 'Complete', 'Response'));

        Account acc = Util_Test.createPersonAccount();
        insert acc;
        acc = [SELECT Id, PersonContactId, PersonContact.LeadSource, AG_DNI__c, LastName
               FROM Account WHERE Id = :acc.Id];
        Contact con = new Contact(Id = acc.PersonContactId, LeadSource = 'Referido por Voluntario');
        update con;
        AG_Programa__c prog = Util_Test.createPrograma(Schema.SObjectType.AG_Programa__c.getRecordTypeInfosByName().get('Mayores de 18').getRecordTypeId());
        insert prog;
        AG_ParticipanteDelPrograma__c part = Util_Test.createParticipanteDelPrograma2(acc, null, prog);
        insert part;
        AG_PlanDePago__c plan = new AG_PlanDePago__c(Participante__c = acc.Id, AG_ParticipanteDelPrograma__c = part.Id);
        insert plan;
        
        AG_Pago__c pago = new AG_Pago__c(AG_PlanDePago__c = plan.Id, Divisa__c = 'Peso argentino.', 
                                         AG_FechaDeAcreditacion__c = Date.today(), AG_Importe__c = 5000);
        insert pago;
        
        System.enqueueJob(new RemoteSiteManagerQueueable('sendPersonalAccount', acc.Id, new List<AG_Pago__c>{pago}));
        Test.startTest();
        System.enqueueJob(new RemoteSiteManagerQueueable('sendPersonalAccount', acc.Id, new List<AG_Pago__c>{pago}));
        System.enqueueJob(new RemoteSiteManagerQueueable('sendCobranza', acc.Id, new List<AG_Pago__c>{pago}));
        Test.stopTest();
        
        system.assertEquals(2, [SELECT COUNT() FROM Integration_Log__c]);
        system.assertEquals(true, [SELECT Enviado_a_Ceres__c FROM AG_Pago__c LIMIT 1].Enviado_a_Ceres__c);
    }
    public static testmethod void sendCobranzaErrorSend(){
        insert new List<Finnegans_Integration__c>{new Finnegans_Integration__c(Name = 'Access Token', Enabled__c = true, Endpoint__c = 'Test', Client_Id__c = 'Test', Client_Secret__c = 'Test'),
            									  new Finnegans_Integration__c(Name = 'Api Cobranza', Enabled__c = true, Endpoint__c = 'Test2', Client_Id__c = 'Test2', Client_Secret__c = 'Test2'),
            									  new Finnegans_Integration__c(Name = 'Api Cliente', Enabled__c = true, Endpoint__c = 'Test3', Client_Id__c = 'Test2', Client_Secret__c = 'Test2')};
        
        Test.setMock(HttpCalloutMock.class, new SingleRequestMock(400, 'Complete', 'ERROR'));
        
        Account acc = Util_Test.createPersonAccount();
        insert acc;
        acc = [SELECT Id, PersonContactId, PersonContact.LeadSource, AG_DNI__c, LastName
               FROM Account WHERE Id = :acc.Id];
        Contact con = new Contact(Id = acc.PersonContactId, LeadSource = 'Referido por Voluntario');
        update con;
        AG_Programa__c prog = Util_Test.createPrograma(Schema.SObjectType.AG_Programa__c.getRecordTypeInfosByName().get('Mayores de 18').getRecordTypeId());
        insert prog;
        AG_ParticipanteDelPrograma__c part = Util_Test.createParticipanteDelPrograma2(acc, null, prog);
        insert part;
        AG_PlanDePago__c plan = new AG_PlanDePago__c(Participante__c = acc.Id, AG_ParticipanteDelPrograma__c = part.Id);
        insert plan;
        
        AG_Pago__c pago = new AG_Pago__c(AG_PlanDePago__c = plan.Id, Divisa__c = 'Peso argentino.', 
                                         AG_FechaDeAcreditacion__c = Date.today(), AG_Importe__c = 5000);
        insert pago;
        
        Test.startTest();
        System.enqueueJob(new RemoteSiteManagerQueueable('sendCobranza', acc.Id, new List<AG_Pago__c>{pago}));
        Test.stopTest();
        
        system.assertEquals(false, [SELECT Enviado_a_Ceres__c FROM AG_Pago__c LIMIT 1].Enviado_a_Ceres__c);
    }
    /*public static testmethod void sendCobranzaNoPersonAccount(){
        insert new List<Finnegans_Integration__c>{new Finnegans_Integration__c(Name = 'Access Token', Enabled__c = true, Endpoint__c = 'Test', Client_Id__c = 'Test', Client_Secret__c = 'Test'),
            									  new Finnegans_Integration__c(Name = 'Api Cobranza', Enabled__c = true, Endpoint__c = 'Test2', Client_Id__c = 'Test2', Client_Secret__c = 'Test2'),
            									  new Finnegans_Integration__c(Name = 'Api Cliente', Enabled__c = true, Endpoint__c = 'Test3', Client_Id__c = 'Test2', Client_Secret__c = 'Test2')};
        
        Test.setMock(HttpCalloutMock.class, new SingleRequestMock(200, 'Complete', 'Response'));

        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business_Account').getRecordTypeId();
        Account acc = new Account(Name = 'Test', Correo_Electronico__c = 'asd@asd.com', recordTypeId = devRecordTypeId);
        insert acc;
        AG_Programa__c prog = Util_Test.createPrograma(Schema.SObjectType.AG_Programa__c.getRecordTypeInfosByName().get('Mayores de 18').getRecordTypeId());
        insert prog;
        AG_ParticipanteDelPrograma__c part = Util_Test.createParticipanteDelPrograma2(acc, null, prog);
        insert part;
        AG_PlanDePago__c plan = new AG_PlanDePago__c(Participante__c = acc.Id, AG_ParticipanteDelPrograma__c = part.Id);
        insert plan;
        
        AG_Pago__c pago = new AG_Pago__c(AG_PlanDePago__c = plan.Id, Divisa__c = 'Peso argentino.', 
                                         AG_FechaDeAcreditacion__c = Date.today(), AG_Importe__c = 5000);
        insert pago;
        
        Test.startTest();
        System.enqueueJob(new RemoteSiteManagerQueueable('sendCobranza', acc.Id, new List<AG_Pago__c>{pago}));
        Test.stopTest();
    }*/
}