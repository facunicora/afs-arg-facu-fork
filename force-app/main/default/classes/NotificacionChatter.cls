public class NotificacionChatter {
    public static void postear(String texto, Id subject, Id mentionId){
        ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
        input.subjectId = subject;
        ConnectApi.MessageBodyInput body = new ConnectApi.MessageBodyInput();
        body.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        
        ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();
        textSegment.text = texto;
        body.messageSegments.add(textSegment);
        
        if(mentionId != null){
            ConnectApi.EntityLinkSegmentInput mention = new ConnectApi.EntityLinkSegmentInput();
            mention.entityId = mentionId;
            body.messageSegments.add(mention);
        }
        
        input.body = body;
        
        if(!Test.isRunningTest()){
            ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), input);
        }
    }
    
    public static void postearPP(String texto, Id subject){
        Usuario_encargado__c usuarioE = Usuario_encargado__c.getValues('Chequear plan de pago');
        if(usuarioE != null && usuarioE.Username__c != null){
            ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
            input.subjectId = subject;
            ConnectApi.MessageBodyInput body = new ConnectApi.MessageBodyInput();
            body.messageSegments = new List<ConnectApi.MessageSegmentInput>();
            
            ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();
            textSegment.text = texto+'\n ';
            body.messageSegments.add(textSegment);
            
            
            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            mentionSegmentInput.id = [SELECT Id FROM User WHERE Username = :usuarioE.Username__c].Id;
            body.messageSegments.add(mentionSegmentInput);
            
            input.body = body;
            
            if(!Test.isRunningTest()){
                ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), input);
            }
        }
    }
}