global with sharing class RemoteSiteConnection implements Database.AllowsCallouts {
    global static HttpResponse pullObjects(String endPoint, Id logId, Boolean upsertLog){
        Integration_Log__c log = new Integration_Log__c(Endpoint__c = endPoint, Comments__c = 'RemoteSiteManager.getToken()');
        //Construct HTTP request and response
        HttpRequest httpRequest = new HttpRequest();
        HttpResponse httpResponse = new HttpResponse();
        Http http = new Http();
        
        //Set Method and Endpoint and Body
        httpRequest.setMethod('GET');
        httpRequest.setEndpoint(endPoint);
        httpRequest.setTimeout(120000);
        
        try {
            //Send endPoint
            httpResponse = http.send(httpRequest);
            
            system.debug('status: ' + httpResponse.getStatus());
            system.debug('statusCode: ' + httpResponse.getStatusCode());
            system.debug('jsonResponse: ' + httpResponse.getBody());
            
            log.Status__c = (httpResponse.getStatusCode() == 200)? 'Processed' : 'Fail';
            log.Response__c = httpResponse.getBody();
        } catch(System.CalloutException e) {
            System.debug(e);
            System.debug('httpResponse: '+httpResponse.toString());
            
            log.Status__c = 'Fail';
            log.Comments__c = e.getMessage();
            log.Response__c = httpResponse.toString();
            
            httpResponse = null;
        }
        
        if(logId != null){
            log.Id = logId;
        }
        if(upsertLog){
            Database.UpsertResult sr = Database.Upsert(log);
            if(!sr.isSuccess()){
                system.debug(logginglevel.error,log.Id + ' --> ' + String.join(sr.getErrors(),' / '));                
            }
        }
        
        return httpResponse;        
    }
    
    global static HttpResponse postObject(String partialEndPoint, String token, String jsonToPost, Id logId){
        String endPoint = partialEndPoint+'?ACCESS_TOKEN='+token;
        String httpResponseString = '';
        //Construct HTTP request and response
        HttpRequest httpRequest = new HttpRequest();
        HttpResponse httpResponse = new HttpResponse();
        Http http = new Http();
        
        //Set Method and Endpoint and Body
        httpRequest.setMethod('POST');
        httpRequest.setEndpoint(endPoint);
        httpRequest.setTimeout(120000);
        httpRequest.setBody(jsonToPost);
        httpRequest.setHeader('Content-Type', 'application/json');
        
        System.debug('jsonToPost: ' + jsonToPost);        
        Integration_Log__c log = (logId != null)? [SELECT Id, Status__c, Json__c, Endpoint__c, Response__c, Comments__c FROM Integration_Log__c WHERE Id = :logId] : new Integration_Log__c();
        log.Json__c = jsonToPost.abbreviate(131072);
        log.Endpoint__c = endPoint;
        
        try {
            //Send endPoint
            httpResponse = http.send(httpRequest);
            
            log.Response__c = httpResponse.getBody();
            system.debug('status: ' + httpResponse.getStatus());
            system.debug('statusCode: ' + httpResponse.getStatusCode());
            
            if(httpResponse.getStatusCode() == 200){
                //jsonResponse = httpResponse.getBody();
                //Map<String,String> response = (Map<String,String>)JSON.deserialize(jsonResponse, Map<String,String>.Class);
                
                log.Status__c = 'Processed';
            }else{
                log.Status__c = 'Fail';
                log.Comments__c = log.Comments__c == null ? httpResponse.getStatus() : log.Comments__c + ' / ' + httpResponse.getStatus() + '/'+ httpResponse.getBody();
            }
            
            system.debug('httpResponse.getBody: ' + httpResponse.getBody());
            
        } catch(System.CalloutException e) {
            System.debug('-----exception-----');
            System.debug(e);
            System.debug(httpResponse.toString());
            
            log.Status__c = 'Fail';
            log.Comments__c = log.Comments__c == null ? e.getMessage() : log.Comments__c + ' / ' + e.getMessage();
        }
        
        Database.UpsertResult sr = Database.Upsert(log);
        if(!sr.isSuccess()){
            system.debug(logginglevel.error,log.Id + ' --> ' + String.join(sr.getErrors(),' / '));                
        }
        
        return httpResponse;
    }
}