@IsTest
private class RemoteSiteModelTest {
    public static testmethod void ResponseFailTest(){ 
        
        RemoteSiteModel.ResponseFail a = new RemoteSiteModel.ResponseFail(); 
        
        a.status = ''; 
        
        a.error = ''; 
        
    } 
    
    public static testmethod void ClienteTest(){ 
        
        RemoteSiteModel.Cliente a = new RemoteSiteModel.Cliente(); 
        
        a.Nombre = ''; 
        
        a.Codigo = ''; 
        
        a.Activo = false; 
        
        a.RazonSocial = ''; 
        
        a.Descripcion = ''; 
        
        a.Email = ''; 
        
        a.PaginaWeb = ''; 
        
        a.CategoriaFiscalCodigo = ''; 
        
        a.IdentificacionTributariaCodigo = ''; 
        
        a.Numero_de_identificacion_tributaria_99999XVC = ''; 
        
        a.ConstanciaInscripcionAfip = ''; 
        
        a.FechaSolicitadaConstanciaInscripcionAFIP = ''; 
        
        a.FechaVencimientoConstanciaInscripcionAFIP = ''; 
        
        a.IdentTributariaPaisOrigen = ''; 
        
        a.TipoPersona = 0; 
        
        a.ClaveBancariaUnica = ''; 
        
        a.CantEmpleados = 0; 
        
        a.IngresosAnuales = ''; 
        
        a.SituacionIIBB = 0; 
        
        a.NroInscripcionIIBB = ''; 
        
        a.PorcPercepcionIIBB = 0; 
        
        a.VendedorCodigo = ''; 
        
        a.CreditoMaximo = 0; 
        
        a.OrganizacionImagen = ''; 
        
        a.EsProveedor = ''; 
        
        a.ConceptoClienteCodigo = ''; 
        
        a.CuentaClienteCodigo = ''; 
        
        a.ListaPrecioDefaultCodigo = ''; 
        
        a.OriginacionCodigo = ''; 
        
        a.MercadoCodigo = ''; 
        
        a.RestriccionCondPagos = false; 
        
        a.ControlValores = ''; 
        
        a.DiasMaximoValores = ''; 
        
        a.DiferenciaCambio = 0; 
        
        a.CondicionesPago = null; 
        
        a.Direcciones = null; 
        
        a.Telefonos = null; 
        
        a.Percepciones = null; 
        
        a.ContactoItems = null; 
        
        a.ProvinciaItems = null; 
        
        a.DominioItems = null; 
        
    } 
    
    public static testmethod void CondicionPagoTest(){ 
        
        RemoteSiteModel.CondicionPago a = new RemoteSiteModel.CondicionPago(); 
        
        a.CondicionPagoCodigo = ''; 
        
        a.Default_nombre_99999XVC = false; 
        
    } 
    
    public static testmethod void DireccionTest(){ 
        
        RemoteSiteModel.Direccion a = new RemoteSiteModel.Direccion(null,null,null,null,null); 
        
        a.PaisCodigo = ''; 
        
        a.ProvinciaCodigo = ''; 
        
        a.LocalidadCodigo = ''; 
        
        a.CodigoPostal = ''; 
        
        a.Calle = ''; 
        
        a.Numero = ''; 
        
        a.Dpto = ''; 
        
        a.Piso = ''; 
        
        a.Tipo = 0; 
        
        a.Descripcion = ''; 
        
        a.Principal = false; 
        
    } 
    
    public static testmethod void TelefonoTest(){ 
        
        RemoteSiteModel.Telefono a = new RemoteSiteModel.Telefono(null,null); 
        
        a.Numero = ''; 
        
        a.Tipo = ''; 
        
    } 
    
    public static testmethod void PercepcionTest(){ 
        
        RemoteSiteModel.Percepcion a = new RemoteSiteModel.Percepcion(); 
        
        a.TipoRetencionCodigo = ''; 
        
        a.RetencionCodigo = ''; 
        
        a.FechaDesde = ''; 
        
        a.FechaHasta = ''; 
        
        a.Porcentaje = 0; 
        
        a.Modo = 0; 
        
        a.MomentoAplicacionCodigo = ''; 
        
        a.Motivo = 0; 
        
        a.Manual = false; 
        
    } 
    
    public static testmethod void ContactoItemTest(){ 
        
        RemoteSiteModel.ContactoItem a = new RemoteSiteModel.ContactoItem(); 
        
        a.OrganizacionID = 0; 
        
        a.PersonaID = ''; 
        
        a.EsPrincipal = false; 
        
        a.CargoID = ''; 
        
    } 
    
    public static testmethod void ProvinciaItemTest(){ 
        
        RemoteSiteModel.ProvinciaItem a = new RemoteSiteModel.ProvinciaItem(null,null); 
        
        a.ControlImpositivo1 = 0; 
        
        a.ProvinciaID = ''; 
        
    } 
    
    public static testmethod void DominioItemTest(){ 
        
        RemoteSiteModel.DominioItem a = new RemoteSiteModel.DominioItem(null); 
        
        a.Dominio = ''; 
        
    } 
    
    public static testmethod void PuntoDeVentaTest(){ 
        
        RemoteSiteModel.PuntoDeVenta a = new RemoteSiteModel.PuntoDeVenta(); 
        
        a.IdentificacionExterna = ''; 
        
        a.Fecha = ''; 
        
        a.ClienteCodigo = ''; 
        
        a.CondicionPagoCodigo = ''; 
        
        a.MonedaCodigo = ''; 
        
        a.ComprobanteTipoImpositivoID = ''; 
        
        a.TransaccionTipoCodigo = ''; 
        
        a.TransaccionSubtipoCodigo = ''; 
        
        a.WorkflowCodigo = ''; 
        
        a.Descripcion = ''; 
        
        a.NumeroComprobante = ''; 
        
        a.EmpresaCodigo = ''; 
        
        a.CAINumero = 0; 
        
        a.CAIFechaVto = ''; 
        
        a.Productos = null; 
        
        a.Conceptos = null; 
        
        a.PuntoVentaItemsBanco = null; 
        
        a.PuntoVentaItemsTarjeta = null; 
        
        a.PuntoVentaItemsEfectivo = null; 
        
        a.PuntoVentaItemsOtros = null; 
        
        a.ItemsRetencionCobranza = null; 
        
        a.Cotizaciones = null; 
        
    } 
    
    public static testmethod void ProductoPtoVentaTest(){ 
        
        RemoteSiteModel.ProductoPtoVenta a = new RemoteSiteModel.ProductoPtoVenta(); 
        
        a.ProductoCodigo = ''; 
        
        a.Precio = 0; 
        
        a.Cantidad = 0; 
        
        a.Descripcion = ''; 
        
        a.CantidadStock2 = ''; 
        
        a.PrecioTipo = ''; 
        
        a.DimensionDistribucion = null; 
        
        a.vinculacionOrigen = ''; 
        
    } 
    
    public static testmethod void DimensionDistribucionTest(){ 
        
        RemoteSiteModel.DimensionDistribucion a = new RemoteSiteModel.DimensionDistribucion(); 
        
        a.dimensionCodigo = ''; 
        
        a.distribucionCodigo = ''; 
        
        a.distribucionItems = null; 
        
    } 
    
    public static testmethod void DistribucionItemTest(){ 
        
        RemoteSiteModel.DistribucionItem a = new RemoteSiteModel.DistribucionItem(); 
        
        a.codigo = ''; 
        
        a.porcentaje = 0; 
        
    } 
    
    public static testmethod void ConceptoPtoVentaTest(){ 
        
        RemoteSiteModel.ConceptoPtoVenta a = new RemoteSiteModel.ConceptoPtoVenta(); 
        
        a.ConceptoCodigo = ''; 
        
        a.TasaImpositiva = 0; 
        
        a.ConceptoImporte = 0; 
        
        a.ConceptoImporteGravado = 0; 
        
    } 
    
    public static testmethod void PuntoVentaItemBancoTest(){ 
        
        RemoteSiteModel.PuntoVentaItemBanco a = new RemoteSiteModel.PuntoVentaItemBanco(null,null); 
        
        a.OperacionBancariaCodigo = ''; 
        
        a.NroCheque = ''; 
        
        a.FechaCheque = ''; 
        
        a.FechaVencimientoCheque = ''; 
        
        a.ImporteACobrar = 0; 
        
        a.MonedaCobroCodigo = ''; 
        
    } 
    
    public static testmethod void PuntoVentaItemTarjetaTest(){ 
        
        RemoteSiteModel.PuntoVentaItemTarjeta a = new RemoteSiteModel.PuntoVentaItemTarjeta(null,null); 
        
        a.OperacionBancariaCodigo = ''; 
        
        a.NroCupon = ''; 
        
        a.FechaCupon = ''; 
        
        a.FechaVencimientoTarjeta = ''; 
        
        a.NumeroAutorizacionCupon = ''; 
        
        a.ImporteACobrar = 0; 
        
        a.MonedaCobroCodigo = ''; 
        
    } 
    
    public static testmethod void PuntoVentaItemEfectivoTest(){ 
        
        RemoteSiteModel.PuntoVentaItemEfectivo a = new RemoteSiteModel.PuntoVentaItemEfectivo(null,null); 
        
        a.CuentaCodigo = ''; 
        
        a.ImporteACobrar = 0; 
        
        a.MonedaCobroCodigo = ''; 
        
    } 
    
    public static testmethod void PuntoVentaItemOtrosTest(){ 
        
        RemoteSiteModel.PuntoVentaItemOtros a = new RemoteSiteModel.PuntoVentaItemOtros(null,null); 
        
        a.CuentaCodigo = ''; 
        
        a.DebeHaber = ''; 
        
        a.ImporteACobrar = 0; 
        
        a.MonedaCobroCodigo = ''; 
        
    } 
    
    public static testmethod void ItemRetencionCobranzaTest(){ 
        
        RemoteSiteModel.ItemRetencionCobranza a = new RemoteSiteModel.ItemRetencionCobranza(); 
        
        a.RetencionCodigo = ''; 
        
        a.Comprobante = ''; 
        
        a.ISAR = 0; 
        
        a.Importe = 0; 
        
        a.Fecha = ''; 
        
        a.CUIT = ''; 
        
    } 
    
    public static testmethod void PuntoVentaCotizacionTest(){ 
        
        RemoteSiteModel.PuntoVentaCotizacion a = new RemoteSiteModel.PuntoVentaCotizacion(); 
        
        a.MonedaCodigo = ''; 
        
        a.Cotizacion = 0; 
        
    } 
    
    public static testmethod void CobranzaTest(){ 
        
        RemoteSiteModel.Cobranza a = new RemoteSiteModel.Cobranza(); 
        
        a.IdentificacionExterna = ''; 
        
        a.EmpresaCodigo = ''; 
        
        a.NumeroComprobante = ''; 
        
        a.Proveedor = ''; 
        
        a.TransaccionTipoCodigo = ''; 
        
        a.TransaccionSubtipoCodigo = ''; 
        
        a.Fecha = ''; 
        
        a.Nombre = ''; 
        
        a.DiferenciaCambio = 0; 
        
        a.UsaCotizacionOrigen = 0; 
        
        a.Descripcion = ''; 
        
        a.CajaCodigo = ''; 
        
        a.CobradorCodigo = ''; 
        
        a.Banco = null; 
        
        a.Efectivo = null; 
        
        a.Tarjeta = null; 
        
        a.Otros = null; 
        
        a.Retenciones = null; 
        
        a.Cotizaciones = null; 
        
        a.CtaCte = null; 
        
    } 
    
    public static testmethod void BancoTest(){ 
        
        RemoteSiteModel.Banco a = new RemoteSiteModel.Banco(); 
        
        a.OperacionBancariaCodigo = ''; 
        
        a.CuentaCodigo = ''; 
        
        a.DebeHaber = 0; 
        
        a.ImporteMonTransaccion = 0; 
        
        a.MonedaCodigo = ''; 
        
        a.Descripcion = ''; 
        
        a.DocumentoFisicoID = 0; 
        
        a.NumeroDocumentoFisico = ''; 
        
        a.FechaDocumentoFisico = ''; 
        
        a.FechaVencimientoDocumentoFisico = ''; 
        
        a.BancoCodigo = ''; 
        
        a.DimensionDistribucion = null; 
        
    } 
    
    public static testmethod void EfectivoTest(){ 
        
        RemoteSiteModel.Efectivo a = new RemoteSiteModel.Efectivo(); 
        
        a.CuentaCodigo = ''; 
        
        a.DebeHaber = 0; 
        
        a.ImporteMonTransaccion = 0; 
        
        a.MonedaCodigo = ''; 
        
        a.Descripcion = ''; 
        
        a.DimensionDistribucion = null; 
        
    } 
    
    public static testmethod void TarjetaTest(){ 
        
        RemoteSiteModel.Tarjeta a = new RemoteSiteModel.Tarjeta(); 
        
        a.OperacionBancariaCodigo = ''; 
        
        a.CuentaCodigo = ''; 
        
        a.DebeHaber = 0; 
        
        a.ImporteMonTransaccion = 0; 
        
        a.MonedaCodigo = ''; 
        
        a.Descripcion = ''; 
        
        a.NumeroDocumentoFisico = ''; 
        
        a.DocumentoTitular = ''; 
        
        a.FechaDocumento = ''; 
        
        a.FechaVencimiento = ''; 
        
        a.NumeroAutorizacionTarjeta = ''; 
        
        a.NumeroTarjeta = ''; 
        
        a.NumeroLote = ''; 
        
        a.Titular = ''; 
        
        a.BancoCodigo = ''; 
        
    } 
    
    public static testmethod void OtroTest(){ 
        
        RemoteSiteModel.Otro a = new RemoteSiteModel.Otro(); 
        
        a.CuentaCodigo = ''; 
        
        a.DebeHaber = 0; 
        
        a.ImporteMonTransaccion = 0; 
        
        a.MonedaCodigo = ''; 
        
        a.Descripcion = ''; 
        
        a.DimensionDistribucion = null; 
        
    } 
    
    public static testmethod void RetencionTest(){ 
        
        RemoteSiteModel.Retencion a = new RemoteSiteModel.Retencion(); 
        
        a.RetencionCodigo = ''; 
        
        a.Importe = 0; 
        
        a.ISAR = 0; 
        
        a.ISARAcumulado = 0; 
        
        a.Fecha = ''; 
        
        a.NumeroRetencion = ''; 
        
    } 
    
    public static testmethod void CotizacionTest(){ 
        
        RemoteSiteModel.Cotizacion a = new RemoteSiteModel.Cotizacion(); 
        
        a.MonedaCodigo = ''; 
        
        a.Cotizacion = 0; 
        
    } 
    
    public static testmethod void CtaCteTest(){ 
        
        RemoteSiteModel.CtaCte a = new RemoteSiteModel.CtaCte(); 
        
        a.CuentaCodigo = ''; 
        
        a.DebeHaber = 0; 
        
        a.ImporteMonTransaccion = 0; 
        
        a.ImporteMonPrincipal = 0; 
        
        a.MonedaCodigo = ''; 
        
        a.Descripcion = ''; 
        
        a.AplicacionOrigen = ''; 
        
        a.DimensionDistribucion = null; 
        
    } 
}