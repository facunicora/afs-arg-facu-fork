@IsTest
private class IntegracionPlanDePagosControllerTest {
	public static testmethod void test(){
        insert new List<Finnegans_Integration__c>{new Finnegans_Integration__c(Name = 'Access Token', Enabled__c = true, Endpoint__c = 'Test', Client_Id__c = 'Test', Client_Secret__c = 'Test'),
            									  new Finnegans_Integration__c(Name = 'Api Cliente', Enabled__c = true, Endpoint__c = 'Test2', Client_Id__c = 'Test2', Client_Secret__c = 'Test2')};
        
        Test.setMock(HttpCalloutMock.class, new SingleRequestMock(200, 'Complete', 'Response'));
        
        Account acc = Util_Test.createPersonAccount();
        insert acc;
        acc = [SELECT Id, PersonContactId, PersonContact.LeadSource, LastName, AG_DNI__c
               FROM Account WHERE Id = :acc.Id];
        Contact con = new Contact(Id = acc.PersonContactId, LeadSource = 'Referido por Voluntario');
        update con;
        AG_Programa__c prog = Util_Test.createPrograma(Schema.SObjectType.AG_Programa__c.getRecordTypeInfosByName().get('Mayores de 18').getRecordTypeId());
        insert prog;
        AG_ParticipanteDelPrograma__c part = Util_Test.createParticipanteDelPrograma2(acc, null, prog);
        insert part;
        AG_PlanDePago__c plan = new AG_PlanDePago__c(Participante__c = acc.Id, AG_ParticipanteDelPrograma__c = part.Id);
        insert plan;
        IntegracionPlanDePagosController.enviarPlanDePagos(plan.Id);
        
        AG_Pago__c pago = new AG_Pago__c(AG_PlanDePago__c = plan.Id, Divisa__c = 'Peso argentino.', 
                                         AG_FechaDeAcreditacion__c = Date.today(), AG_Importe__c = 5000);
        insert pago;
        
        Test.startTest();
        IntegracionPlanDePagosController.enviarPlanDePagos(plan.Id);
        Test.stopTest();
        
        system.assertEquals(1, [SELECT COUNT() FROM Integration_Log__c]);
        system.assertEquals('Response', [SELECT Response__c FROM Integration_Log__c LIMIT 1].Response__c);
        system.assertEquals(true, [SELECT PersonContact.Enviado_a_Ceres__c FROM Account LIMIT 1].PersonContact.Enviado_a_Ceres__c);
    }
}