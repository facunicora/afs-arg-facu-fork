/*
 * Clase programable para mostrar los registros en la comunidad completando los campos necesarios 
 * para que por medio de las reglas de colaboracion de comunidad el participante pueda verlos
 */
global class showRecordsOnCommunity_schedulable implements Schedulable {
    public static boolean isExecuting = false;
    
    global void execute(SchedulableContext SC) {
        isExecuting = true;
        Date days_45 = System.Today() - 45;         
        List<AG_Pasajes__c> lstPasajes = new List<AG_Pasajes__c>();        
        Set<String> setCountries = new Set<String>();
        for(Paises_Excluidos_Publicacion_Pasajes__c pepp : Paises_Excluidos_Publicacion_Pasajes__c.getAll().values()){
        	setCountries.add(pepp.Nombre_del_pais__c);        
        }
        
        //Obtengo los pasajes que cumplan las condiciones
        for(AG_Pasajes__c pas : [SELECT AG_ParticipanteDelPrograma__r.AG_Participante__c, Pasajero__c, Publicar_pasaje__c FROM AG_Pasajes__c 
                                          WHERE Pasajero__c = null AND AG_ParticipanteDelPrograma__r.AG_Programa__r.AG_PaisdelPrograma__c NOT in :setCountries 
                                 		  AND CG_TravelPlan__r.CG_FechaHoraArriboTP__c > :days_45]){
        	//Marcar el checkbox publicar pasaje.
            pas.Publicar_pasaje__c = true;
            pas.Pasajero__c = pas.AG_ParticipanteDelPrograma__r.AG_Participante__c; 
                                              
            lstPasajes.add(pas);
        }
        
        //Actualizo los pasajes
        List<Database.SaveResult> lstSR = Database.Update(lstPasajes,false);
        for(Integer i = 0; i < lstSR.size(); i++){
            Database.SaveResult sr = lstSR[i];
            if(!sr.isSuccess()){
                system.debug(lstPasajes[i].Id + ' --> ' + String.join(sr.getErrors(),','));
            }
        }
	}
}