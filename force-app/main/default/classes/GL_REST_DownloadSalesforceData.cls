public class GL_REST_DownloadSalesforceData {
     
  // call the gl service
    @future(callout=true)
    public static void triggerDownload(String qID, string SFID) {
        string result;         
        string errorTxt;
            
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            try{
                // set the method
                req.setMethod('GET');
                // generate the url for the request
                String url = 'https://test.afsglobal.org/AFSGlobal/CoreFunction/SalesforceSyncARG.ashx?qID=' + qID + '&SFID=' + SFID;
                if(!runningInASandbox()){
                    url = 'https://www.afsglobal.org/AFSGlobal/CoreFunction/SalesforceSyncARG.ashx?qID=' + qID + '&SFID=' + SFID;
                }
                System.debug('url api:' + url);
                req.setHeader('authenticationToken', 'SF_ARG:vrqq58p6jm5cx238abh6c8m749s2');
                req.setHeader('Content-Type', 'application/json');
                req.setHeader('Type', 'download');
                req.setTimeOut(120000);
                
                // add the endpoint to the request
                req.setEndpoint(url);
                
                
                // create the response object
                HTTPResponse resp = http.send(req);
                
                Map<String, Object> r = (Map<String, Object>)System.JSON.deserializeUntyped(resp.getBody());
                result=(String)r.get('status');
                errorTxt=(String)r.get('description');
                
            }catch(exception e){
                errorTxt = e.getMessage();
                result = 'Error';
            }
            System.debug('result==' + result);        
          
    }
 
    private static Boolean runningInASandbox() {
      return [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
}