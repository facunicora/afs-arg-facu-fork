/***
 * Name: PasajeTriggerHandler
 * Created Date: 2019/01/18
 * Created By: Hendercross
 * Description: Contains Pasaje Trigger logic 
***/
public class PasajeTriggerHandler {
    /***
     * Description: Contains logic for Before Insert event
     * Parameters: Trigger.newMap
    ***/
    public static void beforeInsert(List<AG_Pasajes__c> lstNew){
        populatePasajero(lstNew, null, true);
        PasajeTriggers.createTravelPlanVuelo();
    }
    
    /***
     * Description: Contains logic for Before Update event
     * Parameters: Trigger.newMap
    ***/
    public static void beforeUpdate(Map<Id,AG_Pasajes__c> mapNew,Map<Id,AG_Pasajes__c> mapOld){
		populatePasajero(mapNew.Values(), mapOld, false);
    }    
  
    /***
     * Description: Contains logic for After Insert event
     * Parameters: Trigger.newMap
    ***/
    public static void afterInsert(Map<Id,AG_Pasajes__c> mapNew){
        checkPasajeComprado(mapNew,null,true,false);
    }
    
    /***
     * Description: Contains logic for After Update event
     * Parameters: Trigger.newMap
    ***/
    public static void afterUpdate(Map<Id,AG_Pasajes__c> mapNew,Map<Id,AG_Pasajes__c> mapOld){
        checkPasajeComprado(mapNew,mapOld,false,false);
    }
    
    /***
     * Description: Contains logic for After Update event
     * Parameters: Trigger.newMap
    ***/
    public static void afterDelete(Map<Id,AG_Pasajes__c> mapOld){
        checkPasajeComprado(null,mapOld,false,true);
    }
    
    /***
	* Description: Mark the checkbox 'Tiene Pasaje Comprado' of Travel x Partipante related record, when a Pasaje is created or Participante del Programa field is changed or Travel Plan field is changed.
    * Parameters: Trigger.newMap
    ***/
    public static void checkPasajeComprado(Map<Id,AG_Pasajes__c> mapNew, Map<Id,AG_Pasajes__c> mapOld, Boolean isInsert, Boolean isDelete){
        Set<Id> setPartProgId = new Set<Id>();
        Set<Id> setTravelPlanId = new Set<Id>();
        Set<String> setConcatenateKeysToTrue = new Set<String>();
        Set<String> setConcatenateKeysToFalse = new Set<String>();
        List<AG_TravelPlanxParticipante__c> lstTravelPlanXPart = new List<AG_TravelPlanxParticipante__c>();
        
        if(!isDelete){
            for(AG_Pasajes__c pas : mapNew.values()){
                if(pas.AG_ParticipanteDelPrograma__c != null && pas.CG_TravelPlan__c != null && (isInsert || pas.AG_ParticipanteDelPrograma__c != mapOld.get(pas.Id).AG_ParticipanteDelPrograma__c || pas.CG_TravelPlan__c != mapOld.get(pas.Id).CG_TravelPlan__c)){
                    setPartProgId.add(pas.AG_ParticipanteDelPrograma__c);                
                    setTravelPlanId.add(pas.CG_TravelPlan__c);
                    setConcatenateKeysToTrue.add((String)pas.AG_ParticipanteDelPrograma__c + (String)pas.CG_TravelPlan__c);
                }
            }
      	}

        if(!isInsert){
            for(AG_Pasajes__c pas : mapOld.values()){
                if(pas.AG_ParticipanteDelPrograma__c != null && pas.CG_TravelPlan__c != null && (isDelete || pas.AG_ParticipanteDelPrograma__c != mapNew.get(pas.Id).AG_ParticipanteDelPrograma__c || pas.CG_TravelPlan__c != mapNew.get(pas.Id).CG_TravelPlan__c)){
                    setPartProgId.add(pas.AG_ParticipanteDelPrograma__c);                
                    setTravelPlanId.add(pas.CG_TravelPlan__c);
                    setConcatenateKeysToFalse.add((String)pas.AG_ParticipanteDelPrograma__c + (String)pas.CG_TravelPlan__c);
                }
            }            
        }
        
        for(AG_TravelPlanxParticipante__c tpxp : [SELECT Id, AG_ParticipantePrograma__c, AG_TravelPlanxParticipante__c, Tiene_pasaje_comprado__c 
                                                  FROM AG_TravelPlanxParticipante__c WHERE AG_ParticipantePrograma__c IN :setPartProgId AND AG_TravelPlanxParticipante__c IN :setTravelPlanId]){
        	if(setConcatenateKeysToTrue.contains((String)tpxp.AG_ParticipantePrograma__c + (String)tpxp.AG_TravelPlanxParticipante__c)){
            	tpxp.Tiene_pasaje_comprado__c = true;
                lstTravelPlanXPart.add(tpxp);                                              
            }
            if(setConcatenateKeysToFalse.contains((String)tpxp.AG_ParticipantePrograma__c + (String)tpxp.AG_TravelPlanxParticipante__c)){
            	tpxp.Tiene_pasaje_comprado__c = false;
                lstTravelPlanXPart.add(tpxp);                                              
            }                                          
        }
        
        if(!lstTravelPlanXPart.isEmpty()){
            List<Database.SaveResult> lstSR = Database.Update(lstTravelPlanXPart,false);
            for(Integer i = 0; i < lstSR.size();i++){
				AG_TravelPlanxParticipante__c tpxp = lstTravelPlanXPart[i];
				Database.SaveResult sr = lstSR[i];               
                if(!sr.isSuccess()){
                    system.debug(logginglevel.error,(String)tpxp.Id + ' --> ' + String.Join(sr.getErrors(),','));
                }
            }
        }
	}
    
    /***
	* Descripción: Completar el campo Pasajero__c si el checkbox publicar pasaje este checkeado 
	* (esto se hace para poder mostrar el pasaje en la comunidad)
    ***/
    public static void populatePasajero(List<AG_Pasajes__c> lstNew, Map<Id,AG_Pasajes__c> mapOld, Boolean isInsert){
        //Si se esta ejecutando la clase scheduleable saltar este metodo porque hace lo mismo.
        if(showRecordsOnCommunity_schedulable.isExecuting){
            return;
        }
        
        Set<Id> setPartProgId = new Set<Id>();
        List<AG_Pasajes__c> lstPasajesToEdit = new List<AG_Pasajes__c>();
        
        for(AG_Pasajes__c pas : lstNew){
            if(pas.publicar_pasaje__c && (isInsert || !mapOld.get(pas.Id).publicar_pasaje__c) && pas.AG_ParticipanteDelPrograma__c != null){
                setPartProgId.add(pas.AG_ParticipanteDelPrograma__c);
                lstPasajesToEdit.add(pas);
            }
        }
        
        if(!setPartProgId.isEmpty()){
            Map<Id,AG_ParticipanteDelPrograma__c> mapPartProgById = new Map<Id,AG_ParticipanteDelPrograma__c>([SELECT Id, AG_Participante__c FROM AG_ParticipanteDelPrograma__c WHERE Id in :setPartProgId]);
            for(AG_Pasajes__c pas : lstPasajesToEdit){
                pas.Pasajero__c = mapPartProgById.get(pas.AG_ParticipanteDelPrograma__c).AG_Participante__c;
            }
        }
    }
}