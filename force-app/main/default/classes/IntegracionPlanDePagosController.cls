global class IntegracionPlanDePagosController {
	@AuraEnabled
    global static String enviarPlanDePagos(string planId){
        List<String> lstFields = new List<String>(Schema.getGlobalDescribe().get('AG_Pago__c').getDescribe().fields.getMap().keySet());
        List<AG_Pago__c> pagos =  Database.query('SELECT '+String.join(lstFields, ',')+', AG_PlanDePago__r.AG_ParticipanteDelPrograma__r.AG_Participante__c '+
                                                 'FROM AG_Pago__c WHERE AG_PlanDePago__c = :planId AND Enviado_a_Ceres__c = false AND '+
                                                 'AG_PlanDePago__r.AG_ParticipanteDelPrograma__c != NULL');
        
        if(!pagos.isEmpty()){
            System.enqueueJob(new RemoteSiteManagerQueueable('sendPersonalAccount', 
                                                             pagos[0].AG_PlanDePago__r.AG_ParticipanteDelPrograma__r.AG_Participante__c, 
                                                             pagos));
        }else{
            return 'No hay pagos para enviar';
        }
        
        return 'OK';
    }
}