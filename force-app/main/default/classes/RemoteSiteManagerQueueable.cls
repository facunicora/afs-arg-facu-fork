public class RemoteSiteManagerQueueable implements Queueable, Database.AllowsCallouts{
    private String nextStep;
    private Id accId;
    private List<AG_Pago__c> pagos;
    
    public RemoteSiteManagerQueueable(String next, Id accountId, List<AG_Pago__c> pagosList) {
        nextStep = next;
        accId = accountId;
        pagos = pagosList == null? new List<AG_Pago__c>() : pagosList;
    }
    
    public void execute(QueueableContext context) {
        String token = RemoteSiteManager.getToken(false);
        
        if(!String.isEmpty(token)){
            switch on nextStep{
                when 'sendPersonalAccount'{
                    Finnegans_Integration__c fInt = Finnegans_Integration__c.getInstance('Api Cliente');
                    if(fInt == null || !fInt.enabled__c){
                        NotificacionChatter.postearPP('Error al enviar Cliente. Punto de integracion no habilitado', accId);
                        return;
                        //TODO: MANDAR NOTIFICACION CHATTER (ACCOUNT FORMATO INVALIDO)
                    }
                    
                    Account acc = [SELECT Id, IsPersonAccount, AG_DNI__c, name, PersonEmail, PersonContact.Situacion_IIBB__c, 
                                   		  PersonContact.Categoria_Fiscal_Codigo__c, PersonContact.Constancia_Inscripcion_Afip__c, 
                                   		  PersonContact.Identificacion_Tributaria_Codigo__c, PersonMailingCountry, 
                                   		  PersonContact.Condicion_Pago_Codigo__c, PersonContact.Enviado_a_Ceres__c, PersonContactId
                                   FROM Account WHERE ID = :accId];
                    if(!acc.IsPersonAccount){
                        NotificacionChatter.postearPP('Error al enviar Cliente. Formato invalido de Cuenta', accId);
                        return;
                        //TODO: MANDAR NOTIFICACION CHATTER (ACCOUNT FORMATO INVALIDO)
                    }
                    
                    if(acc.PersonContact.Enviado_a_Ceres__c){
                        ID jobID = Test.isRunningTest()? null : System.enqueueJob(new RemoteSiteManagerQueueable('sendCobranza', null, pagos));
                        return; 
                    }
                    
                    if(acc.AG_DNI__c == null){
                        NotificacionChatter.postearPP('Error al enviar Cliente. Dni requerido.', accId);
                        return;
                        //TODO: MANDAR NOTIFICACION CHATTER (DNI REQUERIDO)
                    }
                    
                    AG_PlanDePago__c plan = [SELECT ID_Externo_PPago__c, AG_ParticipanteDelPrograma__r.AG_Programa__r.RecordType.DeveloperName 
                                           FROM AG_PlanDePago__c WHERE Id = :pagos[0].AG_PlanDePago__c];
                    
                    String json = RemoteSiteParser.AccountToCliente(acc, plan);
                    
                    HttpResponse response = RemoteSiteConnection.postObject(fInt.Endpoint__c, token, json, null);
                    if(response.getStatusCode() == 200){
                        update new Contact(Id = acc.PersonContactId, Enviado_a_Ceres__c = true);
                        
                        ID jobID = pagos.isEmpty() || Test.isRunningTest() ? 
                            	   null : System.enqueueJob(new RemoteSiteManagerQueueable('sendCobranza', null, pagos));
                    }else{
                        NotificacionChatter.postearPP('Error al enviar Cliente.', accId);
                        //TODO: MANDAR NOTIFICACION CHATTER (ERROR ENVIO)
                    }
                }
                when 'sendCobranza'{
                    AG_Pago__c pago = pagos[0];
                    Id planDePagoId = pago.AG_PlanDePago__c;
                    
                    Finnegans_Integration__c fInt = Finnegans_Integration__c.getInstance('Api Cobranza');
                    if(fInt == null || !fInt.enabled__c){
                        NotificacionChatter.postearPP('Error al enviar Pagos. Punto de integracion no habilitado', planDePagoId);
                        return;
                        //TODO: MANDAR NOTIFICACION CHATTER (ACCOUNT FORMATO INVALIDO)
                    }
                    
                    List<String> lstFields = new List<String>(Schema.getGlobalDescribe().get('AG_PlanDePago__c').getDescribe().fields.getMap().keySet());
                    AG_PlanDePago__c planDePago = Database.query('SELECT ' + String.join(lstFields, ',') + ', AG_ParticipanteDelPrograma__r.AG_Programa__c, AG_ParticipanteDelPrograma__r.AG_Participante__r.PersonContactId '+
                                                                 'FROM AG_PlanDePago__c WHERE Id = :planDePagoId');
                    
                    Id programId = planDePago.AG_ParticipanteDelPrograma__r.AG_Programa__c;
                    
                    lstFields = new List<String>(Schema.getGlobalDescribe().get('AG_Programa__c').getDescribe().fields.getMap().keySet());
                    AG_Programa__c programa = Database.query('SELECT '+String.join(lstFields,',')+',recordType.DeveloperName  FROM AG_Programa__c WHERE Id = :programId');
                    
                    if(String.isBlank(planDePago.AG_ParticipanteDelPrograma__r.AG_Participante__r.PersonContactId)){
                        NotificacionChatter.postearPP('Error al enviar Pagos. Formato invalido de Cuenta', planDePagoId);
                        return;
                        //TODO: MANDAR NOTIFICACION CHATTER (ERROR ACCOUNT MAL FORMATEADA)
                    }
                    
                    String json = RemoteSiteParser.serializeCobranza(planDePago, new List<AG_Pago__c>{pago}, programa, fInt.EmpresaCodigo__c);
                    
                    HttpResponse response = RemoteSiteConnection.postObject(fInt.Endpoint__c, token, json, null);
                    if(response.getStatusCode() == 200){
                        pago.Enviado_a_Ceres__c = true;
                        update pago;
                    }else{
                        NotificacionChatter.postearPP('Error al enviar Pago.', pago.Id);
                        //TODO: MANDAR NOTIFICACION CHATTER (ERROR ENVIO)
                    }
                    
                    pagos.remove(0);
                    ID jobID = pagos.isEmpty() || Test.isRunningTest() ? 
                        	   null : System.enqueueJob(new RemoteSiteManagerQueueable('sendCobranza', null, pagos));
                }
            }
        }
    }
}