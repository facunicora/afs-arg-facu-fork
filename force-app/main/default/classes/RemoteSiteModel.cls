global class RemoteSiteModel {
    
    public class ResponseFail{
        public string status {get; set;}
        public string error {get; set;}
    }
    
    //-------------- Cliente ----------------------------

	public class Cliente{
    	public String Nombre {get; set;}
    	public String Codigo {get; set;}
    	public Boolean Activo {get; set;}
    	public String RazonSocial {get; set;}
    	public String Descripcion {get; set;}
    	public String Email {get; set;}
    	public String PaginaWeb {get; set;}
    	public String CategoriaFiscalCodigo {get; set;}
    	public String IdentificacionTributariaCodigo {get; set;}
    	public String Numero_de_identificacion_tributaria_99999XVC {get; set;}	//Número de identificación tributaria
    	public String ConstanciaInscripcionAfip {get; set;}
    	public String FechaSolicitadaConstanciaInscripcionAFIP {get; set;}
    	public String FechaVencimientoConstanciaInscripcionAFIP {get; set;}
    	public String IdentTributariaPaisOrigen {get; set;}
    	public Integer TipoPersona {get; set;}
    	public String ClaveBancariaUnica {get; set;}
    	public Integer CantEmpleados {get; set;}
    	public String IngresosAnuales {get; set;}
    	public Integer SituacionIIBB {get; set;}
    	public String NroInscripcionIIBB {get; set;}
    	public Decimal PorcPercepcionIIBB {get; set;}
    	public String VendedorCodigo {get; set;}
    	public Decimal CreditoMaximo {get; set;}
    	public String OrganizacionImagen {get; set;}
    	public String EsProveedor {get; set;}
    	public String ConceptoClienteCodigo {get; set;}
    	public String CuentaClienteCodigo {get; set;}
    	public String ListaPrecioDefaultCodigo {get; set;}
    	public String OriginacionCodigo {get; set;}
    	public String MercadoCodigo {get; set;}
    	public Boolean RestriccionCondPagos {get; set;}
    	public String ControlValores {get; set;}
    	public String DiasMaximoValores {get; set;}
    	public Integer DiferenciaCambio {get; set;}
    	public List<CondicionPago> CondicionesPago {get; set;}
    	public List<Direccion> Direcciones {get; set;}
    	public List<Telefono> Telefonos {get; set;}
    	public List<Percepcion> Percepciones {get; set;}
    	public List<ContactoItem> ContactoItems {get; set;}
    	public List<ProvinciaItem> ProvinciaItems {get; set;}
    	public List<DominioItem> DominioItems {get; set;}
    }
    
    public class CondicionPago{
    	public String CondicionPagoCodigo {get; set;}
    	public Boolean Default_nombre_99999XVC {get; set;}	//Default
    }
    
    public class Direccion{
    	public String PaisCodigo {get; set;}
    	public String ProvinciaCodigo {get; set;}
    	public String LocalidadCodigo {get; set;}
    	public String CodigoPostal {get; set;}
    	public String Calle {get; set;}
    	public String Numero {get; set;}
    	public String Dpto {get; set;}
    	public String Piso {get; set;}
    	public Integer Tipo {get; set;}
    	public String Descripcion {get; set;}
    	public Boolean Principal {get; set;}
        
        public Direccion(String pais, String prov, String loc, String codPost, String call){
            PaisCodigo = pais;
            ProvinciaCodigo = prov;
            LocalidadCodigo = loc;
            CodigoPostal = codPost;
            Calle = call;
        }
    }
    
    public class Telefono{
    	public String Numero {get; set;}
    	public String Tipo {get; set;}
        
        public Telefono(String n, String t){
            Numero = n;
            Tipo = t;
        }
    }
    
    public class Percepcion{
    	public String TipoRetencionCodigo {get; set;}
    	public String RetencionCodigo {get; set;}
    	public String FechaDesde {get; set;}
    	public String FechaHasta {get; set;}
    	public Decimal Porcentaje {get; set;}
    	public Integer Modo {get; set;}
    	public String MomentoAplicacionCodigo {get; set;}
    	public Integer Motivo {get; set;}
    	public Boolean Manual {get; set;}
    }
    
    public class ContactoItem{
    	public Integer OrganizacionID {get; set;}
    	public String PersonaID {get; set;}
    	public Boolean EsPrincipal {get; set;}
    	public String CargoID {get; set;}
    }
    
    public class ProvinciaItem{
    	public Integer ControlImpositivo1 {get; set;}
    	public String ProvinciaID {get; set;}
        
        public ProvinciaItem(Integer c, String p){
            ControlImpositivo1 = c;
            ProvinciaID = p;
        }
    }
    
    public class DominioItem{
    	public String Dominio {get; set;}
        
        public DominioItem(String d){
            Dominio = d;
        }
    }
    
    //---------------- PuntoDeVenta ---------------------
    
    public class PuntoDeVenta{
        public String IdentificacionExterna {get; set;}
        public String Fecha {get; set;}
        public String ClienteCodigo {get; set;}
        public String CondicionPagoCodigo {get; set;}
        public String MonedaCodigo {get; set;}
        public String ComprobanteTipoImpositivoID {get; set;}
        public String TransaccionTipoCodigo {get; set;}
        public String TransaccionSubtipoCodigo {get; set;}
        public String WorkflowCodigo {get; set;}
        public String Descripcion {get; set;}
        public String NumeroComprobante {get; set;}
        public String EmpresaCodigo {get; set;}
        public Integer CAINumero {get; set;}
        public String CAIFechaVto {get; set;}
        public List<ProductoPtoVenta> Productos {get; set;}
        public List<ConceptoPtoVenta> Conceptos {get; set;}
        public List<PuntoVentaItemBanco> PuntoVentaItemsBanco {get; set;}
        public List<PuntoVentaItemTarjeta> PuntoVentaItemsTarjeta {get; set;}
        public List<PuntoVentaItemEfectivo> PuntoVentaItemsEfectivo {get; set;}
        public List<PuntoVentaItemOtros> PuntoVentaItemsOtros {get; set;}
        public List<ItemRetencionCobranza> ItemsRetencionCobranza {get; set;}
        public List<PuntoVentaCotizacion> Cotizaciones {get; set;}
    }
    
    public class ProductoPtoVenta{
        public String ProductoCodigo {get; set;}
        public Decimal Precio {get; set;}
        public Integer Cantidad {get; set;}
        public String Descripcion {get; set;}
        public String CantidadStock2 {get; set;}
        public String PrecioTipo {get; set;}
        public List<DimensionDistribucion> DimensionDistribucion {get; set;}
        public String vinculacionOrigen {get; set;}
    }
    
    public class DimensionDistribucion{
        public String dimensionCodigo {get; set;}
        public String distribucionCodigo {get; set;}
        public List<DistribucionItem> distribucionItems {get; set;}
    }
    
    public class DistribucionItem{
        public String codigo {get; set;}
        public Decimal porcentaje {get; set;}
    }
    
    public class ConceptoPtoVenta{
        public String ConceptoCodigo {get; set;}
        public Decimal TasaImpositiva {get; set;}
        public Decimal ConceptoImporte {get; set;}
        public Decimal ConceptoImporteGravado {get; set;}
    }
    
    public class PuntoVentaItemBanco{
        public String OperacionBancariaCodigo {get; set;}
        public String NroCheque {get; set;}
        public String FechaCheque {get; set;}
        public String FechaVencimientoCheque {get; set;}
        public Decimal ImporteACobrar {get; set;}
        public String MonedaCobroCodigo {get; set;}
        
        public PuntoVentaItemBanco(Decimal importe, String divisa){
            ImporteACobrar = importe;
            MonedaCobroCodigo = divisa;
        }
    }
    
    public class PuntoVentaItemTarjeta{
        public String OperacionBancariaCodigo {get; set;}
        public String NroCupon {get; set;}
        public String FechaCupon {get; set;}
        public String FechaVencimientoTarjeta {get; set;}
        public String NumeroAutorizacionCupon {get; set;}
        public Decimal ImporteACobrar {get; set;}
        public String MonedaCobroCodigo {get; set;}
        
        public PuntoVentaItemTarjeta(Decimal importe, String divisa){
            ImporteACobrar = importe;
            MonedaCobroCodigo = divisa;
        }
    }
    
    public class PuntoVentaItemEfectivo{
        public String CuentaCodigo {get; set;}
        public Decimal ImporteACobrar {get; set;}
        public String MonedaCobroCodigo {get; set;}
        
        public PuntoVentaItemEfectivo(Decimal importe, String divisa){
            ImporteACobrar = importe;
            MonedaCobroCodigo = divisa;
        }
    }
    
    public class PuntoVentaItemOtros{
        public String CuentaCodigo {get; set;}
        public String DebeHaber {get; set;}
        public Decimal ImporteACobrar {get; set;}
        public String MonedaCobroCodigo {get; set;}
        
        public PuntoVentaItemOtros(Decimal importe, String divisa){
            ImporteACobrar = importe;
            MonedaCobroCodigo = divisa;
        }
    }
    
    public class ItemRetencionCobranza{
        public String RetencionCodigo {get; set;}
        public String Comprobante {get; set;}
        public Decimal ISAR {get; set;}
        public Decimal Importe {get; set;}
        public String Fecha {get; set;}
        public String CUIT {get; set;}
    }
    
    public class PuntoVentaCotizacion{
        public String MonedaCodigo {get; set;}
        public Decimal Cotizacion {get; set;}
    }
    
    
    //---------------- Cobranzas ---------------------
    
    public class Cobranza{
        public String IdentificacionExterna {get; set;}
        public String EmpresaCodigo {get; set;}
        public String NumeroComprobante {get; set;}
        public String Proveedor {get; set;}
        public String TransaccionTipoCodigo {get; set;}
        public String TransaccionSubtipoCodigo {get; set;}
        public String Fecha {get; set;}
        public String Nombre {get; set;}
        public Integer DiferenciaCambio {get; set;}
        public Integer UsaCotizacionOrigen {get; set;}
        public String Descripcion {get; set;}
        public String CajaCodigo {get; set;}
        public String CobradorCodigo {get; set;}
        public List<Banco> Banco {get; set;}
        public List<Efectivo> Efectivo {get; set;}
        public List<Tarjeta> Tarjeta {get; set;}
        public List<Otro> Otros {get; set;}
        public List<Retencion> Retenciones {get; set;}
        public List<Cotizacion> Cotizaciones {get; set;}
        public List<CtaCte> CtaCte {get; set;}
    }
    
    public class Banco{
        public String OperacionBancariaCodigo {get; set;}
        public String CuentaCodigo {get; set;}
        public Integer DebeHaber {get; set;}
        public Decimal ImporteMonTransaccion {get; set;}
        public String MonedaCodigo {get; set;}
        public String Descripcion {get; set;}
        public Integer DocumentoFisicoID {get; set;}
        public String NumeroDocumentoFisico {get; set;}
        public String FechaDocumentoFisico {get; set;}
        public String FechaVencimientoDocumentoFisico {get; set;}
        public String BancoCodigo {get; set;}
        public List<DimensionDistribucion> DimensionDistribucion {get; set;}
    }
    
    public class Efectivo{
        public String CuentaCodigo {get; set;}
        public Integer DebeHaber {get; set;}
        public Decimal ImporteMonTransaccion {get; set;}
        public String MonedaCodigo {get; set;}
        public String Descripcion {get; set;}
        public List<DimensionDistribucion> DimensionDistribucion {get; set;}
    }
    
    public class Tarjeta{
        public String OperacionBancariaCodigo {get; set;}
        public String CuentaCodigo {get; set;}
        public Integer DebeHaber {get; set;}
        public Decimal ImporteMonTransaccion {get; set;}
        public String MonedaCodigo {get; set;}
        public String Descripcion {get; set;}
        public String NumeroDocumentoFisico {get; set;}
        public String DocumentoTitular {get; set;}
        public String FechaDocumento {get; set;}
        public String FechaVencimiento {get; set;}
        public String NumeroAutorizacionTarjeta {get; set;}
        public String NumeroTarjeta {get; set;}
        public String NumeroLote {get; set;}
        public String Titular {get; set;}
        public String BancoCodigo {get; set;}
    }
    
    public class Otro{
        public String CuentaCodigo {get; set;}
        public Integer DebeHaber {get; set;}
        public Decimal ImporteMonTransaccion {get; set;}
        public String MonedaCodigo {get; set;}
        public String Descripcion {get; set;}
        public List<DimensionDistribucion> DimensionDistribucion {get; set;}
    }
    
    public class Retencion{
        public String RetencionCodigo {get; set;}
        public Decimal Importe {get; set;}
        public Decimal ISAR {get; set;}
        public Decimal ISARAcumulado {get; set;}
        public String Fecha {get; set;}
        public String NumeroRetencion {get; set;}
    }
    
    public class Cotizacion{
        public String MonedaCodigo {get; set;}
        public Decimal Cotizacion {get; set;}
    }
    
    public class CtaCte{
        public String CuentaCodigo {get; set;}
        public Integer DebeHaber {get; set;}
        public Decimal ImporteMonTransaccion {get; set;}
        public Decimal ImporteMonPrincipal {get; set;}
        public String MonedaCodigo {get; set;}
        public String Descripcion {get; set;}
        public String AplicacionOrigen {get; set;}
        public List<DimensionDistribucion> DimensionDistribucion {get; set;}
    }
    
    //------------------ Producto ---------------------------
/*    
    public class Producto{
    	public String Codigo {get; set;}
    	public Boolean Activo {get; set;}
    	public String Descripcion {get; set;}
    	public String ProductoTipo {get; set;}
    	public String PosicionArancelariaCodigo {get; set;}
    	public Boolean EsStockeable {get; set;}
    	public String MonedaCodigo {get; set;}
    	public Decimal Peso {get; set;}
    	public Decimal Volumen {get; set;}
    	public Decimal CodigoMtx {get; set;}
    	public Decimal UnidadesMtx {get; set;}
    	public String CodigoMercosur {get; set;}
    	public String ProductoRubroCodigo {get; set;}
    	public String ProductoFamiliaCodigo {get; set;}
    	public String ProductoSubFamiliaCodigo {get; set;}
    	public String MarcaCodigo {get; set;}
    	public Boolean UtilizaDespachoImportacion {get; set;}
    	public String ProductoImagen {get; set;}
    	public String CuentaCodigoVenta {get; set;}
    	public String ConceptoCodigoVenta {get; set;}
    	public String UnidadCodigoVenta {get; set;}
    	public Decimal RelacionUnidadVentaStock {get; set;}
    	public String TasaImpositivaCodigoVenta {get; set;}
    	public Decimal PorcentajeComision {get; set;}
    	public String ActividadIVACodigo {get; set;}
    	public String CuentaCodigoCompra {get; set;}
    	public String ConceptoCodigoCompra {get; set;}
    	public String UnidadCodigoCompra {get; set;}
    	public Decimal RelacionUnidadCompraStock {get; set;}
    	public String TasaImpositivaCodigoCompra {get; set;}
    	public Integer CategoriaSIAP {get; set;}
    	public String ProveedorCodigoPrincipal {get; set;}
    	public String ConceptoCodigoLogistica {get; set;}
    	public Boolean ManejaStockOrganizaciones {get; set;}
    	public Integer NoControlaStock {get; set;}
    	public Decimal CodigoBarra {get; set;}
    	public String ProductoCodigoEnvase {get; set;}
    	public Boolean EsElaborado {get; set;}
    	public Boolean EsKit {get; set;}
    	public Boolean UtilizaPartidas {get; set;}
    	public String PartidaTipoCodigo {get; set;}
    	public Boolean UtilizaNumerosSerie {get; set;}
    	public Boolean ControlaStockPorNumeroSerie {get; set;}
    	public String UnidadCodigoStock1 {get; set;}
    	public String UnidadCodigoStock2 {get; set;}
    	public Decimal RelacionUnidadSecundaria {get; set;}
    	public Decimal RelacionUnidadTransporte {get; set;}
    	public List<ComposicionKit> ComposicionKit {get; set;}
    	public List<Deposito> Depositos {get; set;}
    	public List<Retencion> Retenciones {get; set;}
    	public List<Dimension> Dimensiones {get; set;}
    	public List<CodigoDeBarras> Codigo_de_Barras_99999XVC {get; set;}	//Codigo de Barras
    	public List<TasaImpositiva> Tasas_Impositivas_99999XVC {get; set;}	//Tasas Impositivas
    	public List<ConceptoValorizacion> ConceptosValorizacion {get; set;}
    	public Decimal ImporteImpuestosInternos {get; set;}
    	public Decimal CostoStandard {get; set;}
    	public String FechaCostoStandard {get; set;}
    	public Decimal PrecioBaseVenta {get; set;}
    	public Boolean ActualizaVNR {get; set;}
    	public Integer UnidadValorizacion {get; set;}
    	public Boolean UsaFactorMultiplicacion {get; set;}
    	public Decimal FactorMultiplicacion {get; set;}
    	public String CantidadRaciones {get; set;}
    	public String CantidadMateriaSeca {get; set;}    
    }
    
    public class ComposicionKit{
    	public Decimal Cantidad {get; set;}
    	public String ProductoCodigoKit {get; set;}
    }
    
    public class Deposito{
    	public String DepositoCodigo {get; set;}
    	public Decimal StockMinimo {get; set;}
    	public Decimal PuntoReposicion {get; set;}
    	public Decimal StockMaximo {get; set;}
    	public Boolean NoControlaStock {get; set;}
    }
    
    public class Retencion{
    	public String TipoRetencionCodigo {get; set;}
    	public String RetencionCodigo {get; set;}
    }
    
    public class Dimension{
    	public String DimensionCodigo {get; set;}
    	public String DimensionDistribucionCodigo {get; set;}
    }
    
    public class CodigoDeBarras{
    	public Integer Orden {get; set;}
    	public String CodigoBarra {get; set;}
    	public Decimal Multiplicador {get; set;}
    	public String UnidadCodigoPresentacion {get; set;}
    }
    
    public class TasaImpositiva{
    	public String TasaImpositivaCodigo {get; set;}
    }
    
    public class ConceptoValorizacion{
    	public String ConceptoValorizacionTipoCodigo {get; set;}
    	public Boolean ConceptoValorizacionCodigo {get; set;}
    }
*/
}