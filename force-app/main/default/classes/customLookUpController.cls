public class customLookUpController {
    @AuraEnabled
    public static List <sObject> fetchLookUpValues(String searchKeyWord, String ObjectName, String fromWhere, String param) {
        system.debug('ObjectName-->' + ObjectName);
        String searchKey = searchKeyWord + '%';
        String moreConditions = '';
        if(fromWhere == 'CreatePasaje' && ObjectName == 'AG_ParticipanteDelPrograma__c'){
            AG_TravelPlan__c tp = [SELECT Id, AG_Programa__c FROM AG_TravelPlan__c WHERE Id = :param];
            moreConditions = 'AND AG_Programa__c = \'' + (String)tp.AG_Programa__c + '\'';
        }
        
        List <sObject> returnList = new List <sObject>();
      
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey ' + moreConditions + ' order by createdDate DESC limit 5';
        system.debug(sQuery);
        List <sObject> lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
}