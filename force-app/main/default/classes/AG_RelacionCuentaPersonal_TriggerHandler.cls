public class AG_RelacionCuentaPersonal_TriggerHandler {

    public static void fillAccount(List<AG_RelacionCuentaPersonal__c> triggerNew){

        String contId = [select ContactId from User where Id =:UserInfo.getUserId()].ContactId;
        if(isParticipanteComunidad(UserInfo.getProfileId())){

            String accId = [select AccountId from Contact where Id =:contId].AccountId;
            
            for(AG_RelacionCuentaPersonal__c rcp: triggerNew){ rcp.AG_Participante__c = accId;}
        }
    }
    
    private static Boolean isParticipanteComunidad(String idPerfil){
        String participantesProfile = 'AG Participantes Comunidad';
        Profile found = [Select Name from Profile where Id =:idPerfil];
        if (found.Name == participantesProfile){
            return true;
        }else{
            Boolean result = false;
            return result;
        }
        
    }
    
}