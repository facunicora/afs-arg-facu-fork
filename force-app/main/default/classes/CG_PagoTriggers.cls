public class CG_PagoTriggers {
    public static void beforeInsert(List<AG_Pago__c> pagos){
        changeValueExist(pagos);
    }
    
    public static void afterInsert(Map<Id, AG_Pago__c> newMap){
        process();
    }

    public static void changeValueExist(List<AG_Pago__c> listPagosRR){
        List<Date> listPayDate = new List<Date>();
        List<Id> planDePagoListID = new List<Id>();
        
        for(AG_Pago__c payList : listPagosRR){
            listPayDate.add(date.valueOf(payList.AG_FechaDeAcreditacion__c));
            planDePagoListID.add(payList.AG_PlanDePago__c);
        }
        
        Map<id, AG_PlanDePago__c> pDpList = new map<id, AG_PlanDePago__c>([SELECT Divisa__c From AG_PlanDePago__c WHERE id in :planDePagoListID]);
        
        listPayDate.sort(); // Ordena la lista ascendentemente.
        
        List<Valor_de_Cambio__c> valueChangeList = new List<Valor_de_Cambio__c>();
        if(listPayDate.size() > 1){
           valueChangeList = [SELECT Id, Fecha__c, Cambio__c, Divisa_del_cambio__c, Divisa__c FROM Valor_de_Cambio__c WHERE Fecha__c >=: listPayDate[0] AND Fecha__c <=: listPayDate[(listPayDate.size()-1)] ORDER BY Fecha__c ASC]; 
        }else{
           Date myDate2 = Date.today().addDays(-90);
           valueChangeList = [SELECT Id, Fecha__c, Cambio__c, Divisa_del_cambio__c, Divisa__c FROM Valor_de_Cambio__c WHERE Fecha__c <=: listPayDate[0] AND Fecha__c >: myDate2 ORDER BY Fecha__c ASC]; 
        } 
        
        String divisaPdP;
        
        for (AG_Pago__c pago : listPagosRR) {
             divisaPdP = pDpList.get(pago.AG_PlanDePago__c).Divisa__c;
            if(pago.Divisa__c != divisaPdP  && divisaPdP != null){
                Decimal cambio = 0;
                for(Valor_de_Cambio__c vdc : valueChangeList){
                    if(vdc.Fecha__c <= pago.AG_FechaDeAcreditacion__c && vdc.Divisa__c == pago.Divisa__c && vdc.Divisa_del_cambio__c == divisaPdP){
                        cambio = vdc.Cambio__c;
                    }
                }
                system.debug('cambio='+cambio);
                pago.Valor_del_cambio__c = cambio;
                pago.Monto_del_Cambio__c = pago.AG_Importe__c * cambio; 
            }
        }
        
        
    }
    
    public static void process() {
    	
        Map<Id, List<AG_Pago__c>> mapPlanPagoToPagos = new Map<Id, List<AG_Pago__c>>();
        Map<Id, Map<Id,Decimal>> mapPlanPagoToCuotas = new Map<Id, Map<Id,Decimal>>();
        List<AG_Pago__c> pagoListWDfDiv = new List<AG_Pago__c>();
        
        List<AG_Pago__c> listPagos = [SELECT Id, Monto_del_Cambio__c,Valor_del_cambio__c, AG_PlanDePago__c, Divisa__c,AG_Importe__c, AG_PlanDePago__r.Divisa__c, AG_FechaDeAcreditacion__c
                                        FROM AG_Pago__c 
                                        WHERE Id IN: Trigger.new 
                                        ORDER BY AG_PlanDePago__c];
        
        for (AG_Pago__c pago : listPagos) {
            if (!mapPlanPagoToPagos.containsKey(pago.AG_PlanDePago__c)) {
                mapPlanPagoToPagos.put(pago.AG_PlanDePago__c, new List<AG_Pago__c>());
            } 
            
            if(pago.Valor_del_cambio__c != null && pago.Valor_del_cambio__c > 0 && pago.Divisa__c != pago.AG_PlanDePago__r.Divisa__c  && pago.AG_PlanDePago__r.Divisa__c != null){
               
                pago.AG_Importe__c = pago.AG_Importe__c * pago.Valor_del_cambio__c;
                
            }
               mapPlanPagoToPagos.get(pago.AG_PlanDePago__c).add(pago); 
        }
        
        List<AG_Cuota__c> listCuotas = [SELECT Id, AG_PlanDePago__c, AG_Numero__c, AG_Importe__c, AG_ImportePagado__c,
                                        AG_Saldo__c  
                                        FROM AG_Cuota__c 
                                        WHERE AG_PlanDePago__c IN: mapPlanPagoToPagos.keySet()
                                        AND AG_Saldo__c != 0
                                        ORDER BY AG_Numero__c asc];
        
        
        for (AG_Cuota__c cuota : listCuotas) {
            if (!mapPlanPagoToCuotas.containsKey(cuota.AG_PlanDePago__c)) {
                mapPlanPagoToCuotas.put(cuota.AG_PlanDePago__c, new Map<Id,Decimal>());
            } 
            mapPlanPagoToCuotas.get(cuota.AG_PlanDePago__c).put(cuota.Id, cuota.AG_Saldo__c);
        }
        
        List<AG_CuotaxPago__c> listCuotaxPago = new List<AG_CuotaxPago__c>();
        for (Id planPago : mapPlanPagoToPagos.keySet()) {
            if (mapPlanPagoToCuotas.containsKey(planPago)) {
                for (AG_Pago__c pago : mapPlanPagoToPagos.get(planPago)) {
                    
                    Decimal pagoTemp = pago.AG_Importe__c;
                    for (Id cuotaId : mapPlanPagoToCuotas.get(planPago).keySet()) {
                        Decimal saldo = mapPlanPagoToCuotas.get(planPago).get(cuotaId);
                        
                        if (saldo == 0) {
                            continue;
                        } 
                        
                        AG_CuotaxPago__c cuotaxpago = new AG_CuotaxPago__c();
                        cuotaxpago.AG_Cuota__c = cuotaId;
                        cuotaxpago.AG_Pago__c = pago.Id;
                        
                        // Pagamos toda la cuota y gastamos toda la plata del pago
                        if (pagoTemp == saldo) {
                            cuotaxpago.AG_Importe__c = pagoTemp;
                            mapPlanPagoToCuotas.get(planPago).put(cuotaId, 0);
                            listCuotaxPago.add(cuotaxpago);
                            break;
                        } else if (pagoTemp < saldo) {
                            cuotaxpago.AG_Importe__c = pagoTemp;
                            mapPlanPagoToCuotas.get(planPago).put(cuotaId, saldo - pagoTemp);
                            listCuotaxPago.add(cuotaxpago);
                            break;
                        } else {
                            cuotaxpago.AG_Importe__c = saldo;
                            pagoTemp = pagoTemp - saldo;
                            listCuotaxPago.add(cuotaxpago);
                        }
                    }
                }               
            }
        }
        
        if (!listCuotaxPago.isEmpty()) {
            insert listCuotaxPago;
        }
        
    }
}