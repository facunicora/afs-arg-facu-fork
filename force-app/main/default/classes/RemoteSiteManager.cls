global class RemoteSiteManager {
	global static String getToken(Boolean upsertLog){
        Finnegans_Integration__c fInt = Finnegans_Integration__c.getInstance('Access Token');
        
        if(fInt!= null && fInt.Enabled__c){
            String finalEndpoint = fInt.Endpoint__c +
                '?grant_type=client_credentials'+
                '&client_id='+ fInt.Client_Id__c +
                '&client_secret=' + fInt.Client_Secret__c;
            
            HttpResponse response = RemoteSiteConnection.pullObjects(finalEndpoint, null, upsertLog);
            return (response == null)? null : response.getBody();
        }
        
        return null;
    }
}