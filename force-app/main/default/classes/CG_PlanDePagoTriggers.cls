public class CG_PlanDePagoTriggers{
    
    public static void isBefore(List<AG_PlanDePago__c> planDePagoList,Map<Id,AG_PlanDePago__c> oldMap){
        updateFecInicioCuota(planDePagoList,oldMap);
    }
    
    public static void isAfter(List<AG_PlanDePago__c> planDePagoList){
        createQuota();
    }
    
    public static void isAfterInsert(List<AG_PlanDePago__c> planDePagoList){
        planDePagoVerified(planDePagoList);
        createdCuota0M(planDePagoList);
    }
    
    public static void planDePagoVerified(List<AG_PlanDePago__c> planDePagoList){
        List<AG_ParticipanteDelPrograma__c> listPP = new List<AG_ParticipanteDelPrograma__c>();
        for(AG_PlanDePago__c pdp: planDePagoList){
            if(pdp.AG_ParticipanteDelPrograma__c != null){
                if(pdp.AG_ParticipanteDelPrograma__r.CG_TienePlanDePago__c == false){
                    AG_ParticipanteDelPrograma__c test = new AG_ParticipanteDelPrograma__c(Id=pdp.AG_ParticipanteDelPrograma__c,CG_TienePlanDePago__c=true);
                    listPP.add(test);
                }
            }
        }
        
        update listPP;
    }

    public static void createQuota() {
     
        List<AG_PlanDePago__c> triggerNew = [SELECT Id,Divisa__c, Tipo__c, RecordType.DeveloperName, CG_CreateQuota__c, Fecha_incio__c,
                                                 Fecha_fin__c, CG_CantidadDeDias__c, CG_CantidadDeCuotas__c, 
                                                 CG_ImportePendienteDeCuotasaGenerar__c, AG_TotalCuotas__c, CG_TotalPagado__c, CG_CantidadCuotas__c,
                                                (SELECT Id,AG_Numero__c,AG_Importe__c,AG_ImportePagado__c,AG_Saldo__c FROM AG_Cuotas__r
                                                WHERE AG_Numero__c != 0 )
                                                FROM AG_PlanDePago__c 
                                                WHERE Id IN: Trigger.new];
        Map<Id, AG_PlanDePago__c> triggerOldMap = (Map<Id, AG_PlanDePago__c>)Trigger.oldMap;
        
        List<AG_Cuota__c> listQuota = new List<AG_Cuota__c>();
        List<SObject> recordsToUpdate = new List<SObject>();
        
        for (AG_PlanDePago__c pp : triggerNew) {
             if ((trigger.isInsert && pp.CG_CreateQuota__c == true || trigger.isUpdate && triggerOldMap.get(pp.Id).CG_CreateQuota__c == false && pp.CG_CreateQuota__c == true) && pp.RecordType.DeveloperName == 'Dinamico' && pp.Fecha_incio__c != null && pp.Fecha_fin__c != null && pp.CG_ImportePendienteDeCuotasaGenerar__c > 0 && (pp.CG_CantidadDeDias__c != null || pp.CG_CantidadDeCuotas__c != null)) {
                   
                    Decimal QuotaAmountWq;
                    Decimal QuotaAmount;
                    if (pp.CG_CantidadDeDias__c != null) {
                        system.debug('pp.CG_CantidadDeDias__c='+pp.CG_CantidadDeDias__c);
                        Integer numberDaysDue = date.today().daysBetween(pp.Fecha_fin__c);
                        Integer numberOfQuota = integer.valueof((numberDaysDue / pp.CG_CantidadDeDias__c).round(System.RoundingMode.FLOOR));
                        Integer QuotaSpay = 0;
                        if(pp.AG_Cuotas__r.size() > 0 && pp.Tipo__c == 'Programas Mayores 18'){
                            
                            for(integer i=0;i<pp.AG_Cuotas__r.size();i++){
                                system.debug('pp.AG_Cuotas__r[i]='+pp.AG_Cuotas__r[i]);
                                if(pp.AG_Cuotas__r[i].AG_ImportePagado__c == 0){
                                   QuotaSpay++;
                                }
                            }
                            
                            if(QuotaSpay>0) QuotaAmountWq = pp.CG_ImportePendienteDeCuotasaGenerar__c / QuotaSpay;
                            system.debug('QuotaAmountWq='+QuotaAmountWq);
                            
                            for (Integer i = 0; i < pp.AG_Cuotas__r.size(); i++) {
                                if(pp.AG_Cuotas__r[i].AG_ImportePagado__c == 0){
                                    pp.AG_Cuotas__r[i].AG_Importe__c = pp.AG_Cuotas__r[i].AG_Importe__c + QuotaAmountWq;
                                    system.debug('cuota a update ='+pp.AG_Cuotas__r[i]);
                                    recordsToUpdate.add(pp.AG_Cuotas__r[i]); 
                                }
                                numberOfQuota--;
                            }
                        }
                        system.debug('numberOfQuota='+numberOfQuota);
                        if(numberOfQuota>0){ 
                           
                            QuotaAmount = pp.CG_ImportePendienteDeCuotasaGenerar__c / numberOfQuota;
                            for (Integer i = 1; i <= numberOfQuota; i++) {
                                AG_Cuota__c cuota = new AG_Cuota__c();
                                cuota.AG_PlanDePago__c = pp.Id;
                                cuota.AG_Importe__c = QuotaAmount;
                                cuota.AG_Numero__c = i + pp.CG_CantidadCuotas__c - 1;
                                cuota.AG_FechaDeVencimiento__c = date.today().addDays(integer.valueof(pp.CG_CantidadDeDias__c * i));
                                if(pp.Tipo__c == 'Programas Mayores 18'){
                                    cuota.Divisa__c = pp.Divisa__c;
                                }
                                listQuota.add(cuota);
                            }
                        } 
                    
                    } else if (pp.CG_CantidadDeCuotas__c != null) {
                        
                        Integer numberDaysDue = date.today().daysBetween(pp.Fecha_fin__c);
                        Integer CantidadDeDias = integer.valueof((numberDaysDue / pp.CG_CantidadDeCuotas__c).round(System.RoundingMode.FLOOR));
                        Integer QuotaSpay = 0;
                        
                        if(pp.AG_Cuotas__r.size() > 0 && pp.Tipo__c == 'Programas Mayores 18'){
                            for(integer i=0;i<pp.AG_Cuotas__r.size();i++){
                                if(pp.AG_Cuotas__r[i].AG_ImportePagado__c == 0){
                                   QuotaSpay++;
                                }
                            }
                            
                            if(QuotaSpay>0) QuotaAmountWq = pp.CG_ImportePendienteDeCuotasaGenerar__c / QuotaSpay;
                            for (Integer i = 0; i < pp.AG_Cuotas__r.size(); i++) {
                                if(pp.AG_Cuotas__r[i].AG_ImportePagado__c == 0){
                                    pp.AG_Cuotas__r[i].AG_Importe__c = pp.AG_Cuotas__r[i].AG_Importe__c + QuotaAmountWq;
                                    recordsToUpdate.add(pp.AG_Cuotas__r[i]); 
                                }
                                (pp.CG_CantidadDeCuotas__c)--;
                            }
                        }
                        
                        if(pp.CG_CantidadDeCuotas__c>0){ 
                            QuotaAmount = pp.CG_ImportePendienteDeCuotasaGenerar__c / pp.CG_CantidadDeCuotas__c;
                            for (Integer i = 1; i <= pp.CG_CantidadDeCuotas__c; i++) {
                                AG_Cuota__c cuota = new AG_Cuota__c();
                                cuota.AG_PlanDePago__c = pp.Id;
                                cuota.AG_Importe__c = QuotaAmount;
                                cuota.AG_Numero__c = i + pp.CG_CantidadCuotas__c - 1;
                                cuota.AG_FechaDeVencimiento__c = date.today().addDays(integer.valueof(CantidadDeDias * i));
                                if(pp.Tipo__c == 'Programas Mayores 18'){
                                    cuota.Divisa__c = pp.Divisa__c;
                                }
                                listQuota.add(cuota);
                            }
                        }
                        
                    }
                            
            pp.CG_CreateQuota__c = false;
            recordsToUpdate.add(pp);
                
            } 
 
        }
        
        if (!listQuota.isEmpty()) {
            insert listQuota;
        }
        if(!recordsToUpdate.isEmpty()){
            recordsToUpdate.sort();
            update recordsToUpdate;
        }
    
    
    }
    
    //Metodo para actualizar el campo Fecha Inicio Cuota cuando el plan de cuotas es generado
    public static void updateFecInicioCuota(List<AG_PlanDePago__c> newList, Map<Id,AG_PlanDePago__c> oldMap) {
		
        //Recuperar el tipo de registro
        RecordType rt = [Select Id From RecordType where DeveloperName = 'Dinamico'
                        							and SobjectType = 'AG_PlanDePago__c' Limit 1];     
        
        //Si se genera plan de cuotas se actualiza fecha inicio de cuota                                    
        for (AG_PlanDePago__c pp : newList) {            
            if ((trigger.isInsert && pp.CG_CreateQuota__c || trigger.isUpdate && !oldMap.get(pp.Id).CG_CreateQuota__c && pp.CG_CreateQuota__c) && pp.RecordTypeId == rt.Id && pp.Fecha_incio__c != null && pp.Fecha_fin__c != null && pp.CG_ImportePendienteDeCuotasaGenerar__c > 0 && (pp.CG_CantidadDeDias__c != null || pp.CG_CantidadDeCuotas__c != null)) {
              	
                pp.Fecha_inicio_de_cuotas__c = date.today();
            }
        }
    }
    
    /*
     * Metodo que crea la primera cuota para participantes del programa para Mayores 
     * @andrec
     * 17/01/2019
     */
    public static void createdCuota0M(List<AG_PlanDePago__c> planDePagoList){
        List<AG_Cuota__c> listQuota = new List<AG_Cuota__c>();
        
        List<AG_PlanDePago__c> listPlanDePagoNew = new List<AG_PlanDePago__c>();
        Set<Id> PPIds = new Set<Id>(); 
        Set<Id> pDpIds = (new Map<Id,SObject>(planDePagoList)).keySet();
        List<Cuota0mayores2__c> cuota0mayores = Cuota0mayores2__c.getAll().values();
        Map<String,Cuota0mayores2__c> cuota0mayoresMap = new Map<String,Cuota0mayores2__c>();
        
        for(Cuota0mayores2__c q0 : cuota0mayores){
            cuota0mayoresMap.put(q0.Partner__c,q0);
        }
        
        for(AG_PlanDePago__c pDp : planDePagoList){
            PPIds.add(pDp.AG_ParticipanteDelPrograma__c);
        }
        
        
        List<AG_ParticipanteDelPrograma__c> listPP = [SELECT Id, RecordType.Name, (SELECT Estado__c,Forma_de_Pago__c, Partner__c FROM Presupuestos__r WHERE Estado__c = 'Aprobado') FROM AG_ParticipanteDelPrograma__c WHERE Id IN: PPIds AND (NOT RecordType.DeveloperName LIKE '%Menores%')];
        List<AG_PlanDePago__c> pDpListFull = [Select Id,Medio_de_Pago__c,Divisa__c,AG_ParticipanteDelPrograma__c,AG_ParticipanteDelPrograma__r.AG_Participante__r.PersonEmail,Tipo__c,LastModifiedDate,CreatedDate,AG_ParticipanteDelPrograma__r.AG_Participante__r.RecordType.Name,AG_ParticipanteDelPrograma__r.Tipo__c, recordType.Name FROM AG_PlanDePago__c WHERE Id IN:pDpIds];
        Map<Id,String> ppMap = new Map<Id,String>();
        for(AG_ParticipanteDelPrograma__c pp : listPP){
            //Siempre debe haber solo un presupuesto con el estado de Aprobado
            if(pp.Presupuestos__r.size() > 0) ppMap.put(pp.Id,pp.Presupuestos__r[0].Partner__c);
        }
        
        for(AG_PlanDePago__c pDp : pDpListFull){
            String pDpRR = pDp.AG_ParticipanteDelPrograma__r.AG_Participante__r.RecordType.Name;
            String pDpType = pDp.AG_ParticipanteDelPrograma__r.Tipo__c;
            if(pDp.recordType.Name == 'Dinamico' && pDp.CreatedDate == pDp.LastModifiedDate && pDpRR == 'Participante' && pDp.Tipo__c == 'Programas Mayores 18' && pDpType != 'Participante_Beca_Total'){
                AG_Cuota__c newCuote = new AG_Cuota__c();
                newCuote.AG_PlanDePago__c = pDp.Id;
                newCuote.AG_FechaDeVencimiento__c = date.today().addDays(10);
                newCuote.AG_Importe__c = cuota0mayoresMap.get(ppMap.get(pDp.AG_ParticipanteDelPrograma__c)) == null ? cuota0mayoresMap.get('Otro').cuota_0__c : cuota0mayoresMap.get(ppMap.get(pDp.AG_ParticipanteDelPrograma__c)).cuota_0__c;
                newCuote.AG_Numero__c = 0;
                newCuote.Divisa__c = pDp.Divisa__c;
                newCuote.CG_EmailDelParticipante__c = pDp.AG_ParticipanteDelPrograma__r.AG_Participante__r.PersonEmail;
                listQuota.add(newCuote);
                listPlanDePagoNew.add(pDp);
            }
        }
        
        if(listQuota.size() > 0){
          insert listQuota;
        }
        
        /**************************/
    }
}