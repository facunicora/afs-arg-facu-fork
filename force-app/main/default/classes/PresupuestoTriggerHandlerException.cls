public class PresupuestoTriggerHandlerException extends Exception {
    
    public static void isBefore(List<Presupuestador__c> newPresupuestoList){
        cheackContentProviderInProgram(newPresupuestoList);
    }
    
    public static void isAfter(List<Presupuestador__c> newPresupuestoList, Map<Id, Presupuestador__c> newPresupuestoMap){
        PreAproved(newPresupuestoList,newPresupuestoMap);
    }
    
    public static void PreAproved(List<Presupuestador__c> newPresupuestoList,Map<Id, Presupuestador__c> newPresupuestoMap){
        system.debug('PreAproved');
        Set<Id> setPPIds = new Set<Id>();
        List<AG_ParticipanteDelPrograma__c> newPPList = new List<AG_ParticipanteDelPrograma__c>();
        List<Presupuestador__c> newPreList = new List<Presupuestador__c>();
        List<AG_PlanDePago__c> newpDpList = new List<AG_PlanDePago__c>();
        List<SObject> recordsToUpdate = new List<SObject>();
        
        try{
        for(Presupuestador__c pre : newPresupuestoList){
            setPPIds.add(pre.Presupuestos__c);
        }
       
        List<AG_ParticipanteDelPrograma__c> PPList = [SELECT Id,AG_Programa__r.Content_provider__c,AG_EstadoDeLaPostulacion__c,HC_Paginas_a_ocultar__c,Monto_Aprobado__c,Divisa_Aprobada__c,(SELECT Id,vTotal__c,Selector_de_Divisa__c,Forma_de_Pago__c,Estado__c,Seguro_Medico__c,Hospedaje__c FROM Presupuestos__r),(SELECT Id,Monto__c FROM PlanDePagos__r) FROM AG_ParticipanteDelPrograma__c WHERE Id IN: setPPIds];
        
        for(AG_ParticipanteDelPrograma__c pp : PPList){
            Integer flag = 0;
            for(Presupuestador__c p : pp.Presupuestos__r){
                if(newPresupuestoMap.get(p.Id) != null && p.Estado__c == 'Aprobado'){
                   
                    pp.Monto_Aprobado__c = p.vTotal__c;
                    pp.Divisa_Aprobada__c = p.Selector_de_Divisa__c;
                    pp.AG_MedioDePago_del__c = p.Forma_de_Pago__c;
                    
                    if(p.Seguro_Medico__c != 'Seguro Médico' && p.Seguro_Medico__c != 'AFS'){
                       pp.HC_Paginas_a_ocultar__c = multipicklistUpdate(pp.HC_Paginas_a_ocultar__c,'Historial Médico',true);
                    }else{
                       pp.HC_Paginas_a_ocultar__c = multipicklistUpdate(pp.HC_Paginas_a_ocultar__c,'Historial Médico',false);
                    }
                    
                    if(p.Hospedaje__c != null && p.Hospedaje__c.contains('Casa de Familia')){
                       pp.HC_Paginas_a_ocultar__c = multipicklistUpdate(pp.HC_Paginas_a_ocultar__c,'Mi vida cotidiana',false);
                    }else{
                       pp.HC_Paginas_a_ocultar__c = multipicklistUpdate(pp.HC_Paginas_a_ocultar__c,'Mi vida cotidiana',true);
                    }
                    
                    if(pp.AG_EstadoDeLaPostulacion__c == 'Admisión Asignado' && pp.PlanDePagos__r.size() > 0){
                        pp.PlanDePagos__r[0].Monto__c = pp.Monto_Aprobado__c;
                        pp.PlanDePagos__r[0].Medio_de_Pago__c = pp.AG_MedioDePago_del__c;
                        pp.PlanDePagos__r[0].Divisa__c = pp.Divisa_Aprobada__c;
                        recordsToUpdate.add(pp.PlanDePagos__r[0]);
                    }
                   
                    recordsToUpdate.add(pp);
                    flag = 1;
                    break;
                }
            }
            
            for(Presupuestador__c p : pp.Presupuestos__r){
                if(p.Estado__c == 'Aprobado'){
                    system.debug('flag='+flag);
                    system.debug('newPresupuestoMap.get(p.Id)='+newPresupuestoMap.get(p.Id));
                    system.debug('p.Id='+p.Id);
                    if(flag > 0 && newPresupuestoMap.get(p.Id) == null){
                        p.Estado__c = 'Borrador';
                        recordsToUpdate.add(p);
                        break;
                    }  
                    flag++;
                }
            }
            
            if(flag < 1 && pp.Monto_Aprobado__c > 0){
                pp.Monto_Aprobado__c = null;
                pp.Divisa_Aprobada__c = '';						
                recordsToUpdate.add(pp);
            }	
        }
        
        recordsToUpdate.sort();
            system.debug('recordsToUpdate='+recordsToUpdate);
        update recordsToUpdate;
            
        } catch (Exception Ex) {
                System.debug('Exception {');
                System.debug('Message : ' + Ex.getMessage());
                System.debug('Cause : ' + Ex.getCause());
                System.debug('Line Number : ' + Ex.getLineNumber());
                System.debug('Type Name : ' + Ex.getTypeName());
                System.debug('StackTrace : ' + Ex.getStackTraceString());
                System.debug('}');
      }
    }
    
    public static string multipicklistUpdate(String currentValues,String inputValue,boolean addValue){
        Map<String, String> currentValueMap = new Map<String, String>();
        String multiselecFinal;
        if(currentValues != null){
            for(String s :currentValues.split(';')){
                currentValueMap.put(s,s);
            } 
        }
        if(addValue == true){
            if(!currentValueMap.containsKey(inputValue)){
                currentValueMap.put(inputValue,inputValue);
                currentValues = string.join(currentValueMap.values(),';');
            } 
        }else{
            if(currentValueMap.containsKey(inputValue)){
                currentValueMap.remove(inputValue);
                currentValues = string.join(currentValueMap.values(),';');
            }
        }
        return currentValues;
    }
    
    public static void cheackContentProviderInProgram(List<Presupuestador__c> newPresupuestoList){
    	Set<Id> setPPIds = new Set<Id>();
        List<AG_ParticipanteDelPrograma__c> newPPList = new List<AG_ParticipanteDelPrograma__c>();
        List<Presupuestador__c> newPreList = new List<Presupuestador__c>();
        List<AG_PlanDePago__c> newpDpList = new List<AG_PlanDePago__c>();
        List<SObject> recordsToUpdate = new List<SObject>();
        
        try{
            
            for(Presupuestador__c pre : newPresupuestoList){
                setPPIds.add(pre.Presupuestos__c);
            } 
            Map<Id,AG_ParticipanteDelPrograma__c> PPMap = new Map<Id,AG_ParticipanteDelPrograma__c>([SELECT Id,AG_Programa__r.Content_provider__c FROM AG_ParticipanteDelPrograma__c WHERE Id IN: setPPIds]);
                
            for(Presupuestador__c pre : newPresupuestoList){
                if(PPMap.get(pre.Presupuestos__c) != null && PPMap.get(pre.Presupuestos__c).AG_Programa__r.Content_provider__c == 'AFS' && pre.Seguro_Medico__c != 'AFS'){
                  // throw new PresupuestoTriggerHandlerException('error');
                  pre.addError('Este presupuesto debe tener seleccionado el Seguro Médico igual AFS ya que el Participante de Programa tiene un Programa de Content Provider igual a AFS');
                }
                
            }
            
        } catch (Exception Ex) {
                System.debug('Exception {');
                System.debug('Message : ' + Ex.getMessage());
                System.debug('Cause : ' + Ex.getCause());
                System.debug('Line Number : ' + Ex.getLineNumber());
                System.debug('Type Name : ' + Ex.getTypeName());
                System.debug('StackTrace : ' + Ex.getStackTraceString());
                System.debug('}');
               /* ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, Ex.getMessage());
                ApexPages.addMessage(msg);*/
        }
        
        }
        
}