public class guardarycrear {
    //Mi cambio
    public Map<String,Tabla_de_Precios__c> precios;
    public Presupuestador__c presupuesto {get;set;}
    public ID accID {get;set;}
    public String findDivisaPrice {get;set;}
    public String codeDivisa{get;set;}
    public String errorMessage {get;set;}
    public Decimal divisa {get;set;}
    public String fecha_divisa {get;set;}
    ApexPages.StandardController sController;
    
    
    public guardarycrear(ApexPages.StandardController controller) {
        accID = apexPages.currentPage().getParameters().get('accid');
        sController = controller;
        IF(!Test.isRunningTest())controller.addFields(new String[]{
            'Semanas__c',
                'Presupuestador__c'
                });
        
        presupuesto = (Presupuestador__c) controller.getRecord();
        
        if(accId != null){
            presupuesto.Presupuestos__c = accID;
        }
        
    }
    
    public PageReference changePartnerWhenClone(){
        system.debug('changePartnerWhenClone--->'+presupuesto.Partner__c);
        if(presupuesto.Partner__c != 'CELTIC'){
            presupuesto.VTSdl__c = 0;
        }
        return null;
    }
    
    public PageReference Init(){
        errorMessage = ' Seleccionar Semanas';
        if(Presupuesto.Presupuestador__c == 'Version 2'){
            preciosQuery();
            return Divisa();
        }else{
            Divisa();
            return null;
        }
        
    }
    
    public String getLinkDivisa(){
        String linkDivisa;
        IF(codeDivisa != null && codeDivisa != 'USD'){
            findDivisaPrice = codeDivisa+' a USD';
            linkDivisa = 'https://www.google.com/search?q='+codeDivisa+'+to+USD';
        }else{
            findDivisaPrice = null;
        }
        return linkDivisa;
    }
    
    public void preciosQuery(){
        precios = new Map<String,Tabla_de_Precios__c>();
        for(Tabla_de_Precios__c precio : [SELECT Name,Item__c, Precio1__c,Precio2__c,Precio3__c,Precio4__c,Precio5__c,Precio6__c,
                                          Precio7__c,Precio8__c,Maximo1__c,Maximo2__c,Maximo3__c,Maximo4__c,Maximo5__c,Maximo6__c,Maximo7__c,Tipo_de_pago__c,
                                          chosenpartner__c
                                          FROM Tabla_de_Precios__c where chosenpartner__c = :presupuesto.Partner__c] ){
                                              precios.put(precio.Item__c,precio);
                                          }
        PageReference pr = Precios();
    }
    
    public PageReference saveAndNew() {
        sController.save(); 
        if(ApexPages.getMessages().size()==0){
            //PageReference pg = new PageReference('/apex/Presupuestador');
            PageReference pg = new PageReference('/apex/EditPresupuesto?accid='+presupuesto.Presupuestos__c);
            pg.setRedirect(true);
            return pg;
        }else{
            return null;
            
        }
        
    }
    
    public void divisaSelected(){
        List<Valor_de_Cambio__c> valorDelCambio = [SELECT Cambio__c, Divisa__c, Fecha__c  FROM Valor_de_Cambio__c WHERE Fecha__c <= TODAY AND Divisa__c =: presupuesto.Selector_de_Divisa__c
                                                   AND Divisa_del_cambio__c = 'Dólares Americanos'
                                                   ORDER BY Fecha__c DESC LIMIT 1];
        if(valorDelCambio.size() > 0){
            presupuesto.Cotizaci_n_de_Divisa__c = valorDelCambio[0].Cambio__c; 
            fecha_divisa = valorDelCambio[0].Fecha__c.format(); 
        }else{
            presupuesto.Cotizaci_n_de_Divisa__c = null;
            fecha_divisa = null;
        }
    }
    
    public Decimal precioPorSemanaPartner(Date fechaInicio, Decimal numSemanas, String hospedaje){
        Decimal precioFinal = 0;
        date fecha = date.newInstance(fechaInicio.toStartofWeek().year(),fechaInicio.toStartofWeek().month(),fechaInicio.toStartofWeek().day());
        Tabla_de_Precios__c precio = precios.get(hospedaje);
        List<TemporadasSC__c> lisTemporadas = [SELECT Name,Fecha_de_fin_temporada__c,Fecha_de_inicio_temporada__c,Temporada__c,A_o__c FROM TemporadasSC__c where A_o__c =: fechaInicio.year()];
        for(Integer i=1;i<=numSemanas;i++){
            for(TemporadasSC__c rt : lisTemporadas){
                if(rt.Temporada__c == 'A' && ((fecha >= rt.Fecha_de_inicio_temporada__c && fecha < (Date.newInstance(rt.Fecha_de_inicio_temporada__c.year(), 12, 31)).addDays(1)) || (fecha >= Date.newInstance(rt.Fecha_de_inicio_temporada__c.year(), 1, 1) && fecha < (rt.Fecha_de_fin_temporada__c).addDays(1)))){
                    precioFinal = precioFinal + precio.Precio1__c;
                }else if(rt.Temporada__c == 'B' && fecha >= rt.Fecha_de_inicio_temporada__c && fecha < (rt.Fecha_de_fin_temporada__c).addDays(1)){
                    precioFinal = precioFinal + precio.Precio2__c;
                }else if(rt.Temporada__c == 'C' && fecha >= rt.Fecha_de_inicio_temporada__c && fecha < (rt.Fecha_de_fin_temporada__c).addDays(1)){
                    precioFinal = precioFinal + precio.Precio3__c;
                }
            }
            
            fecha = fecha.addDays(7);
        }
        return precioFinal;
    }
    
    public PageReference Divisa(){
        if(presupuesto.Selector_de_Divisa__c == 'Bolívar.'){
            presupuesto.simbolodemoneda__c = 'Bs. ';
            codeDivisa = 'VEF';
        }else if(presupuesto.Selector_de_Divisa__c == 'Boliviano.'){
            presupuesto.simbolodemoneda__c = 'Bs ';
            codeDivisa = 'BOB';
        }else if(presupuesto.Selector_de_Divisa__c == 'Colón costarricense.'){
            presupuesto.simbolodemoneda__c = '¢ ';
            codeDivisa = 'CRC';
        }else if(presupuesto.Selector_de_Divisa__c == 'Dólares Americanos'){
            presupuesto.simbolodemoneda__c = 'USD$ ';
            codeDivisa = 'USD';
        }else if(presupuesto.Selector_de_Divisa__c == 'Euro'){
            presupuesto.simbolodemoneda__c = '€ ';
            codeDivisa = 'EUR';
        }else if(presupuesto.Selector_de_Divisa__c == 'Libra Esterlina'){
            presupuesto.simbolodemoneda__c = '£ ';
            codeDivisa = 'GBP';
        }else if(presupuesto.Selector_de_Divisa__c == 'Peso argentino.'){
            presupuesto.simbolodemoneda__c = 'ARS$ ';
            codeDivisa = 'ARS';
        }else if(presupuesto.Selector_de_Divisa__c == 'Peso chileno.'){
            presupuesto.simbolodemoneda__c = 'CLP$ ';
            codeDivisa = 'CLP';
        }else if(presupuesto.Selector_de_Divisa__c == 'Peso colombiano.'){
            presupuesto.simbolodemoneda__c = 'COP$ ';
            codeDivisa = 'COP';
        }else if(presupuesto.Selector_de_Divisa__c == 'Peso cubano.'){
            presupuesto.simbolodemoneda__c = 'CUC$ ';
            codeDivisa = 'CUC';
        }else if(presupuesto.Selector_de_Divisa__c == 'Peso uruguayo.'){
            presupuesto.simbolodemoneda__c = 'UYU$ ';
            codeDivisa = 'UYU';
        }else if(presupuesto.Selector_de_Divisa__c == 'Real brasileño.'){
            presupuesto.simbolodemoneda__c = 'R$ ';
            codeDivisa = 'BRL';
        }else{
            presupuesto.simbolodemoneda__c = '$ ';
        }
        
        divisaSelected();
        
        IF(presupuesto.vTotal_USD__c == null)presupuesto.vTotal_USD__c = 0;
        IF(presupuesto.vTotal__c == null)presupuesto.vTotal__c = 0;
        IF(presupuesto.Cotizaci_n_de_Divisa__c == null)presupuesto.Cotizaci_n_de_Divisa__c = 0; 
        
        presupuesto.vTotal_USD__c = presupuesto.vTotal__c * presupuesto.Cotizaci_n_de_Divisa__c;
        return null;
        
    }
    
    public PageReference Precios(){
        
        presupuesto.vTotal__c = 0;
        presupuesto.vTotal_USD__c = 0;
        if(presupuesto.Semanas__c != null && presupuesto.Semanas__c > 0){
            errorMessage = '';
        }else if(presupuesto.Semanas__c == null || presupuesto.Semanas__c <= 0){
            errorMessage = ' Seleccionar Semanas';
        }
        
        //Precio Curso Obligatorio
        if(precios.get(presupuesto.Curso_Obligatorio_2__c) != null && presupuesto.Curso_Obligatorio_2__c != null){
            presupuesto.UI1__c = BuscarPrecios(presupuesto.Curso_Obligatorio_2__c,presupuesto.Semanas__c);
            if(precios.get(presupuesto.Curso_Obligatorio_2__c).Tipo_de_pago__c == 'Por Semana'){
                presupuesto.VTI1__c = presupuesto.UI1__c * (presupuesto.Semanas__c == null ? 0 : presupuesto.Semanas__c);
                presupuesto.CI1__c = presupuesto.Semanas__c;
            }else if(precios.get(presupuesto.Curso_Obligatorio_2__c).Tipo_de_pago__c == 'Único'){
                presupuesto.VTI1__c = presupuesto.UI1__c;
                presupuesto.CI1__c = 1;
            }
            presupuesto.vTotal__c += presupuesto.VTI1__c;
        }else{
            presupuesto.UI1__c = 0;
            presupuesto.VTI1__c = 0;
        }
        
        //Precio Curso Opcional
        if(precios.get(presupuesto.Curso_Opcional__c) != null && presupuesto.Curso_Opcional__c != null){
            presupuesto.UI2__c = BuscarPrecios(presupuesto.Curso_Opcional__c,presupuesto.Semanas__c);
            if(precios.get(presupuesto.Curso_Opcional__c).Tipo_de_pago__c == 'Por Semana'){
                presupuesto.VTI2__c = presupuesto.UI2__c * (presupuesto.Semanas__c == null ? 0 : presupuesto.Semanas__c);
                presupuesto.CI2__c = presupuesto.Semanas__c;
                
            }else if(precios.get(presupuesto.Curso_Opcional__c).Tipo_de_pago__c == 'Único'){
                presupuesto.VTI2__c = presupuesto.UI2__c;
                presupuesto.CI2__c = 1;
            }
            presupuesto.vTotal__c += presupuesto.VTI2__c;
        }else{
            presupuesto.VTI2__c = 0;
            presupuesto.UI2__c = 0;
        }
        
        //Precio Hospedaje
        if(precios.get(presupuesto.Hospedaje__c) != null && presupuesto.Hospedaje__c != null){
            presupuesto.UI3__c = BuscarPrecios(presupuesto.Hospedaje__c,presupuesto.Semanas__c);
            if( precios.get(presupuesto.Hospedaje__c).Tipo_de_pago__c == 'Por Semana'){
                presupuesto.VTI3__c = presupuesto.UI3__c * (presupuesto.Semanas__c == null ? 0 : presupuesto.Semanas__c);
                presupuesto.CI3__c = presupuesto.Semanas__c;
            }else if(precios.get(presupuesto.Hospedaje__c).Tipo_de_pago__c == 'Único' && presupuesto.Partner__c.contains('SC') == false){
                presupuesto.VTI3__c = presupuesto.UI3__c;
                presupuesto.CI3__c = 1;
            }else if(presupuesto.Partner__c.contains('SC') == true){ 
                presupuesto.VTI3__c = precioPorSemanaPartner(presupuesto.Fecha_de_Inicio__c,presupuesto.Semanas__c,presupuesto.Hospedaje__c);
                presupuesto.CI3__c = 1;
            }
            presupuesto.vTotal__c += presupuesto.VTI3__c;
            
        }else{
            presupuesto.VTI3__c = 0;
            presupuesto.UI3__c = 0;
        }
        
        //Precio Inscripcion
        if(precios.get('Inscripción') != null && presupuesto.Inscripci_n__c == true){
            presupuesto.UIN__c = BuscarPrecios('Inscripción',presupuesto.Semanas__c);
            if(precios.get('Inscripción').Tipo_de_pago__c == 'Por Semana'){
                presupuesto.VTIN__c = presupuesto.UIN__c * (presupuesto.Semanas__c == null ? 0 : presupuesto.Semanas__c);
                presupuesto.CIN__c = presupuesto.Semanas__c;
            }else if(precios.get('Inscripción').Tipo_de_pago__c == 'Único'){
                presupuesto.VTIN__c = presupuesto.UIN__c;
                presupuesto.CIN__c = 1;
            }
            presupuesto.vTotal__c += presupuesto.VTIN__c;
            
        }else if(precios.get('Inscripcion') == null || presupuesto.Inscripci_n__c == false){
            presupuesto.VTIN__c = 0;
            presupuesto.UIN__c = 0;
            presupuesto.CIN__c = null;
        }
        
        //Precio GCC
        if(precios.get('GCC') != null && presupuesto.GCC__c == true){ // && presupuesto.GCC__c != null
            presupuesto.UG__c = BuscarPrecios('GCC',presupuesto.Semanas__c);
            if(precios.get('GCC').Tipo_de_pago__c == 'Por Semana'){
                presupuesto.VTG__c = presupuesto.UG__c * (presupuesto.Semanas__c == null ? 0 : presupuesto.Semanas__c);
                presupuesto.CG__c = presupuesto.Semanas__c;
            }else if(precios.get('GCC').Tipo_de_pago__c == 'Único'){
                presupuesto.VTG__c = presupuesto.UG__c;
                presupuesto.CG__c = 1;
                
            }
            presupuesto.vTotal__c += presupuesto.VTG__c;
            
        }else if(precios.get('GCC') == null || presupuesto.GCC__c == false){
            presupuesto.VTG__c = 0;
            presupuesto.UG__c = 0;
            presupuesto.CG__c = null;
            
        }
        
        //Precio Materiales
        if(precios.get('Materiales') != null && presupuesto.Materiales__c == true){ // && presupuesto.Materiales__c != null
            presupuesto.UM__c = BuscarPrecios('Materiales',presupuesto.Semanas__c);
            if(precios.get('Materiales').Tipo_de_pago__c == 'Por Semana'){
                presupuesto.VTM__c = presupuesto.UM__c * (presupuesto.Semanas__c == null ? 0 : presupuesto.Semanas__c);
                presupuesto.CM__c = presupuesto.Semanas__c;
            }else if(precios.get('Materiales').Tipo_de_pago__c == 'Único'){
                presupuesto.VTM__c = presupuesto.UM__c;
                presupuesto.CM__c = 1;
                
            }
            presupuesto.vTotal__c += presupuesto.VTM__c;
            
        }else if(precios.get('Materiales') == null || presupuesto.Materiales__c == false){
            presupuesto.VTM__c = 0;
            presupuesto.UM__c =0;
            presupuesto.CM__c = null;
        }
        
        //Precio Servicio de Limpieza
        if(precios.get('Servicio de Limpieza') != null && presupuesto.Servicio_de_Limpieza__c == true){
            presupuesto.USdl__c = BuscarPrecios('Servicio de Limpieza',presupuesto.Semanas__c);
            presupuesto.VTSdl__c = presupuesto.USdl__c;
            presupuesto.vTotal__c += presupuesto.VTSdl__c;
            
        }else if(precios.get('Servicio de Limpieza') == null || presupuesto.Servicio_de_Limpieza__c == false){
            presupuesto.VTSdl__c = 0;
            presupuesto.USdl__c =0;
        }
        
        //Precio Traslado desde Aeropuerto
        if(precios.get(presupuesto.Traslado_desde_Aeropuerto__c) != null && presupuesto.Traslado_desde_Aeropuerto__c != null){
            presupuesto.UA1__c = BuscarPrecios(presupuesto.Traslado_desde_Aeropuerto__c,presupuesto.Semanas__c);
            if(precios.get(presupuesto.Traslado_desde_Aeropuerto__c).Tipo_de_pago__c == 'Por Semana'){
                presupuesto.VTA1__c = presupuesto.UA1__c * (presupuesto.Semanas__c == null ? 0 : presupuesto.Semanas__c);
                presupuesto.CA1__c = presupuesto.Semanas__c;
                
            }else if(precios.get(presupuesto.Traslado_desde_Aeropuerto__c).Tipo_de_pago__c == 'Único'){
                presupuesto.VTA1__c = presupuesto.UA1__c;
                presupuesto.CA1__c = 1;
            }
            presupuesto.vTotal__c += presupuesto.VTA1__c;
        }else{
            presupuesto.UA1__c = 0;
            presupuesto.VTA1__c = 0;
        }
        
        //Precio Seguro Medico
        if(presupuesto.Seguro_Medico__c != null && precios.get(presupuesto.Seguro_Medico__c) != null){
            presupuesto.UA2__c = BuscarPrecios(presupuesto.Seguro_Medico__c,presupuesto.Semanas__c);
            if(precios.get(presupuesto.Seguro_Medico__c).Tipo_de_pago__c == 'Por Semana'){
                presupuesto.VTA2__c = presupuesto.UA2__c * (presupuesto.Semanas__c == null ? 0 : presupuesto.Semanas__c);
                presupuesto.CA2__c = presupuesto.Semanas__c;
                
            }else if(precios.get(presupuesto.Seguro_Medico__c).Tipo_de_pago__c == 'Único'){
                presupuesto.VTA2__c = presupuesto.UA2__c;
                presupuesto.CA2__c = 1;
                
            }
            presupuesto.vTotal__c += presupuesto.VTA2__c;
            
        }else{
            presupuesto.UA2__c = 0;
            presupuesto.VTA2__c = 0;
        }
        
        //Visa
        if(presupuesto.Visa__c != null && precios.get(presupuesto.Visa__c) != null){
            presupuesto.UVs__c = BuscarPrecios(presupuesto.Visa__c,presupuesto.Semanas__c);
            if(precios.get(presupuesto.Visa__c).Tipo_de_pago__c == 'Por Semana'){
                presupuesto.VTVs__c = presupuesto.UVs__c * (presupuesto.Semanas__c == null ? 0 : presupuesto.Semanas__c);
                presupuesto.CVs__c = presupuesto.Semanas__c;
                
            }else if(precios.get(presupuesto.Visa__c).Tipo_de_pago__c == 'Único'){
                presupuesto.VTVs__c = presupuesto.UVs__c;
                presupuesto.CVs__c = 1;
                
            }
            presupuesto.vTotal__c += presupuesto.VTVs__c;
            
        }else{
            presupuesto.UVs__c = 0;
            presupuesto.VTVs__c = 0;
        }
        
        //Semanas de Verano
        if(presupuesto.Semanas_de_Verano__c != null && precios.get('Semanas de Verano') != null){
            Tabla_de_Precios__c precioVerano = precios.get('Semanas de Verano');
            presupuesto.USdv__c = precioVerano.Precio1__c;
            presupuesto.VTsv__c = precioVerano.Precio1__c * presupuesto.Semanas_de_Verano__c;
            presupuesto.vTotal__c += presupuesto.VTsv__c ; 
            
        }else{
            presupuesto.VTsv__c = 0;  
        }
        
        if(presupuesto.vTotal__c == null)presupuesto.vTotal__c = 0;
        if(presupuesto.VTA2__c == null)presupuesto.VTA2__c = 0;
        if(presupuesto.vTotal_USD__c == null)presupuesto.vTotal_USD__c = 0;
        if(presupuesto.Cotizaci_n_de_Divisa__c == null)presupuesto.Cotizaci_n_de_Divisa__c = 0;
        if(presupuesto.Forma_de_Pago__c == 'Tarjeta de Crédito'){
            presupuesto.URtdc__c = (0.05 * presupuesto.vTotal__c).setScale(2);
            presupuesto.VTRtdc__c = presupuesto.URtdc__c;
            presupuesto.vTotal__c = presupuesto.vTotal__c + presupuesto.VTRtdc__c;
        }else{
            presupuesto.URtdc__c = 0;
            presupuesto.VTRtdc__c = 0;
        } 
        presupuesto.vTotal_USD__c = presupuesto.vTotal__c * presupuesto.Cotizaci_n_de_Divisa__c ;
        
        return null;
    }
    
    public Decimal BuscarPrecios(String curso, Decimal Semanas){
        Tabla_de_Precios__c precio = precios.get(curso);
        Decimal precioUnit = 0;
        if(precio != null){
            //precio 1
            if(precio.Maximo1__c == null){
                precioUnit = precio.Precio1__c;
            }else if(Semanas< precio.Maximo1__c ){
                precioUnit = precio.Precio1__c;
            }
            //precio 2
            else if(precio.Maximo2__c == null ){
                precioUnit = precio.Precio2__c;
            }else if(Semanas< precio.Maximo2__c ) {
                precioUnit = precio.Precio2__c;
            }
            //precio 3
            else if(precio.Maximo3__c == null ){
                precioUnit = precio.Precio3__c;
            }else if(Semanas< precio.Maximo3__c ) {
                precioUnit = precio.Precio3__c;
            }
            //precio 4
            else if(precio.Maximo4__c == null ){
                precioUnit = precio.Precio4__c;
            }else if(Semanas< precio.Maximo4__c ) {
                precioUnit = precio.Precio4__c;
            }
            //precio 5
            else if(precio.Maximo5__c == null ){
                precioUnit = precio.Precio5__c;
            }else if(Semanas< precio.Maximo5__c ) {
                precioUnit = precio.Precio5__c;
            }
            //precio 6
            else if(precio.Maximo6__c == null ){
                precioUnit = precio.Precio6__c;
            }else if(Semanas< precio.Maximo6__c ) {
                precioUnit = precio.Precio6__c;
            }
            //precio 7
            else if(precio.Maximo7__c == null ){
                precioUnit = precio.Precio7__c;
            }else if(Semanas< precio.Maximo7__c ) {
                precioUnit = precio.Precio7__c;
                //precio 8
            }else if(Semanas> precio.Maximo7__c ) {
                precioUnit = precio.Precio8__c;
            }else {
                precioUnit = 0;
            }
        }else{
            precioUnit = 0;
        }
        return precioUnit;
        
    }
    
    public PageReference Preciosxcant(){
        //Precio Curso Obligatorio
        if(presupuesto.VTI1__c != null && presupuesto.UI1__c != null && presupuesto.CI1__c != null && presupuesto.Curso_Obligatorio_2__c != null){
            presupuesto.VTI1__c = presupuesto.UI1__c * presupuesto.CI1__c ;
        }else{ 
            presupuesto.VTI1__c = 0;
        }
        //Precio Curso Opcional
        if(presupuesto.VTI2__c != null && presupuesto.UI2__c != null && presupuesto.CI2__c != null && presupuesto.Curso_Opcional__c != null){
            presupuesto.VTI2__c = presupuesto.UI2__c * presupuesto.CI2__c;
        }else{
            presupuesto.VTI2__c = 0;
        }
        //Precio Hospedaje
        if(presupuesto.VTI3__c != null && presupuesto.UI3__c != null && presupuesto.CI3__c != null && presupuesto.Hospedaje__c != null){
            presupuesto.VTI3__c = presupuesto.UI3__c * presupuesto.CI3__c ;
        }else{
            presupuesto.VTI3__c = 0;
        }
        //Precio Inscripcion
        if(presupuesto.VTIN__c != null && presupuesto.UIN__c != null && presupuesto.CIN__c != null && presupuesto.Inscripci_n__c != false){
            presupuesto.VTIN__c = presupuesto.UIN__c * presupuesto.CIN__c ;
        }else{
            presupuesto.VTIN__c = 0;
        }
        //Precio GCC
        if(presupuesto.VTG__c != null && presupuesto.UG__c != null && presupuesto.CG__c != null && presupuesto.GCC__c != false){
            presupuesto.VTG__c = presupuesto.UG__c * presupuesto.CG__c;
        }else{
            presupuesto.VTG__c = 0;
        }
        //Precio Materiales
        if(presupuesto.VTM__c != null && presupuesto.UM__c != null && presupuesto.CM__c != null && presupuesto.Materiales__c != false){
            presupuesto.VTM__c = presupuesto.UM__c * presupuesto.CM__c;
        }else{
            presupuesto.VTM__c = 0;
        }
        
        //Precio Traslado desde Aeropuerto
        if(presupuesto.VTA1__c != null && presupuesto.UA1__c != null && presupuesto.CA1__c != null && presupuesto.Traslado_desde_Aeropuerto__c != null && presupuesto.Traslado_desde_Aeropuerto__c != 'NO'){
            presupuesto.VTA1__c = presupuesto.UA1__c * presupuesto.CA1__c;
        }else{
            presupuesto.VTA1__c = 0;
        }
        //Precio Seguro Medico
        if(presupuesto.VTA2__c != null && presupuesto.UA2__c != null && presupuesto.CA2__c != null && presupuesto.Seguro_Medico__c != null && presupuesto.Seguro_Medico__c != 'NO'){
            presupuesto.VTA2__c = presupuesto.UA2__c * presupuesto.CA2__c;
        }else{
            presupuesto.VTA2__c = 0;
        }
        //Precio Visa
        if(presupuesto.VTVs__c != null && presupuesto.UVs__c != null && presupuesto.CVs__c != null && presupuesto.Visa__c != null && presupuesto.Visa__c != 'NO'){
            presupuesto.VTVs__c = presupuesto.UVs__c * presupuesto.CVs__c;
        }else{
            presupuesto.VTVs__c = 0;
        }
        
        if(presupuesto.vTotal__c == null)presupuesto.vTotal__c = 0;
        if(presupuesto.vTotal_USD__c == null)presupuesto.vTotal_USD__c = 0;
        if(presupuesto.Cotizaci_n_de_Divisa__c == null)presupuesto.Cotizaci_n_de_Divisa__c = 0;
        
        presupuesto.vTotal__c = presupuesto.VTI1__c + presupuesto.VTI2__c + presupuesto.VTI3__c + presupuesto.VTIN__c + presupuesto.VTG__c + presupuesto.VTM__c + presupuesto.VTA1__c + presupuesto.VTA2__c + presupuesto.VTVs__c;
        if(presupuesto.Forma_de_Pago__c == 'Tarjeta de Crédito'){
            presupuesto.URtdc__c = (0.05 * presupuesto.vTotal__c).setScale(2);
            presupuesto.VTRtdc__c = presupuesto.URtdc__c;
            presupuesto.vTotal__c = presupuesto.vTotal__c + presupuesto.VTRtdc__c;
            
        }else{
            presupuesto.URtdc__c = 0;
            presupuesto.VTRtdc__c = 0;
        }
        presupuesto.vTotal_USD__c = presupuesto.vTotal__c * presupuesto.Cotizaci_n_de_Divisa__c ;
        return null;
    }
     //Nueva clase para presupuesto libre
    public PageReference Preciosxcantv3(){
        
        if(presupuesto.VUtext1__c != null && presupuesto.CantText1__c != null && presupuesto.Texto1__c != null){
            presupuesto.VTtext1__c = presupuesto.VUtext1__c * presupuesto.CantText1__c ;
        }else{ 
            presupuesto.VTtext1__c = 0;
        }
        
        if(presupuesto.VUtext2__c != null && presupuesto.CantText2__c != null && presupuesto.Texto2__c != null){
            presupuesto.VTtext2__c = presupuesto.VUtext2__c * presupuesto.CantText2__c;
        }else{
            presupuesto.VTtext2__c = 0;
        }
        
        if(presupuesto.VUtext3__c != null && presupuesto.CantText3__c != null && presupuesto.Texto3__c != null){
            presupuesto.VTtext3__c = presupuesto.VUtext3__c * presupuesto.CantText3__c;
        }else{
            presupuesto.VTtext3__c = 0;
        }
        
         if(presupuesto.VUtext4__c != null && presupuesto.CantText4__c != null && presupuesto.Texto4__c != null){
            presupuesto.VTtext4__c = presupuesto.VUtext4__c * presupuesto.CantText4__c;
        }else{
            presupuesto.VTtext4__c = 0;
        }
     
         if(presupuesto.VUtext5__c != null && presupuesto.CantText5__c != null && presupuesto.Texto5__c != null){
            presupuesto.VTtext5__c = presupuesto.VUtext5__c * presupuesto.CantText5__c;
        }else{
            presupuesto.VTtext5__c = 0;
        }
        
        if(presupuesto.VUtext6__c != null && presupuesto.CantText6__c != null && presupuesto.Texto6__c != null){
            presupuesto.VTtext6__c = presupuesto.VUtext6__c * presupuesto.CantText6__c;
        }else{
            presupuesto.VTtext6__c = 0;
        }
        
       
        
        if(presupuesto.vTotal__c == null)presupuesto.vTotal__c = 0;
        if(presupuesto.vTotal_USD__c == null)presupuesto.vTotal_USD__c = 0;
        if(presupuesto.Cotizaci_n_de_Divisa__c == null)presupuesto.Cotizaci_n_de_Divisa__c = 0;
        
        presupuesto.vTotal__c = presupuesto.VTtext1__c + presupuesto.VTtext2__c + presupuesto.VTtext3__c + presupuesto.VTtext4__c + presupuesto.VTtext5__c + presupuesto.VTtext6__c;
        if(presupuesto.Forma_de_Pago__c == 'Tarjeta de Crédito'){
            presupuesto.URtdc__c = (0.05 * presupuesto.vTotal__c).setScale(2);
            presupuesto.VTRtdc__c = presupuesto.URtdc__c;
            presupuesto.vTotal__c = presupuesto.vTotal__c + presupuesto.VTRtdc__c;
            
        }else{
            presupuesto.URtdc__c = 0;
            presupuesto.VTRtdc__c = 0;
        }
        presupuesto.vTotal_USD__c = presupuesto.vTotal__c * presupuesto.Cotizaci_n_de_Divisa__c ;
        return null;
    }
    
}