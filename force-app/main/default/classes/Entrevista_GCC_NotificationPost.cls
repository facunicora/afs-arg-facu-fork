public class Entrevista_GCC_NotificationPost {
	public static void SendEmailAndPostChatter(Map<Id, Entrevista_GCC__c> newEntrevistaMap) {
        EmailTemplate template = [SELECT Id FROM EmailTemplate WHERE name LIKE 'Plantilla Entrevista GCC' LIMIT 1];
        List<Messaging.SingleEmailMessage> masterListMails = new List<Messaging.SingleEmailMessage>();   
        List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
        
        for(Entrevista_GCC__c ent : [SELECT Id, Participante__r.AG_Participante__r.PersonContactId FROM Entrevista_GCC__c 
                                     WHERE Id in :newEntrevistaMap.keySet() AND Participante__r.AG_Participante__r.PersonContactId != null]){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTargetObjectId(ent.Participante__r.AG_Participante__r.PersonContactId);
                mail.setTemplateId(template.id);
                mail.saveAsActivity = false;
                masterListMails.add(mail);
                system.debug(mail);
                                         
                ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
                input.subjectId = ent.id;
                ConnectApi.MessageBodyInput body = new ConnectApi.MessageBodyInput();
                body.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        
                ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();
                textSegment.text = 'Tiene una entrevista con ';
                body.messageSegments.add(textSegment);
                
                ConnectApi.EntityLinkSegmentInput mention = new ConnectApi.EntityLinkSegmentInput();
                mention.entityId = ent.Participante__r.AG_Participante__r.PersonContactId;
                body.messageSegments.add(mention);
        
                input.body = body;
                batchInputs.add(new ConnectApi.BatchInput(input));
            }
        if(!Test.isRunningTest()){
            Messaging.sendEmail(masterListMails);
            ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputs);
        }
        	
	} 
}