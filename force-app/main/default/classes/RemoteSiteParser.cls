global class RemoteSiteParser {
    public static String AccountToCliente(Account acc, AG_PlanDePago__c plan){
        RemoteSiteModel.Cliente cliente = new RemoteSiteModel.Cliente();
        cliente.Nombre = acc.Name;
        cliente.codigo = plan.ID_Externo_PPago__c;
        cliente.Activo = true;
        cliente.RazonSocial = acc.Name;
        cliente.Email = acc.PersonEmail;
        cliente.SituacionIIBB = Integer.valueOf(acc.PersonContact.Situacion_IIBB__c);
        cliente.CategoriaFiscalCodigo = acc.PersonContact.Categoria_Fiscal_Codigo__c;
        cliente.ConstanciaInscripcionAfip = acc.PersonContact.Constancia_Inscripcion_Afip__c;
        cliente.CuentaClienteCodigo = plan.AG_ParticipanteDelPrograma__r.AG_Programa__r.RecordType.DeveloperName == 'Menores'? 'ANTENV' : 'ANTENV1';
        cliente.ConceptoClienteCodigo = 'CLI';
        
        String documento = String.valueOf(acc.AG_DNI__c);
        if(documento.length() > 7){
            if(acc.PersonContact.Identificacion_Tributaria_Codigo__c == 'DNI' || documento.length() < 10){
                cliente.IdentificacionTributariaCodigo = 'DNI';
                String doc = documento.substring(0,2)+'.'+documento.substring(2,5)+'.'+documento.substring(5,8);
                cliente.Numero_de_identificacion_tributaria_99999XVC = acc.PersonMailingCountry == 'Uruguay'? doc+'-'+documento.substring(8,9) : doc;
            }else if(acc.PersonContact.Identificacion_Tributaria_Codigo__c == 'CUIL'){
                cliente.IdentificacionTributariaCodigo = 'CUIL';
                cliente.Numero_de_identificacion_tributaria_99999XVC = documento.substring(0,2)+'-'+documento.substring(2,10)+'-'+documento.substring(10,11);
            }
        }
        
        
        RemoteSiteModel.CondicionPago CondicionPago = new RemoteSiteModel.CondicionPago();
        CondicionPago.CondicionPagoCodigo = acc.PersonContact.Condicion_Pago_Codigo__c;
        cliente.CondicionesPago = new List<RemoteSiteModel.CondicionPago>{CondicionPago};
        
       //cliente.Direcciones = new List<RemoteSiteModel.Direccion>{new RemoteSiteModel.Direccion(con.MailingCountry, con.MailingState, con.MailingCity, con.MailingPostalCode, con.MailingStreet)};
        
        /*RemoteSiteModel.ContactoItem ContactoItem = new RemoteSiteModel.ContactoItem();
        ContactoItem.PersonaID = con.Id;
        cliente.ContactoItems = new List<RemoteSiteModel.ContactoItem>{ContactoItem};*/
        

	    return JSON.serialize(cliente).replace('Numero_de_identificacion_tributaria_99999XVC', 'Número de identificación tributaria').replace('Default_nombre_99999XVC', 'Default');
    }
    
    public static String serializeCobranza(AG_PlanDePago__c plan, List<AG_Pago__c> pagos, AG_Programa__c programa, string empresaCodigo){
        RemoteSiteModel.Cobranza cobranza = new RemoteSiteModel.Cobranza();
        
        cobranza.IdentificacionExterna = plan.AG_ParticipanteDelPrograma__r.AG_Participante__r.PersonContactId;
        cobranza.Proveedor = plan.ID_Externo_PPago__c;
        
        //cobranza.IdentificacionExterna = plan.ID_Externo_PPago__c;
        cobranza.EmpresaCodigo = empresaCodigo; //'Prueba32'; // 'EMPRE01';
        //cobranza.Proveedor = plan.AG_ParticipanteDelPrograma__r.AG_Participante__r.PersonContactId; //'0030v00000PEgfrAAD';	//PersonContactId
        cobranza.TransaccionTipoCodigo = 'OPERTESORERIA';
        cobranza.TransaccionSubtipoCodigo = 'COBRANZA';
        cobranza.DiferenciaCambio = 0;
        cobranza.UsaCotizacionOrigen = 0;
        
        cobranza.Efectivo = new List<RemoteSiteModel.Efectivo>();
        cobranza.Banco = new List<RemoteSiteModel.Banco>();
        cobranza.Tarjeta = new List<RemoteSiteModel.Tarjeta>();
        cobranza.Otros = new List<RemoteSiteModel.Otro>();
        cobranza.Cotizaciones = new List<RemoteSiteModel.Cotizacion>();
        cobranza.CtaCte = new List<RemoteSiteModel.CtaCte>();
        
        Decimal totalMonto = 0;
        for(AG_Pago__c pago :pagos){
            if(pago.AG_FechaDeAcreditacion__c != null){
                String fechaFormateada = String.valueOf(pago.AG_FechaDeAcreditacion__c);
                cobranza.Fecha = fechaFormateada.substring(0,4) +'-'+ fechaFormateada.substring(5,7) +'-'+ fechaFormateada.substring(8,10);
            }
            
            switch on pago.AG_MedioDePago_del__c{
                when 'Efectivo'{
                    RemoteSiteModel.Efectivo efectivo = new RemoteSiteModel.Efectivo();
                    
                    efectivo.DebeHaber = 1;
                    efectivo.ImporteMonTransaccion = pago.AG_Importe__c;
                    efectivo.MonedaCodigo = parseDivisa(pago.Divisa__c);
                    efectivo.CuentaCodigo = efectivo.MonedaCodigo == 'DOL'? 'CAJCHIUSD' :efectivo.MonedaCodigo == 'PES'? 'CAJCHI' : 'CCE';
                    
                    cobranza.Efectivo.add(efectivo);
                }
                when 'Transferencia'{
                    RemoteSiteModel.Banco banco = new RemoteSiteModel.Banco();
                    
                    banco.OperacionBancariaCodigo = 'TRANSFERENCIATER';
                    banco.DebeHaber = 1;
                    banco.ImporteMonTransaccion = pago.AG_Importe__c;
                    banco.MonedaCodigo = parseDivisa(pago.Divisa__c);
                    banco.CuentaCodigo = banco.MonedaCodigo == 'DOL'? 'BCOBBVAUSD' : 'BCOBBVA1';
                    
                    cobranza.Banco.add(banco);
                }
                when 'Deposito'{
                    RemoteSiteModel.Banco banco = new RemoteSiteModel.Banco();
                    
                    banco.OperacionBancariaCodigo = 'DEPOSEFECT';
                    banco.DebeHaber = 1;
                    banco.ImporteMonTransaccion = pago.AG_Importe__c;
                    banco.MonedaCodigo = parseDivisa(pago.Divisa__c);
                    banco.CuentaCodigo = banco.MonedaCodigo == 'DOL'? 'BCOBBVAUSD' : 'BCOBBVA1';
                    
                    cobranza.Banco.add(banco);
                }
                when 'Cheque'{
                    RemoteSiteModel.Banco banco = new RemoteSiteModel.Banco();
                    
                    banco.OperacionBancariaCodigo = 'RECEPCHTER';
                    banco.DebeHaber = 1;
                    banco.ImporteMonTransaccion = pago.AG_Importe__c;
                    banco.MonedaCodigo = parseDivisa(pago.Divisa__c);
                    banco.CuentaCodigo = 'MERCPAG';
                    
                    cobranza.Banco.add(banco);
                }
                when 'Via PayU'{
                    RemoteSiteModel.Banco banco = new RemoteSiteModel.Banco();
                    
                    banco.OperacionBancariaCodigo = 'COBRAPAYU';
                    banco.DebeHaber = 1;
                    banco.ImporteMonTransaccion = pago.AG_Importe__c;
                    banco.MonedaCodigo = parseDivisa(pago.Divisa__c);
                    banco.CuentaCodigo = 'PAY U';
                    
                    cobranza.Banco.add(banco);
                }
                when 'Tarjeta de crédito'{
                    RemoteSiteModel.Tarjeta tarjeta = new RemoteSiteModel.Tarjeta();
                    
                    tarjeta.OperacionBancariaCodigo = 'PATAR';
                    tarjeta.DebeHaber = 1;
                    tarjeta.ImporteMonTransaccion = pago.AG_Importe__c;
                    tarjeta.MonedaCodigo = parseDivisa(pago.Divisa__c);
                    tarjeta.CuentaCodigo = 'PAY U';
                    
                    cobranza.Tarjeta.add(tarjeta);
                }
                when 'Tarjeta de débito'{
                    RemoteSiteModel.Tarjeta tarjeta = new RemoteSiteModel.Tarjeta();
                    
                    tarjeta.OperacionBancariaCodigo = 'PADEB';
                    tarjeta.DebeHaber = 1;
                    tarjeta.ImporteMonTransaccion = pago.AG_Importe__c;
                    tarjeta.MonedaCodigo = parseDivisa(pago.Divisa__c);
                    tarjeta.CuentaCodigo = 'PAY U';
                    
                    cobranza.Tarjeta.add(tarjeta);
                }
                when else{
                    RemoteSiteModel.Otro otro = new RemoteSiteModel.Otro();
                    
                    otro.DebeHaber = 1;
                    otro.ImporteMonTransaccion = pago.AG_Importe__c;
                    otro.MonedaCodigo = parseDivisa(pago.Divisa__c);
                    
                    cobranza.Otros.add(otro);
                }
            }
            totalMonto += pago.AG_Importe__c;
        }
        
        RemoteSiteModel.CtaCte ctaCte = new RemoteSiteModel.CtaCte();
        //ctaCte.CuentaCodigo = 'ANTACTESP';
        ctaCte.CuentaCodigo = programa.RecordType.DeveloperName == 'Menores'? 'ANTENV' : 'ANTENV1';
        ctaCte.DebeHaber = -1;
        ctaCte.ImporteMonTransaccion = totalMonto;
        ctaCte.ImporteMonPrincipal = totalMonto;
        ctaCte.MonedaCodigo = parseDivisa(pagos[0].Divisa__c);
        cobranza.CtaCte.add(ctaCte);
        
        //COTIZACIONES
        Map<String, Decimal> mapCotizaciones = new Map<String, Decimal>();
        for(Valor_de_Cambio__c vc:[SELECT Cambio__c, divisa__c FROM Valor_de_Cambio__c 
                                   WHERE divisa__c != null AND divisa_del_cambio__c != null AND divisa_del_cambio__c = :pagos[0].Divisa__c 
                                   ORDER BY createdDate DESC]){
                                       
            if(!mapCotizaciones.containsKey(vc.divisa__c)){
                mapCotizaciones.put(vc.divisa__c, vc.Cambio__c);
            }
        }
        for(String divisa :mapCotizaciones.keySet()){
            RemoteSiteModel.Cotizacion cot = new RemoteSiteModel.Cotizacion();
            cot.MonedaCodigo = parseDivisa(divisa);
            cot.Cotizacion = mapCotizaciones.get(divisa);
            if(cot.MonedaCodigo != null){
                cobranza.Cotizaciones.add(cot);
            }
        }
        
        RemoteSiteModel.Cotizacion cot = new RemoteSiteModel.Cotizacion();
        cot.MonedaCodigo = parseDivisa(pagos[0].Divisa__c);
        cot.Cotizacion = 1;
        /*cot.Cotizacion = pagos[0].Divisa__c == 'Dólares Americanos' ? [SELECT Cambio__c FROM Valor_de_Cambio__c 
                                                                       WHERE divisa__c = 'Peso argentino.' AND divisa_del_cambio__c = 'Dólares Americanos'
                                                                       ORDER BY createdDate DESC LIMIT 1].Cambio__c :
        															  [SELECT Cambio__c FROM Valor_de_Cambio__c 
                                                                       WHERE divisa__c = 'Dólares Americanos' AND divisa_del_cambio__c = :pagos[0].Divisa__c
                                                                       ORDER BY createdDate DESC LIMIT 1].Cambio__c;*/
        if(cot.MonedaCodigo != null){
            cobranza.Cotizaciones.add(cot);
        }
        
        return JSON.serialize(cobranza);
    }
    
    public static String parseDivisa(String divisa){
        switch on divisa{
            when 'Peso argentino.'{
                return 'PES';
            }
            when 'Dólares Americanos'{
                return 'DOL';
            }
            when 'Real brasileño.'{
                return 'Real';
            }
            when 'Bolívar.'{
                return 'Bolívar Venezolano ';
            }
            when 'Boliviano.'{
                return 'Peso Boliviano ';
            }
            when 'Libra Esterlina'{
                return 'Libra Esterlina ';
            }
            when 'Peso chileno.'{
                return 'Peso Chileno ';
            }
            when 'Peso colombiano.'{
                return 'Peso Colombiano ';
            }
            when 'Peso uruguayo.'{
                return 'Pesos Uruguayos ';
            }
            when 'Euro'{
                return 'EURO';
            }
            when else{
                return null;
            }
        }
    }
    
    /*public static String serializePtoVenta(Id conId, AG_PlanDePago__c plan, List<AG_Pago__c> pagos, AG_Programa__c programa){
        RemoteSiteModel.PuntoDeVenta ptoVenta = new RemoteSiteModel.PuntoDeVenta();
        
        ptoVenta.IdentificacionExterna = plan.ID_Externo_PPago__c;
        ptoVenta.ClienteCodigo = conId;
        ptoVenta.Descripcion = programa.IdDePrograma__c;
        
        ptoVenta.PuntoVentaItemsBanco = new List<RemoteSiteModel.PuntoVentaItemBanco>();
        ptoVenta.PuntoVentaItemsTarjeta = new List<RemoteSiteModel.PuntoVentaItemTarjeta>();
        ptoVenta.PuntoVentaItemsEfectivo = new List<RemoteSiteModel.PuntoVentaItemEfectivo>();
        ptoVenta.PuntoVentaItemsOtros = new List<RemoteSiteModel.PuntoVentaItemOtros>();
        
        for(AG_Pago__c pago :pagos){
            ptoVenta.Fecha = String.valueOf(pago.AG_FechaDeAcreditacion__c.format()).replace('/','-');
            ptoVenta.MonedaCodigo = parseDivisa(pago.Divisa__c);
            
            switch on pago.AG_MedioDePago_del__c{
                when 'Efectivo'{
                    ptoVenta.TransaccionTipoCodigo = parseDivisa(pago.Divisa__c) == 'PES'? 'CAJCHI' : 'CAJCHIUSD';
                    ptoVenta.PuntoVentaItemsEfectivo.add(new RemoteSiteModel.PuntoVentaItemEfectivo(pago.AG_Importe__c, parseDivisa(pago.Divisa__c)));
                }
                when 'Transferencia'{
                    ptoVenta.TransaccionTipoCodigo = 'TRANSFERENCIATER';
                    ptoVenta.PuntoVentaItemsBanco.add(new RemoteSiteModel.PuntoVentaItemBanco(pago.AG_Importe__c, parseDivisa(pago.Divisa__c)));
                }
                when 'Deposito'{
                    ptoVenta.TransaccionTipoCodigo = 'RECEPCHTER';
                    ptoVenta.PuntoVentaItemsBanco.add(new RemoteSiteModel.PuntoVentaItemBanco(pago.AG_Importe__c, parseDivisa(pago.Divisa__c)));
                }
                when 'Cheque'{
                    ptoVenta.TransaccionTipoCodigo = 'RECEPCHTER';
                    ptoVenta.PuntoVentaItemsBanco.add(new RemoteSiteModel.PuntoVentaItemBanco(pago.AG_Importe__c, parseDivisa(pago.Divisa__c)));
                }
                when 'Tarjeta de crédito'{
                    ptoVenta.TransaccionTipoCodigo = 'PATAR';
                    ptoVenta.PuntoVentaItemsTarjeta.add(new RemoteSiteModel.PuntoVentaItemTarjeta(pago.AG_Importe__c, parseDivisa(pago.Divisa__c)));
                }
                when 'Tarjeta de débito'{
                    ptoVenta.TransaccionTipoCodigo = 'PADEB';
                    ptoVenta.PuntoVentaItemsTarjeta.add(new RemoteSiteModel.PuntoVentaItemTarjeta(pago.AG_Importe__c, parseDivisa(pago.Divisa__c)));
                }
                when 'Via PayU'{
                    ptoVenta.TransaccionTipoCodigo = 'COBRAPAYU';
                    ptoVenta.PuntoVentaItemsBanco.add(new RemoteSiteModel.PuntoVentaItemBanco(pago.AG_Importe__c, parseDivisa(pago.Divisa__c)));
                }
                when else{
                    ptoVenta.PuntoVentaItemsOtros.add(new RemoteSiteModel.PuntoVentaItemOtros(pago.AG_Importe__c, parseDivisa(pago.Divisa__c)));
                }
            }
        }
        
        
        return JSON.serialize(ptoVenta).replace('Numero_de_identificacion_tributaria_99999XVC','Número de identificación tributaria');
    }*/
}