public class AG_TravelPlanBuffer {


    public static void travelPlanSync(Id tpId){
        Savepoint sp = Database.setSavepoint();
        try {
            AG_TravelPlan__c tplan = AG_BufferUtil.getTravelPlanById(tpId);

            if(tplan != null){
                List<AG_TravelPlanxParticipante__c> tpps = [SELECT Id, AG_ParticipantePrograma__c FROM AG_TravelPlanxParticipante__c WHERE AG_TravelPlanxParticipante__c=:tpId AND AG_ParticipantePrograma__r.AG_Participante__r.GL_Person__c !=null LIMIT 1];
                participantTravelPlanSync(tpps[0].AG_ParticipantePrograma__c, true);
            }
        }catch (Exception e){
            System.debug('message ' + e.getMessage());
        }
    }

    public static void participantTravelPlanSync(Id participanteProgramaId, Boolean syncTravelItems){
        Savepoint sp = Database.setSavepoint();
        try {
			System.debug('participanteProgramaId param : ' + participanteProgramaId);
            AG_ParticipanteDelPrograma__c pp = AG_BufferUtil.getParticipantePrograma(participanteProgramaId);
            Id personId = pp.AG_Participante__r.GL_PERSON__c;
            if(personId != null){
                GL_SERVICE__c sv = AG_BufferUtil.getService(personId, pp.AG_Programa__r.IdDePrograma__c);
                System.debug('ServiceID: ' + sv.Id);
                AG_TravelPlan__c tplan = AG_BufferUtil.getTravelPlan(participanteProgramaId);

                if(tplan != null){
                        System.debug('test0');
                    GL_TRAVEL_PLAN__c glTravelPlan;
                    if(tplan.GL_TRAVEL_PLAN__c != null){
                        System.debug('test1');
                        glTravelPlan = AG_BufferUtil.getGLTravelPlan(tplan.GL_TRAVEL_PLAN__c);
                         glTravelPlan = AG_BufferMapping.syncTravelPlan(tplan, glTravelPlan);
                        update glTravelPlan;
                        if(syncTravelItems){
                            syncTravelItems(tplan, glTravelPlan);
                        }
                    }else{
                        System.debug('test2');
                        //creo/edito el travel plan
                        glTravelPlan = createTravelPlan(tPlan, glTravelPlan);
                        syncTravelItems(tplan, glTravelPlan);
                    }
                    System.debug('test3');                    
                    upsertTravelPlanService(sv, glTravelPlan);


                    AG_BufferUtil.callSync(sv.Id, 'PLACEMENT');
                }

            }

        } catch (Exception e) {
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            Database.rollback(sp);
        }

    }
    public static void cleanGlTravelPlan(GL_TRAVEL_PLAN__c glTpId){
        List<GL_TRAVEL_PLAN_TRAVEL_ITEM__c> tpTitems = [SELECT id,IS_DELETED__c,TRAVEL_ITEM_ID__c from GL_TRAVEL_PLAN_TRAVEL_ITEM__c where TRAVEL_PLAN_ID__c =:glTpId.id];
        Set<Id> tItemsIds = new Set<Id>();
        for(GL_TRAVEL_PLAN_TRAVEL_ITEM__c tpti : tpTitems){
            tpti.IS_DELETED__c = 'Y';
        }
        update tpTitems;
    }

    public static GL_TRAVEL_PLAN__c createTravelPlan(AG_TravelPlan__c travelPlan, GL_TRAVEL_PLAN__c glTravelPlan){

        if(glTravelPlan == null){
            glTravelPlan = new GL_TRAVEL_PLAN__c();
            insert glTravelPlan;
            glTravelPlan.ID__c = glTravelPlan.id;
            glTravelPlan.TRAVEL_PLAN_REF__c = travelPlan.Name;
            glTravelPlan.TRAVEL_PLAN_NAME__c = travelPlan.Name + ' Name13';
        }

        glTravelPlan = AG_BufferMapping.syncTravelPlan(travelPlan, glTravelPlan);
        update glTravelPlan;

        travelPlan.GL_TRAVEL_PLAN__c = glTravelPlan.Id;
        updateTravelPlan(travelPlan.Id, glTravelPlan.Id);
        system.debug('TRAVEL PLAN CREATED OK');

        return glTravelPlan;
    }

    @Future
    public static void updateTravelPlan(String tpId, String glTpId){
        update new AG_TravelPlan__c(Id = tpId, GL_TRAVEL_PLAN__c = glTpId);
    }

    public static void syncTravelItems(AG_TravelPlan__c travelPlan, GL_TRAVEL_PLAN__c glTravelPlan ){
        //create GL_TRAVEL_ITEMS
        //get TravelPlanVuelos
        List<AG_AeropuertosDelTravelPlan__c> travelPlanVuelos = AG_BufferUtil.getTravelPlanVuelos(travelPlan.Id);

        cleanGlTravelPlan(glTravelPlan);

        List<GL_TRAVEL_ITEM__c> listTravelItems = new List<GL_TRAVEL_ITEM__c>();

        //sync AG_TravelPlanVuelo -> GL_TRAVEL_ITEM
        for(AG_AeropuertosDelTravelPlan__c tpv : travelPlanVuelos){
            GL_TRAVEL_ITEM__c ti = new GL_TRAVEL_ITEM__c();
            ti = AG_BufferMapping.syncTravelItem(tpv, ti);

            listTravelItems.add(ti);
        }
        insert listTravelItems;

        // update travel items id__c
        for(GL_TRAVEL_ITEM__c ti : listTravelItems){
            ti.ID__c = ti.id;
        }
        update listTravelItems;

        //insert relation GL_TRAVEL_PLAN_TRAVEL_ITEM
        List<GL_TRAVEL_PLAN_TRAVEL_ITEM__c>  listTravelPlanTravelItem = new List<GL_TRAVEL_PLAN_TRAVEL_ITEM__c>();
        for(GL_TRAVEL_ITEM__c ti : listTravelItems){
            GL_TRAVEL_PLAN_TRAVEL_ITEM__c travelPlanTI = new GL_TRAVEL_PLAN_TRAVEL_ITEM__c(TRAVEL_ITEM_ID__c = ti.ID__c, TRAVEL_PLAN_ID__c = glTravelPlan.ID__c);
            travelPlanTI.AG_TravelItem__c = ti.Id;
            travelPlanTI.AG_TravelPlan__c = glTravelPlan.Id;
            listTravelPlanTravelItem.add(travelPlanTI);
        }
        insert listTravelPlanTravelItem;

        // update travel_plan_travel_items id__c
        for(GL_TRAVEL_PLAN_TRAVEL_ITEM__c tpti : listTravelPlanTravelItem){
            tpti.ID__c = tpti.id;
        }
        update listTravelPlanTravelItem;
    }


    public static void upsertTravelPlanService(GL_SERVICE__c service, GL_TRAVEL_PLAN__c glTravelPlan){
        List<GL_TRAVEL_PLAN_SERVICE__c> current = [select id from GL_TRAVEL_PLAN_SERVICE__c where SERVICE_ID__c =: service.ID__c AND TRAVEL_PLAN_ID__c =: glTravelPlan.ID__c];
        if(current.isEmpty()){
            GL_TRAVEL_PLAN_SERVICE__c tpService = new GL_TRAVEL_PLAN_SERVICE__c(SERVICE_ID__c = service.ID__c, TRAVEL_PLAN_ID__c = glTravelPlan.ID__c);
            insert tpService;
            tpService.ID__c = tpService.Id;
            tpService.AG_Service__c = service.Id;
            tpService.AG_TravelPlan__c = glTravelPlan.Id;
            update tpService;
        }
    }

}