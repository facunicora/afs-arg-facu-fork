public class ModelToTest {
    //Importante: no tener comentarios en el string!
    public static void parse(){
        Integration_Log__c log = [SELECT Id, comments__c, json__c FROM Integration_Log__c WHERE endpoint__c = 'ModelToTest'];
        String s = log.json__c.trim();
        String res = '';
        
        s = s.substringAfter('public').trim();
        
        while(s != '' && s != null){
            if(s.startsWithIgnoreCase('class')){
                String nombreClase = s.substringAfter(' ').substringBefore('{').trim();
                String s2 = s.substringAfter('{');
                Boolean flag = true;
                
                res += 'public static testmethod void '+nombreClase+'Test(){ <br/>';
                
                while(!s2.startsWith('}')){
                    if(s2.substringAfter('public').trim().startsWithIgnoreCase(nombreClase)){
                        flag = false;
                        String s3 = s2.substringAfter('(').substringBefore('{').trim();
                        
                        res += 'RemoteSiteModel.'+nombreClase+' a = new RemoteSiteModel.'+nombreClase+'(';
                        while(true){
                            if(s.startsWith(')')){
                                res += '); <br/>';
                                break;
                            }
                            
                            res += 'null';
                            if(s3.contains(',')){
                                res += ',';
                                s3 = s3.substringAfter(',');
                            }else{
                                res += '); <br/>';
                                break;
                            }
                        }
                    }
                    
                    s2 = s2.substringAfter('}').trim();
                }
                
                if(flag){
                    res += 'RemoteSiteModel.'+nombreClase+' a = new RemoteSiteModel.'+nombreClase+'(); <br/>';
                }
            }else if(s.startsWithIgnoreCase('String')){
                String nombreCampo = s.substringAfter(' ').substringBefore('{get; set;}').trim();
                res += 'a.'+nombreCampo+' = \'\'; <br/>';
            }else if(s.startsWithIgnoreCase('Integer') || s.startsWithIgnoreCase('Decimal')){
                String nombreCampo = s.substringAfter(' ').substringBefore('{get; set;}').trim();
                res += 'a.'+nombreCampo+' = 0; <br/>';
            }else if(s.startsWithIgnoreCase('Boolean')){
                String nombreCampo = s.substringAfter(' ').substringBefore('{get; set;}').trim();
                res += 'a.'+nombreCampo+' = false; <br/>';
            }else if(s.startsWithIgnoreCase('List')){
                String nombreCampo = s.substringAfter('>').substringBefore('{get; set;}').trim();
                //String nombreTipo = s.substringAfter('<').substringBefore('>').trim();
                res += 'a.'+nombreCampo+' = null; <br/>';
            }
            
            if(s.substringAfter('}').trim().startsWith('}')){
                res += '} <br/>';
            }
            s = s.substringAfter('public').trim();
        }
        
        log.comments__c = res;
        update log;
    }
}