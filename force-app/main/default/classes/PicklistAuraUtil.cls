public class PicklistAuraUtil {
	// Wrapper to handle picklist values
    public class PicklistWrapperUtil {
    	@AuraEnabled
        public Boolean isSelected {get;set;}
        @AuraEnabled
        public String fieldVal {get;set;}
        @AuraEnabled
        public String fieldLabel {get;set;}
        
        public PicklistWrapperUtil(String lbl, boolean flag,String val){
            fieldVal = val;
            isSelected = flag;
            fieldLabel = lbl;
        }
    }
    
    @AuraEnabled
    public static Map<String,List<PicklistWrapperUtil>> getPicklistValues(String objName, string fields){
        map<String,List<PicklistWrapperUtil>> mapToReturn = new map<String,List<PicklistWrapperUtil>>();
        SObject objObject = Schema.getGlobalDescribe().get(objName).newSObject() ;
    	Schema.sObjectType objType = objObject.getSObjectType();               
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();                
        Map<String,Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        
        for(String fld : fields.split(',')){
          for(Schema.PicklistEntry picklistVal : fieldMap.get(fld).getDescribe().getPickListValues()){
            if(!mapToReturn.containsKey(fld)){
                mapToReturn.put(fld,new List<PicklistWrapperUtil>());
            }
            PicklistWrapperUtil pwu = new PicklistWrapperUtil(picklistVal.getLabel(),false,picklistVal.getValue()); 
            mapToReturn.get(fld).add(pwu);  
          }  
        }
        
        return mapToReturn;
    }
}