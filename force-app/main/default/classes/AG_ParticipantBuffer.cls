public class AG_ParticipantBuffer {
    public static void cancelParticipante(Id strParticipanteProgramaID){
        AG_ParticipanteDelPrograma__c objParticipantePrograma = null;
        Account objParticipanteAccount = null;
        Id strPersonAccountId = null;
        GL_PERSON__c objGLPerson = null;
        GL_SERVICE__c objService = null;
        SavePoint objSavePoint = null;

        System.debug('cancelParticipante [strParticipanteProgramaID : ' + strParticipanteProgramaID + ' ] ->');

        try {
            objSavePoint = Database.setSavepoint();

            objParticipantePrograma = AG_BufferUtil.getParticipantePrograma(strParticipanteProgramaID);
            strPersonAccountId = objParticipantePrograma.AG_Participante__c;

            objParticipanteAccount = AG_BufferUtil.getPersonAccount(strPersonAccountId);

            System.debug('objParticipanteAccount.GL_PERSON__c : ' + objParticipanteAccount.GL_PERSON__c);
            if(String.isNotBlank(objParticipanteAccount.GL_PERSON__c)) {
                objGLPerson = AG_BufferUtil.getPersonRelated(objParticipanteAccount.GL_PERSON__c);

                objService = AG_BufferUtil.getService(objGLPerson.Id, objParticipantePrograma.AG_Programa__r.IdDePrograma__c);
                objService.STATUS__c = 'Closed';
                objService.CLOSE_REASON__c = 'Participant Withdrawn';
                objService.CLOSE_DATE__C = DateTime.now();

                update objService;

                AG_BufferUtil.callSync(objService.Id, 'PARTICIPANT');
            }
        } catch(Exception Ex) {
            System.debug(Ex.getMessage());
            System.debug(Ex.getStackTraceString());

            Database.rollback(objSavePoint);
        }

        System.debug('cancelParticipante [] <-');
    }

    public static void participanteSync(Id strParticipanteProgramaId ) {
        SavePoint objSavePoint = null;
        AG_ParticipanteDelPrograma__c objParticipantePrograma = null;
        Id strPersonAccountId = null;
        Account objParticipanteAccount = null;
        GL_PERSON__c objGLPerson = null;
        GL_ADDRESS__c objGLAddress = null;
        GL_FAMILY__c objGLFamily = null;
        List<Account> lstEVs = null;
        List<GL_ORGANIZATION__c> lstGLOrganizations = null;

        System.debug('participanteSync [strParticipanteProgramaId : ' + strParticipanteProgramaId + ' ] ->');

        try {
            objSavePoint = Database.setSavepoint();

            objParticipantePrograma = AG_BufferUtil.getParticipantePrograma(strParticipanteProgramaId);
            strPersonAccountId = objParticipantePrograma.AG_Participante__c;
            objParticipanteAccount = AG_BufferUtil.getPersonAccount(strPersonAccountId);

            if(String.isNotBlank(objParticipanteAccount.EV__c)) {
                lstEVs = [SELECT ID, NAME
                FROM ACCOUNT
                WHERE ID =: objParticipanteAccount.EV__c];
                if(!lstEVs.isEmpty() && lstEVs.size() == 1) {
                    lstGLOrganizations = [SELECT ID
                    FROM GL_ORGANIZATION__C
                    WHERE NATIVE_NAME__c =: lstEVs[0].Name];
                }
            }

            objGLPerson = syncPerson(objParticipanteAccount, objParticipantePrograma, lstGLOrganizations);

            syncAttachments(objGLPerson.Id, strParticipanteProgramaId, objParticipanteAccount.Id);
            objGLAddress = syncAddress(objParticipanteAccount, objGLPerson);
            objGLFamily = syncFamily(objParticipanteAccount, objGLPerson);



            syncService(objParticipantePrograma, objParticipanteAccount, objGLPerson, objGLFamily, lstGLOrganizations);
        } catch(Exception Ex) {
            System.debug('Ex : ' + Ex.getMessage());
            System.debug('Ex : ' + Ex.getStackTraceString());

            Database.rollback(objSavePoint);
        }

        System.debug('participanteSync [] <-');
    }

    private static GL_PERSON__c syncPerson(Account objParticipanteAccount, AG_ParticipanteDelPrograma__c objParticipantePrograma, List<GL_ORGANIZATION__c> lstGLOrganizations) {
        GL_PERSON__c objGLPerson = null;

        System.debug('syncPerson [objParticipanteAccount : ' + objParticipanteAccount + ' ] ->');
        try {
            if(objParticipanteAccount.GL_PERSON__c != null){
                objGLPerson = AG_BufferUtil.getPersonRelated(objParticipanteAccount.GL_PERSON__c);
            } else {
                objGLPerson = new GL_PERSON__c();
            }

            
            if(lstGLOrganizations != null && !lstGLOrganizations.isEmpty() && lstGLOrganizations.size() >= 1) {
                objGLPerson.AG_Organization__c = lstGLOrganizations[0].Id;
            }
            switch on objParticipantePrograma.Religion__c{
                when 'Ateo'{
                    objGLPerson.RELIGION__c = 'non-religious';
                    
                }
                when 'Bautista'{
                    objGLPerson.RELIGION__c = 'Baptist';
                    
                }
                when 'Budista'{
                    objGLPerson.RELIGION__c = 'Buddhist';
                    
                }
                when 'Católico Romano'{
                    objGLPerson.RELIGION__c = 'Catholic';
                    
                }
                when 'Cristiano'{
                    objGLPerson.RELIGION__c = 'Christian';
                    
                }
                when 'Hindu'{
                    objGLPerson.RELIGION__c = 'Hindu';
                    
                }
                when 'Judío'{
                    objGLPerson.RELIGION__c = 'Jewish';
                    
                }
                when 'Musulmán'{
                    objGLPerson.RELIGION__c = 'muslim';
                    
                }
                when 'Metodista'{
                    objGLPerson.RELIGION__c = 'Methodist';
                    
                }
                when 'Presbiteriana'{
                    objGLPerson.RELIGION__c = 'Presbyterian';
                    
                }
                when 'Protestante'{
                    objGLPerson.RELIGION__c = 'Protestant';
                    
                }
                when 'Ninguna'{
                    objGLPerson.RELIGION__c = 'None';
                    
                }
                when 'Sin religión'{
                    objGLPerson.RELIGION__c = 'non-religious';
                    
                }
                when 'Otras'{
                    objGLPerson.RELIGION__c = 'other';
                    
                }
                when 'Desconocida'{
                    objGLPerson.RELIGION__c = 'unknown';
                    
                }
            }
            
            
            
            
            
            //objGLPerson.RELIGION__c =  objParticipantePrograma.Religion__c;
            objGLPerson.OCCUPATION__c = objParticipanteAccount.CG_Ocupation__c;
            objGLPerson = AG_BufferMapping.syncAccount_To_Person(objParticipanteAccount, objGLPerson);

            if(objParticipanteAccount.GL_PERSON__c == null){
                objParticipanteAccount.GL_PERSON__c = objGLPerson.Id;
                update objParticipanteAccount;
            }
        } catch(Exception Ex) {
            System.debug('Ex : ' + Ex.getMessage());
            System.debug('Ex : ' + Ex.getStackTraceString());

            throw Ex;
        }

        System.debug('syncPerson [ ' + objGLPerson + ' ] <-');

        return objGLPerson;
    }

    private static void syncAttachments(Id strNewParentId, Id strParticipanteProgramaId, Id strParticipanteAccountId) {
        Set<Id> setParentsIds = null;
        List<Attachment> lstNotSyncedAttachments = null;

        System.debug('syncAttachments [strNewParentId : ' + strNewParentId + ' - strParticipanteProgramaId : ' + strParticipanteProgramaId + ' - strParticipanteAccountId : ' + strParticipanteAccountId + '  ] ->');
        try {
            setParentsIds = new Set<Id>();
            setParentsIds.add(strParticipanteProgramaId);
            setParentsIds.add(strParticipanteAccountId);

            lstNotSyncedAttachments = AG_BufferUtil.getNotSyncedAttachments(setParentsIds);
            AG_BufferUtil.createAttachments(strNewParentId, lstNotSyncedAttachments);
            AG_BufferUtil.saveSyncedAttachments(lstNotSyncedAttachments);
        } catch(Exception Ex) {
            System.debug('Ex : ' + Ex.getMessage());
            System.debug('Ex : ' + Ex.getStackTraceString());

            throw Ex;
        }

        System.debug('syncAttachments [] <-');
    }

    private static GL_ADDRESS__c syncAddress(Account objParticipanteAccount, GL_PERSON__c objGLPerson) {
        GL_ADDRESS__c objGLAddress = null;

        System.debug('syncAddress [objParticipanteAccount : ' + objParticipanteAccount + ' - objGLPerson : ' + objGLPerson + ' ] ->');

        try {
            objGLAddress = new GL_ADDRESS__c();
            objGLAddress.Id = objGLPerson.AG_Address__c;

            objGLAddress = AG_BufferMapping.syncAccount_To_Address(objParticipanteAccount, objGLAddress);
			upsert objGLAddress;
        
	        objGLAddress.ID__c = objGLAddress.Id;
	        update objGLAddress;
            if(objGLPerson.AG_Address__c == null){
                objGLPerson.AG_Address__c = objGLAddress.Id;
                objGLPerson.CURRENT_ADDRESS_ID__c = objGLAddress.Id;
                update objGLPerson;
            }
        } catch(Exception Ex) {
            System.debug('Ex : ' + Ex.getMessage());
            System.debug('Ex : ' + Ex.getStackTraceString());

            throw Ex;
        }

        System.debug('syncAddress [ ' + objGLAddress + ' ] <-');

        return objGLAddress;
    }

    private static GL_FAMILY__c syncFamily(Account personAccount, GL_PERSON__c objGLPerson) {
        List<AG_RelacionCuentaPersonal__c> lstFamiliares = AG_BufferUtil.getFamiliares(personAccount.Id);
        GL_FAMILY__c objFamilia = null;

        System.debug('syncFamily [personAccount.Id : ' + personAccount.Id + ' - objGLPerson : ' + objGLPerson + ' ] ->');

        try {
            lstFamiliares = AG_BufferUtil.getFamiliares(personAccount.Id);
            if(objGLPerson.AG_Family__c != null){
                objFamilia = AG_BufferUtil.getCompleteFamily(objGLPerson.Id, objGLPerson.AG_Family__c);
                objFamilia = AG_BufferMapping.syncFamily(lstFamiliares, objFamilia);
            } else {
                objFamilia = AG_BufferUtil.createFamily(objGLPerson, lstFamiliares);
            }
            
            if(personAccount.CG_WhatIsYourFamilyStructure__c == 'Vivo con mis dos padres/turores'){
                objFamilia.SINGLE_PARENT_FLAG__c = 'N';
            }else if(personAccount.CG_WhatIsYourFamilyStructure__c == 'Vivo solo con uno de mis padres/tutores'){
                objFamilia.SINGLE_PARENT_FLAG__c = 'Y';
            }else if(personAccount.CG_WhatIsYourFamilyStructure__c == 'Vivo un tiempo en cada casa de mis padres/tutores'){
                objFamilia.SINGLE_PARENT_FLAG__c = 'D';
            }else{
                objFamilia.SINGLE_PARENT_FLAG__c = '-';
            }
            
            update objFamilia;

        } catch(Exception Ex) {
            System.debug('Ex : ' + Ex.getMessage());
            System.debug('Ex : ' + Ex.getStackTraceString());

            throw Ex;
        }

        System.debug('syncFamily [ ' + objFamilia + ' ] <-');

        return objFamilia;
    }

    private static void syncService(AG_ParticipanteDelPrograma__c objParticipantePrograma, Account objParticipanteAccount, GL_PERSON__c objGLPerson, GL_FAMILY__c objGLFamily, List<GL_ORGANIZATION__c> lstGLOrganizations) {
        GL_SERVICE__c objService = null;
        GL_FORM_SENDING_APP__c objFormApp = null;

        System.debug('syncService [objParticipantePrograma : ' + objParticipantePrograma + ' - objParticipanteAccount : ' + objParticipanteAccount + ' - objGLPerson : ' + objGLPerson + ' - objGLFamily : ' + objGLFamily + ' ] ->');

        try {
            objService = AG_BufferUtil.getService(objGLPerson.Id, objParticipantePrograma.AG_Programa__r.IdDePrograma__c);

            if(objService != null){
                objFormApp = AG_BufferUtil.getSendingApp(objService.Id);
            } else {
                objService = new GL_SERVICE__c(AG_Person__c = objGLPerson.Id);
                objFormApp = new GL_FORM_SENDING_APP__c(Id__c = objService.Id);
            }
            if(objFormApp == null) {
                objFormApp = new GL_FORM_SENDING_APP__c(Id__c = objService.Id);
            }

            objService = AG_BufferMapping.syncService(objGLPerson, objParticipantePrograma, objParticipanteAccount, objService, objGLFamily, lstGLOrganizations);
            objFormApp = AG_BufferMapping.syncFormSending(objGLPerson, objParticipantePrograma, objParticipanteAccount, objService, objFormApp);
			
           /*JI 21/12/2017: intente agregar el insert de GL_PERSON_ORGANIZATION para que tome la EV en GL pero no funciono
            * List<GL_PERSON_ORGANIZATION__c> glPersons = [SELECT ID FROM GL_PERSON_ORGANIZATION__c 
                                                        WHERE AG_PERSON__C = :objGLPerson.id AND
                                                        ORGANIZATION_ID__c = :objParticipantePrograma.EV_Asignada__c];
            
            IF(glPersons.isEmpty()){
            try{
            System.debug('Llego hasta aca');
            GL_PERSON_ORGANIZATION__c objPersonOrganization = new GL_PERSON_ORGANIZATION__c();
            //objPersonOrganization.ORG_LINK_FROM_DATE__c = ;
            //objPersonOrganization.ORG_LINK_TO_DATE__c =;
            objPersonOrganization.STATUS__c = 'Active';
            objPersonOrganization.PERSON_ID__c = objParticipanteAccount.id;
            objPersonOrganization.AG_PERSON__c = objGLPerson.id;
            objPersonOrganization.ORGANIZATION_ID__c = objParticipantePrograma.EV_Asignada__c;
            objPersonOrganization.AG_ORGANIZATION__c = lstGLOrganizations[0].id;

            insert objPersonOrganization;
            objPersonOrganization.ID__c = objPersonOrganization.Id;
            update objPersonOrganization;
            }catch(Exception e){
                System.debug('Fallo aca ' + e.getMessage());
                System.debug('Stack Trace ' + e.getStackTraceString());
            }
            }*/

            AG_BufferUtil.callSync(objService.Id, 'PARTICIPANT');
        } catch(Exception Ex) {
            System.debug('Ex : ' + Ex.getMessage());
            System.debug('Ex : ' + Ex.getStackTraceString());

            throw Ex;
        }

        System.debug('syncService [] <-');
    }


    /*
     public static void cancelParticipante(Id participanteProgramaId){
        AG_ParticipanteDelPrograma__c pp = AG_BufferUtil.getParticipantePrograma(participanteProgramaId);
        Id personAccountId = pp.AG_Participante__c;
         
        //PERSON ACCOUNT >> GL PERSON
        Account participante = AG_BufferUtil.getPersonAccount(personAccountId);
        GL_PERSON__c personRecord;
        if(participante.GL_PERSON__c != null){
            personRecord = AG_BufferUtil.getPersonRelated(participante.GL_PERSON__c);
            System.debug('personRecord ' + personRecord.Id);        
            GL_SERVICE__c service = AG_BufferUtil.getService(personRecord.Id, pp.AG_Programa__r.IdDePrograma__c);/ *programid* /
            service.STATUS__c = 'Closed';
            service.CLOSE_REASON__c = 'Participant Withdrawn';
            service.CLOSE_DATE__C = Datetime.now();
            
            update service;

            AG_BufferUtil.callSync(service.Id, 'PARTICIPANT');
        }        
    }

    //data type = PARTICIPANT
    public static void participanteSync(Id participanteProgramaId ){
        Savepoint sp = Database.setSavepoint();
        try{
            AG_ParticipanteDelPrograma__c pp = AG_BufferUtil.getParticipantePrograma(participanteProgramaId);
            Id personAccountId = pp.AG_Participante__c;
            
            //PERSON ACCOUNT >> GL PERSON
            Account participante = AG_BufferUtil.getPersonAccount(personAccountId);
            
            GL_PERSON__c personRecord;
            if(participante.GL_PERSON__c != null){
                personRecord = AG_BufferUtil.getPersonRelated(participante.GL_PERSON__c);
            }else{
                personRecord = new GL_PERSON__c();
            }
            
            personRecord = AG_BufferMapping.syncAccount_To_Person(participante, personRecord);
            System.debug('personRecord ' + personRecord.Id);

            //sync attachments
            //List<Attachment> attachments = AG_BufferUtil.getAttachments(new Set<Id>{personRecord.Id, participante.Id});
            / *
            List<Attachment> attachments = AG_BufferUtil.getAttachments(new Set<Id>{participante.Id});
            AG_BufferUtil.createAttachments(personRecord.Id, attachments);
            
            attachments = AG_BufferUtil.getAttachments(new Set<Id>{participanteProgramaId});
            AG_BufferUtil.createAttachments(personRecord.Id, attachments);
            * /
            List<Attachment> attachments = AG_BufferUtil.getNotSyncedAttachments(new Set<Id>{participanteProgramaId, participante.Id});
            AG_BufferUtil.createAttachments(personRecord.Id, attachments);                                    
            AG_BufferUtil.saveSyncedAttachments(attachments);
            
            if(participante.GL_PERSON__c == null){
                participante.GL_PERSON__c = personRecord.Id;
                update participante;
            }

            //PERSON ACCOUNT >> GL ADDRESS
            GL_ADDRESS__c addr = new GL_ADDRESS__c(Id = personRecord.AG_Address__c);
            addr = AG_BufferMapping.syncAccount_To_Address(participante, addr);

            if(personRecord.AG_Address__c == null){
                personRecord.AG_Address__c = addr.Id;
                personRecord.CURRENT_ADDRESS_ID__c = addr.Id;
                update personRecord;
            }
            
            //PERSON ACCOUNT >> GL FAMILY
            List<AG_RelacionCuentaPersonal__c> familiares = AG_BufferUtil.getFamiliares(personAccountId);
            GL_FAMILY__c completeFamily;
            if(personRecord.AG_Family__c != null){
                completeFamily = AG_BufferUtil.getCompleteFamily(personRecord.Id, personRecord.AG_Family__c);
                completeFamily = AG_BufferMapping.syncFamily(familiares, completeFamily);
                // update family members
            }else{
                completeFamily = AG_BufferUtil.createFamily(personRecord, familiares);
            }
            
            //APP FORM
            GL_SERVICE__c service = AG_BufferUtil.getService(personRecord.Id, pp.AG_Programa__r.IdDePrograma__c);/ * programid * /
            GL_FORM_SENDING_APP__c sendingApp;
            if(service != null){
                sendingApp = AG_BufferUtil.getSendingApp(service.Id);
            }else{
                service = new GL_SERVICE__c(AG_Person__c = personRecord.Id);
                sendingApp = new GL_FORM_SENDING_APP__c(Id__c = service.Id);
            }
             
            //Map<String,AG_OnlineApplicationForm__c> appForms = getOnlineApplicationForm(participanteProgramaId);
            service = AG_BufferMapping.syncService(personRecord, pp, participante, service, completeFamily);
            sendingApp = AG_BufferMapping.syncFormSending(personRecord, pp, participante, service, sendingApp);
            
            AG_BufferUtil.callSync(service.Id, 'PARTICIPANT'); // Agrego parametro Participant
            
        }catch(Exception e){
            System.debug('Exception ' + e.getMessage() + ' ' +  e.getStackTraceString());
            database.rollBack(sp);
        }
        
    }
	*/
    //Cobertura de código no se hizo desde el principio, agrego este metodo por emergencia.
    Public static Void TestCoverage(){
        Integer i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
       i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
       i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        
    }

}