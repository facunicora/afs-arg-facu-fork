@isTest
    public class Util_Test {
	public static Account createAccount(){
        Account acc = new Account();
        acc.Name = 'Test Account';
        return acc;
    }
        
    public static Account createPersonAccount(){
		return new Account(LastName = 'Test', 
                           AG_DNI__c = 40404040,
                           Correo_Electronico__c = 'asd@asd.com',
						   recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId());
        
    }
    
    public static Contract createContract(Account acc){
        Contract con = new Contract();
        con.Name = 'Test Contract';
        con.AccountId = acc.Id;
        return con;
    }
    
    public static Task createTask(Id idObj){
        Task tas = new Task();
        tas.OwnerId = UserInfo.getUserId();
        tas.ActivityDate = System.today();
        tas.WhatID = idObj;
        return tas;
    }
    
    public static Opportunity createOpportunity(Account acc){
        Opportunity opp = new Opportunity();
		opp.Name = 'Test Opp';
        opp.AccountId = acc.Id;
        opp.CloseDate = system.today() + 300;
        opp.StageName = 'Relevar';
        return opp;
    }
    
    public static Product2 createProduct2(){
        Product2 prd = new Product2();
		prd.Name = 'Test Product';
        prd.ProductCode = '20CSFI01';
        return prd;
    }
    
    public static PricebookEntry createPriceBookEntry(Product2 prd){
        Id pricebookId = Test.getStandardPricebookId();         
        PricebookEntry pe = new PricebookEntry();
		pe.Pricebook2Id = pricebookId;
        pe.Product2Id = prd.Id;
        pe.UnitPrice = 1000;
        pe.IsActive = true;
		return pe;
    }
    
    public static OpportunityLineItem createOLI(Product2 prd,Opportunity opp,PricebookEntry pe){               
        OpportunityLineItem oli = new OpportunityLineItem();
		oli.Product2Id = prd.Id;
		oli.Quantity = 2;
        oli.OpportunityId = opp.Id;
        oli.PricebookEntryId = pe.Id;
        oli.TotalPrice = oli.Quantity * pe.UnitPrice;
		return oli;
    }   
  
    public static Lead createLead(){
        Lead le = new Lead(); 
        le.State = 'Buenos Aires';
        le.LastName = 'Test LastName';
        le.Company = 'Test Company';
        return le;
    }
    
    public static Asset createAsset(Contract con){
        Asset ast = new Asset();
        ast.Name = 'Test Asset';
        ast.Status = 'Provisionado';
        return ast;
    }
    
    public static User createUser(){ 
        User us = new User();
        Profile p = [SELECT Id FROM Profile LIMIT 1]; 
        us.ProfileId = p.Id;
        us.Alias = 'userTest';
        us.LastName = 'Test';
        us.Email = 'test@test.com';
        us.Username = 'test@hendercross.com';
        us.CommunityNickname = 'userTest';
        us.TimeZoneSidKey = 'America/Argentina/Buenos_Aires';
        us.LocaleSidKey = 'en_US';
        us.EmailEncodingKey = 'UTF-8';
        us.LanguageLocaleKey = 'en_US';
        return us;
    }
    
    public static List<User> createListUser(){ 
        List<User> usList = new List<User>();
        User us = new User();
        Profile p = [SELECT Id,name FROM Profile WHERE name='Administrador del sistema']; 
        us.ProfileId = p.Id;
        us.Alias = 'userTest';
        us.LastName = 'Test';
        us.Email = 'test@test.com';
        us.CommunityNickname = 'userTest';
        us.TimeZoneSidKey = 'America/Argentina/Buenos_Aires';
        us.LocaleSidKey = 'en_US';
        us.EmailEncodingKey = 'UTF-8';
        us.LanguageLocaleKey = 'en_US';
        usList.add(us);
        return usList;
    }
    
    public static List<AG_PlanDePago__c> createPlanesDePago(integer num){
        List<AG_PlanDePago__c> planesDePagoLista = new List<AG_PlanDePago__c>();
        for(Integer i=0;i<num;i++) {
            AG_PlanDePago__c p = new AG_PlanDePago__c(CG_CreateQuota__c = false,
                                                      Monto__c = 1000);
            planesDePagoLista.add(p);
        }
        return planesDePagoLista;
    }
    
    public static List<AG_Pago__c> createPagoDolar(integer num, AG_PlanDePago__c planDePago){
        List<AG_Pago__c> pagoLista = new List<AG_Pago__c>();
        for(Integer i=0;i<num;i++) {
            AG_Pago__c p = new AG_Pago__c(AG_PlanDePago__c = planDePago.Id,
                                          AG_Importe__c = 100,
                                          AG_FechaDeAcreditacion__c = Date.today());
            pagoLista.add(p);
        }

        return pagoLista;
    }
    
    public static List<AG_Pago__c> createPagoPesos(integer num, AG_PlanDePago__c planDePago){
        List<AG_Pago__c> pagoLista = new List<AG_Pago__c>();
        for(Integer i=0;i<num;i++) {
            /*AG_Pago__c p = new AG_Pago__c(AG_PlanDePago__c = planDePago.Id,
                                          AG_Importe__c = 100,
                                          Monto_en_pesos__c = 1000,
                                          AG_FechaDeAcreditacion__c = Date.today());*/
            AG_Pago__c p = new AG_Pago__c(AG_PlanDePago__c = planDePago.Id,
                                          AG_Importe__c = 100,
                                          AG_FechaDeAcreditacion__c = Date.today());
            pagoLista.add(p);
        }

        return pagoLista;
    }
    
    public static Valor_de_Cambio__c createValorDeCambio(Date aa, Decimal cambio, String divisa, String divisaCambio){
            Valor_de_Cambio__c vc = new Valor_de_Cambio__c();
            vc.Fecha__c = aa;
            vc.Cambio__c = cambio;
            vc.Divisa__c = divisa;
            vc.Divisa_del_cambio__c = divisaCambio;

        return vc;
    }
    
    public static Account createAccountWRecordType(Id recordtype){
        Account Pa = new Account();
        Pa.RecordTypeId = recordtype;
        Pa.LastName = 'Lastname Test';
        Pa.Correo_Electronico__c = 'test@test.com';
        Pa.PersonMailingCountry = 'probando arg';
        return Pa;
    }

    public static AG_Programa__c createPrograma(Id recordtype){
        AG_Programa__c pr = new AG_Programa__c();
        pr.name = 'Pr Test';
        pr.RecordTypeId = recordtype;
        pr.AG_Status__c = 'Confirmado';
        pr.IdDePrograma__c = '1';
        pr.AG_PaisdelPrograma__c = 'Alemania';
        pr.AG_Categoria__c = 'Test';
        pr.Program_Code__c = '168498';
        pr.AG_EdadMax__c = 30;
        pr.AG_EdadMinima__c = 9;
        pr.AG_IOC_sender__c = 'test';
        pr.AG_MesInicio__c = date.newInstance(2019, 11, 01);
        pr.AG_MesFin__c = date.newInstance(2019, 11, 21);
        pr.AG_Precio__c = 500;
        pr.Comision_Reclutamiento__c = 10.5;
        pr.Target_Group__c = 'Mayores 18';
        return pr;
        
    }
    
    public static AG_ParticipanteDelPrograma__c createParticipanteDelPrograma(Account participante, Id recordtype, AG_Programa__c programa){
        AG_ParticipanteDelPrograma__c newPP = new AG_ParticipanteDelPrograma__c();
        newPP.name = 'PP Test';
        newPP.AG_Participante__c = participante.Id;
        newPP.AG_Programa__c = programa.Id;
        newPP.Tipo__c = 'Participante';
        newPP.RecordTypeId = recordtype;
        
        return newPP;
    }
        
    public static AG_ParticipanteDelPrograma__c createParticipanteDelPrograma2(Account participante, Id recordtype, AG_Programa__c programa){
        AG_ParticipanteDelPrograma__c newPP = new AG_ParticipanteDelPrograma__c();
        newPP.name = 'PP Test';
        newPP.AG_Participante__c = participante.Id;
        newPP.AG_Programa__c = programa.Id;
        newPP.Tipo__c = 'Participante';
        newPP.RecordTypeId = recordtype != null? recordtype : Schema.SObjectType.AG_ParticipanteDelPrograma__c.getRecordTypeInfosByDeveloperName().get('Mayores_18').getRecordTypeId();
        
        return newPP;
    }
    
    public static Presupuestador__c createPresupuesto(AG_ParticipanteDelPrograma__c pp, String estadoPre){
        Presupuestador__c pres = new Presupuestador__c();
        pres.Presupuestos__c = pp.Id;
        pres.Partner__c = 'CELTIC';
        pres.Estado__c = estadoPre;
        pres.Presupuestador__c = 'Version 2';
        
        return pres;
    }
    
    public static AG_TravelPlan__c createTravelPlan(AG_Programa__c prog, AG_Aeropuertos_del_Programa__c orig, AG_Aeropuertos_del_Programa__c dest){
        AG_TravelPlan__c tp = new AG_TravelPlan__c();
        tp.Name = 'Test';
        if(prog != null){
            tp.AG_Programa__c = prog.Id;
        }
        if(orig != null){
            tp.CG_OrigenTP__c = orig.Id;
        }
        if(dest != null){
            tp.CG_DestinoTP_del__c = dest.Id;
        }
        return tp;
    }
    
    public static AG_TravelPlanxParticipante__c createTravelPlanXPartcipante(AG_TravelPlan__c tp,AG_ParticipanteDelPrograma__c pp){
        AG_TravelPlanxParticipante__c tpxp = new AG_TravelPlanxParticipante__c();
        tpxp.Name = 'Test';
        if(tp != null){
            tpxp.AG_TravelPlanxParticipante__c = tp.Id;
        }
        if(pp != null){
            tpxp.AG_ParticipantePrograma__c = pp.Id;
        }
        return tpxp;
    }
    
    public static AG_Pasajes__c createPasaje(AG_TravelPlan__c tp, AG_ParticipanteDelPrograma__c pp, AG_Aeropuertos_del_Programa__c orig, AG_Aeropuertos_del_Programa__c dest){
        AG_Pasajes__c pas = new AG_Pasajes__c();
        if(tp != null){
            pas.CG_TravelPlan__c = tp.Id;
        }
        if(pp != null){
            pas.AG_ParticipanteDelPrograma__c = pp.Id;
        }
        if(orig != null){
            pas.AG_OrigenPasaje__c = orig.Id;
        }
        if(dest != null){
            pas.AG_DestinoPasaje__c = dest.Id;
        }
        return pas;
    }
}