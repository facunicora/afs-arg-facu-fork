public class OcultarOMostrarSeccionesEnElPorfolio {
    public static Boolean ocultarOMostrarPermisosYConsentimientos(AG_ParticipanteDelPrograma__c pp,Date fechaInicio,Date fecNacimiento){
        Boolean isChanged = false;
        List<String> lstPaginasOcultar = new List<String>();
        Integer edad;
        if(fechaInicio != null && fecNacimiento != null){            
            edad = fechaInicio.year() -  fecNacimiento.year();
            //Validar que todavia no paso su cumpleaños este año (se usa 2000 porque es bisiesto)
            if(Date.newInstance(2000,fecNacimiento.month(),fecNacimiento.day()) > Date.newInstance(2000,fechaInicio.month(),fechaInicio.day())){
                edad--;
            }
        }
        if(edad >= 18 || edad == null){
            if(pp.HC_Paginas_a_ocultar__c != null && pp.HC_Paginas_a_ocultar__c.ToUpperCase().contains('PERMISOS Y CONSENTIMIENTOS')){
                lstPaginasOcultar = pp.HC_Paginas_a_ocultar__c.split(';');
                Integer indexToRemove;
                for(Integer i= 0 ; i < lstPaginasOcultar.size(); i++){
                    if(lstPaginasOcultar[i].ToUpperCase() == 'PERMISOS Y CONSENTIMIENTOS'){
                        indexToRemove = i;
                        break;
                    }
                }
                lstPaginasOcultar.remove(indexToRemove);
                pp.HC_Paginas_a_ocultar__c = String.join(lstPaginasOcultar,';');
                isChanged = true;
            }
        }else{
            if(pp.HC_Paginas_a_ocultar__c == null || !pp.HC_Paginas_a_ocultar__c.ToUpperCase().contains('PERMISOS Y CONSENTIMIENTOS')){
                if(pp.HC_Paginas_a_ocultar__c != null){
                    lstPaginasOcultar = pp.HC_Paginas_a_ocultar__c.split(';');
                }                    
                lstPaginasOcultar.add('Permisos y Consentimientos');
                pp.HC_Paginas_a_ocultar__c = String.join(lstPaginasOcultar,';');
                isChanged = true;
            }
        }
        return isChanged;
    }
}