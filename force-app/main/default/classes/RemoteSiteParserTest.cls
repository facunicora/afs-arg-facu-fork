@IsTest
private class RemoteSiteParserTest {
    public static testmethod void AccountToClienteTest(){
        Account acc = Util_Test.createPersonAccount();
        acc.AG_DNI__c = 404040404;
        insert acc;
        acc = [SELECT Id, PersonContactId, PersonContact.LeadSource, AG_DNI__c, LastName
               FROM Account WHERE Id = :acc.Id];
        Contact con = new Contact(Id = acc.PersonContactId, LeadSource = 'Referido por Voluntario', Identificacion_Tributaria_Codigo__c = 'CUIL');
        update con;
        AG_Programa__c prog = Util_Test.createPrograma(Schema.SObjectType.AG_Programa__c.getRecordTypeInfosByName().get('Mayores de 18').getRecordTypeId());
        insert prog;
        AG_ParticipanteDelPrograma__c part = Util_Test.createParticipanteDelPrograma2(acc, null, prog);
        insert part;
        AG_PlanDePago__c plan = new AG_PlanDePago__c(Participante__c = acc.Id, AG_ParticipanteDelPrograma__c = part.Id);
        insert plan;
        
        acc = [SELECT Id, IsPersonAccount, AG_DNI__c, name, PersonEmail, PersonContact.Situacion_IIBB__c, 
                                   		  PersonContact.Categoria_Fiscal_Codigo__c, PersonContact.Constancia_Inscripcion_Afip__c, 
                                   		  PersonContact.Identificacion_Tributaria_Codigo__c, PersonMailingCountry, 
                                   		  PersonContact.Condicion_Pago_Codigo__c, PersonContact.Enviado_a_Ceres__c, PersonContactId
                                   FROM Account WHERE ID = :acc.Id];
        RemoteSiteParser.AccountToCliente(acc, plan);
    }
    
    public static testmethod void serializeCobranzaTest(){
        Account acc = Util_Test.createPersonAccount();
        insert acc;
        acc = [SELECT Id, PersonContactId, PersonContact.LeadSource, AG_DNI__c, LastName
               FROM Account WHERE Id = :acc.Id];
        Contact con = new Contact(Id = acc.PersonContactId, LeadSource = 'Referido por Voluntario');
        update con;
        AG_Programa__c prog = Util_Test.createPrograma(Schema.SObjectType.AG_Programa__c.getRecordTypeInfosByName().get('Mayores de 18').getRecordTypeId());
        insert prog;
        AG_ParticipanteDelPrograma__c part = Util_Test.createParticipanteDelPrograma2(acc, null, prog);
        insert part;
        AG_PlanDePago__c plan = new AG_PlanDePago__c(Participante__c = acc.Id, AG_ParticipanteDelPrograma__c = part.Id);
        insert plan;
        
        List<AG_Pago__c> pagos = new List<AG_Pago__c>{
            new AG_Pago__c(AG_PlanDePago__c = plan.Id, Divisa__c = 'Peso argentino.', 
                           AG_MedioDePago_del__c = 'Efectivo', AG_FechaDeAcreditacion__c = Date.today(), AG_Importe__c = 5000),
            new AG_Pago__c(AG_PlanDePago__c = plan.Id, Divisa__c = 'Peso argentino.', 
                           AG_MedioDePago_del__c = 'Transferencia', AG_FechaDeAcreditacion__c = Date.today(), AG_Importe__c = 5000),
            new AG_Pago__c(AG_PlanDePago__c = plan.Id, Divisa__c = 'Peso argentino.', 
                           AG_MedioDePago_del__c = 'Deposito', AG_FechaDeAcreditacion__c = Date.today(), AG_Importe__c = 5000),
            new AG_Pago__c(AG_PlanDePago__c = plan.Id, Divisa__c = 'Peso argentino.', 
                           AG_MedioDePago_del__c = 'Cheque', AG_FechaDeAcreditacion__c = Date.today(), AG_Importe__c = 5000),
            new AG_Pago__c(AG_PlanDePago__c = plan.Id, Divisa__c = 'Peso argentino.', 
                           AG_MedioDePago_del__c = 'Via PayU', AG_FechaDeAcreditacion__c = Date.today(), AG_Importe__c = 5000),
            new AG_Pago__c(AG_PlanDePago__c = plan.Id, Divisa__c = 'Peso argentino.', 
                           AG_MedioDePago_del__c = 'Tarjeta de crédito', AG_FechaDeAcreditacion__c = Date.today(), AG_Importe__c = 5000),
            new AG_Pago__c(AG_PlanDePago__c = plan.Id, Divisa__c = 'Peso argentino.', 
                           AG_MedioDePago_del__c = 'Tarjeta de débito', AG_FechaDeAcreditacion__c = Date.today(), AG_Importe__c = 5000),
            new AG_Pago__c(AG_PlanDePago__c = plan.Id, Divisa__c = 'Peso argentino.', 
                           AG_FechaDeAcreditacion__c = Date.today(), AG_Importe__c = 5000)};
        insert pagos;
        
        List<Valor_de_Cambio__c> valores = new List<Valor_de_Cambio__c>{
            new Valor_de_Cambio__c(Cambio__c = 10, divisa_del_cambio__c = 'Peso argentino.', divisa__c = 'Dólares Americanos')
        };
        insert valores;
        RemoteSiteParser.serializeCobranza(plan, pagos, prog, 'Test');
    }
    
    public static testmethod void parseDivisaTest(){
        RemoteSiteParser.parseDivisa('Peso argentino.');
        RemoteSiteParser.parseDivisa('Dólares Americanos');
        RemoteSiteParser.parseDivisa('Real brasileño.');
        RemoteSiteParser.parseDivisa('Bolívar.');
        RemoteSiteParser.parseDivisa('Boliviano.');
        RemoteSiteParser.parseDivisa('Libra Esterlina');
        RemoteSiteParser.parseDivisa('Peso chileno.');
        RemoteSiteParser.parseDivisa('Peso colombiano.');
        RemoteSiteParser.parseDivisa('Peso uruguayo.');
        RemoteSiteParser.parseDivisa('Euro');
        RemoteSiteParser.parseDivisa('Otro');
    }
}