trigger CG_OnPago on AG_Pago__c (after insert, before insert, before update) {
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            CG_PagoTriggers.changeValueExist(Trigger.new);
        }
    }else{
        if(Trigger.isInsert){
            CG_PagoTriggers.process(); 
        }
    }
    

}