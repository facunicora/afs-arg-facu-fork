trigger Entrevista_GCC_Trigger on Entrevista_GCC__c (After insert) {
	if(Trigger.isInsert && Trigger.isAfter){
        Entrevista_GCC_NotificationPost.SendEmailAndPostChatter(Trigger.newMap);
    }
}