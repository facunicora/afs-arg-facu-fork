trigger CG_OnPasaje on AG_Pasajes__c (before insert,before Update ,after insert, after update, after delete) {

    if(Trigger.isBefore && Trigger.isInsert){
        PasajeTriggerHandler.beforeInsert(Trigger.new);
    }else if(Trigger.isAfter && Trigger.isInsert){
        PasajeTriggerHandler.afterInsert(Trigger.newMap);
    }else if(Trigger.isBefore && Trigger.isUpdate){
        PasajeTriggerHandler.beforeUpdate(Trigger.newMap,Trigger.oldMap);
    }else if(Trigger.isAfter && Trigger.isUpdate){
        PasajeTriggerHandler.afterUpdate(Trigger.newMap,Trigger.oldMap);
    }else if(Trigger.isAfter && Trigger.isDelete){
        PasajeTriggerHandler.afterDelete(Trigger.oldMap);
    }

}