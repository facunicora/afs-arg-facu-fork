trigger AG_TravelPlan on AG_TravelPlan__c (before update, after update) {

    if(Trigger.isBefore && Trigger.IsUpdate){
        if(Trigger.new.size() == 1){
            if(Trigger.new[0].SendToGlobalLink__c && !Trigger.old[0].SendToGlobalLink__c){
                Trigger.new[0].SendToGlobalLink__c = false;
                AG_TravelPlanBuffer.travelPlanSync(Trigger.new[0].Id);
            }
        }
    }
}