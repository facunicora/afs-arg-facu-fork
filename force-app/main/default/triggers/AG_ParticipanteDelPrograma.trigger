trigger AG_ParticipanteDelPrograma on AG_ParticipanteDelPrograma__c (before insert, before update, after update) {
    //sincronizar con global link solo si es 1 registro
    if(Trigger.isBefore && Trigger.IsUpdate){
        system.debug('Trigger.new.size() 1 --->'+Trigger.new.size());
        if(Trigger.new.size() == 1){
            AG_ParticipanteDelPrograma__c pp = Trigger.new[0];
            if(pp.CG_SendToGlobalLink__c && !Trigger.OldMap.get(pp.Id).CG_SendToGlobalLink__c){
                Savepoint sp = Database.setSavepoint();
                //try{
                    AG_ParticipantBuffer.participanteSync(pp.Id);                    
                //}catch(Exception e){
                  //  Database.rollback(sp);
                //    System.debug('ERROR on sync ' + e.getMessage());
                //}
                pp.CG_SendToGlobalLink__c = false;
            }
        }        
    }
    
    //sincronizar con global link solo si es 1 registro
    if(Trigger.isBefore && Trigger.IsUpdate){
        system.debug('Trigger.new.size() 2 --->'+Trigger.new.size());
        if(Trigger.new.size() == 1){
            AG_ParticipanteDelPrograma__c pp = Trigger.new[0];
            if(pp.AG_EstadoDeLaPostulacion__c == 'Baja' && Trigger.OldMap.get(pp.Id).AG_EstadoDeLaPostulacion__c != 'Baja'){
                Savepoint sp = Database.setSavepoint();
                //try{
                    AG_ParticipantBuffer.cancelParticipante(pp.Id);                    
                //}catch(Exception e){
                //    Database.rollback(sp);
                //    System.debug('ERROR on sync ' + e.getMessage());
                //}
                pp.CG_SendToGlobalLink__c = false;
            }
        }        
    }
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            ParticipanteDelProgromaTriggerHandler.isBeforeInsert(Trigger.new);
        }else if(Trigger.isUpdate){
            ParticipanteDelProgromaTriggerHandler.isBeforeUpdate(Trigger.newMap,Trigger.oldMap);
        }
    }
    
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            ParticipanteDelProgromaTriggerHandler.isAfterUpdate(Trigger.new,Trigger.oldMap);
        }
    }

}