trigger PresupuestoTrigger on Presupuestador__c (before insert, before update, after update, after insert) {
    if(Trigger.IsBefore){
        PresupuestoTriggerHandlerException.isBefore(Trigger.New);
    }else{
        PresupuestoTriggerHandlerException.isAfter(Trigger.New, Trigger.NewMap);
    }
}