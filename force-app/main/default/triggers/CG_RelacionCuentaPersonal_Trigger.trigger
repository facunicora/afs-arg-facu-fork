trigger CG_RelacionCuentaPersonal_Trigger on AG_RelacionCuentaPersonal__c (before insert, before update) {

    if(Trigger.isInsert && Trigger.isBefore){
        
        AG_RelacionCuentaPersonal_TriggerHandler.fillAccount(Trigger.New);

                
    }
        
        
}