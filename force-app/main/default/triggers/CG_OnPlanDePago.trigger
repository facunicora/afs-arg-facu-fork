trigger CG_OnPlanDePago on AG_PlanDePago__c (before Insert, before Update, after insert, after update) {
	
    if(Trigger.IsBefore){
         CG_PlanDePagoTriggers.isBefore(Trigger.new,Trigger.OldMap);
    }

    if(Trigger.IsAfter){
        CG_PlanDePagoTriggers.isAfter(Trigger.new);
        if(Trigger.IsInsert){
            CG_PlanDePagoTriggers.isAfterInsert(Trigger.new);
        }
    }

}