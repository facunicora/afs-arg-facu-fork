trigger GL_Triger_DownloadSalesforceData on AG_TRANSFER_SF_TO_GL_PENDING__c (after insert) {
  
   for(AG_TRANSFER_SF_TO_GL_PENDING__c  p : trigger.new) {
   string qid;
   string sfid;
   qid = p.id;
   sfid = p.SF_ID__c;
       
       
         GL_REST_DownloadSalesforceData.triggerDownload(qid,sfid);  
   }  
}